from journal import celery, create_app

app, celery = create_app('default', celery)

# Logging:
#
# logfile = os.path.join(os.getenv('HOME'), 'back/elardy.log')
# handler = logging.FileHandler(logfile, mode='a', encoding='utf-8')
# app.logger.addHandler(handler)
# app.logger.setLevel(logging.INFO)
#
# to start the celery worker execute the next command in the shell:
# $ celery worker -A celery_worker.celery --loglevel=info
# the virtual environment must be activated
# uwsgi --http :9090 --wsgi-file celery_worker.py --callable app

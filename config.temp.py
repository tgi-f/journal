import os

from datetime import timedelta
from glob import glob

basedir = os.path.dirname(__file__)


class Config:
    SECRET_KEY = 'MY SECRET KEY'
    PERMANENT_SESSION_LIFETIME = timedelta(hours=2)
    CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
    CELERY_BROKER_URL = 'redis://localhost:6379/0'
    # CELERY_RESULT_BACKEND = 'rpc://'
    # CELERY_BROKER_URL = 'pyamqp://guest@localhost//'
    # CELERY_BROKER_URL = 'redis://localhost:6379/0'
    # CELERY_BROKER_URL = 'pyamqp://username:password@192.168.56.2//'
    CELERY_IMPORTS = ('journal.tasks',)
    CELERY_TASK_RESULT_EXPIRES = timedelta(hours=2)
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    MAX_CONTENT_LENGTH = 5 * 1024 * 1024
    TOKEN_LENGTH = 12
    REQUEST_INTERVAL = 24
    MAIL_SERVER = 'smtp.yandex.ru'
    MAIL_PORT = 465
    MAIL_USE_SSL = True
    MAIL_USERNAME = 'username'  # for target@yandex.ru the username is 'target'
    MAIL_PASSWORD = 'password'
    J_SUBJECT_PREFIX = '[Journal A] '
    J_SENDER = 'support <noreply@this.host.ru>'
    CAPTCHA_FONTS = glob(os.path.join(basedir, 'deployment/captcha/*.ttf'))
    CAPTCHA_SIZES = (42, 48, 52)
    SITE_NAME = 'Журнал А'
    SITE_DESCRIPTION = 'Блоги о разном, записная книжка.'

    @staticmethod
    def init_app(app):
        pass


class Development(Config):
    ASSETS_DEBUG = True
    DEBUG = True
    MAIL_DEBUG = True
    MAIL_SUPPRESS_SEND = True
    SESSION_PROTECTION = None
    TOKEN_LENGTH = 0.1
    REQUEST_INTERVAL = 0.11
    SEND_FILE_MAX_AGE_DEFAULT = 0
    CACHE_HOST = '192.168.56.102'
    SQLALCHEMY_DATABASE_URI = \
        'postgresql+psycopg2://username:password@localhost/journal_d'


class Testing(Config):
    TESTING = True
    MAIL_SUPPRESS_SEND = True
    WTF_CSRF_ENABLED = False
    SERVER_NAME = 'localhost'
    SQLALCHEMY_DATABASE_URI = \
        'postgresql+psycopg2://username:password@localhost/journal_t'


class Production(Config):
    SESSION_PROTECTION = 'basic'
    SEND_FILE_MAX_AGE_DEFAULT = int(timedelta(days=28).total_seconds())
    LINKS_PER_PAGE = 30             # default is 3
    ALBUMS_PER_PAGE = 10            # default is 3
    POSTS_PER_PAGE = 30             # default is 3
    COMMENTS_PER_PAGE = 25          # default is 3
    PM_PER_PAGE = 30                # default is 3
    FRIENDS_PER_PAGE = 15           # default is 3
    ANNOUNCEMENTS_PER_PAGE = 10     # default is 3
    # SERVER_LOG_DIR = '/var/log/nginx/'
    # SERVER_LOG_FILE = 'access.log'
    # ERROR_LOG_DIR = '/var/log/gunicorn/'
    # ERROR_LOG_FILE' = 'error.log'
    # WORKER_LOG_DIR = '/var/log/celery/'
    # WORKER_LOG_FILE = 'worker.log'
    # BROKER_LOG_DIR = '/var/log/redis/'
    # BROKER_LOG_FILE = 'redis-server.log'
    SQLALCHEMY_DATABASE_URI = \
        'postgresql+psycopg2://username:password@localhost/journal'


config = {'development': Development,
          'testing': Testing,
          'production': Production,
          'default': Production}

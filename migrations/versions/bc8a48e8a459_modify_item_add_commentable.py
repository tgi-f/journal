"""Modify Item: add commentable

Revision ID: bc8a48e8a459
Revises: 181bfb7d95c3
Create Date: 2017-04-25 10:23:43.795334

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'bc8a48e8a459'
down_revision = '181bfb7d95c3'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('items', sa.Column('commentable', sa.Boolean(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('items', 'commentable')
    # ### end Alembic commands ###

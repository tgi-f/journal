"""Create Blocking

Revision ID: 92591ed7a741
Revises: 5d6d7f59d287
Create Date: 2017-04-27 10:43:56.753266

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '92591ed7a741'
down_revision = '5d6d7f59d287'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('blockings',
    sa.Column('blocker_id', sa.Integer(), nullable=False),
    sa.Column('blocked_id', sa.Integer(), nullable=False),
    sa.Column('since', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['blocked_id'], ['users.id'], ),
    sa.ForeignKeyConstraint(['blocker_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('blocker_id', 'blocked_id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('blockings')
    # ### end Alembic commands ###

"""Modify Message

Revision ID: 9282bb3acd89
Revises: 457a8da3a5c1
Create Date: 2017-06-19 10:28:00.817865

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9282bb3acd89'
down_revision = '457a8da3a5c1'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('messages', sa.Column('postponed', sa.Boolean(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('messages', 'postponed')
    # ### end Alembic commands ###

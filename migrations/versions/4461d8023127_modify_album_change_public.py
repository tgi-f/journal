"""Modify Album: change public

Revision ID: 4461d8023127
Revises: 4dd5af8a0435
Create Date: 2017-03-29 10:16:44.323747

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4461d8023127'
down_revision = '4dd5af8a0435'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('albums', 'public',
               existing_type=sa.BOOLEAN(),
               nullable=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('albums', 'public',
               existing_type=sa.BOOLEAN(),
               nullable=True)
    # ### end Alembic commands ###

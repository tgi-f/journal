from flask_script import Manager, Shell
from flask_migrate import Migrate, MigrateCommand
from sqlalchemy.exc import OperationalError, ProgrammingError

from celery_worker import app
from journal import db
from journal.manage_tools import get_email, get_password, get_username
from journal.models.announce import Announcement
from journal.models.auth import (
    Account, Blocking, Follow, Group, Permission, User)
from journal.models.auth_units import permissions, groups
from journal.models.blogs import Item
from journal.models.captcha import Captcha
from journal.models.commentaries import Commentary
from journal.models.labels import Label
from journal.models.links import Link
from journal.models.lists import List, Section
from journal.models.pictures import Album, Picture
from journal.models.pm import Message


manager = Manager(app)
migrate = Migrate(app, db)


def make_shell_context():
    return {'app': app, 'db': db, 'permissions': permissions,
            'groups': groups, 'Group': Group, 'Permission': Permission,
            'Account': Account, 'User': User, 'Announcement': Announcement,
            'Album': Album, 'Picture': Picture, 'Item': Item,
            'Follow': Follow, 'Blocking': Blocking, 'List': List,
            'Section': Section, 'Label': Label, 'Commentary': Commentary,
            'Message': Message, 'Link': Link, 'Captcha': Captcha}


manager.add_command('shell', Shell(make_context=make_shell_context))
manager.add_command('db', MigrateCommand)


@manager.command
def test():
    import unittest
    suite = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(suite)


ERROR_MESSAGE = """Make sure your database and its tables exist.
Check your 'config.py' then use 'db upgrade' command and then try again."""


@manager.command
def create_root():
    try:
        Group.insert_groups(permissions, groups, Permission)
    except (OperationalError, ProgrammingError):
        print(ERROR_MESSAGE)
        return None
    username, address, password = get_username(), get_email(), get_password()
    account = Account.query.filter_by(address=address).first()
    if account is None:
        account = Account(address=address)
    group = Group.query.filter_by(title=groups.root).first()
    root = User(username=username, password=password)
    root.account = account
    root.group = group
    db.session.add(root)
    db.session.commit()


if __name__ == '__main__':
    manager.run()

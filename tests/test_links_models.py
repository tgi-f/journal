from datetime import datetime
from string import ascii_letters, digits

from sqlalchemy.exc import IntegrityError

from journal import db
from journal.models.links import Link
from journal.models.exc import BadRequiredArg, NoneRequiredArg
from tests.cases import Models


class Links(Models):
    def test_link_there_is_no_links_initially(self):
        self.assertEqual(Link.query.count(), 0)

    def test_link_link_creation(self):
        link = self.create_link('http://yandex.ru')
        self.assertIsNotNone(Link.query.get(link.id))

    def test_link_raises_without_required_argument(self):
        with self.assertRaises(NoneRequiredArg):
            Link()

    def test_link_raises_with_bad_url(self):
        with self.assertRaises(BadRequiredArg):
            Link(url='')
        with self.assertRaises(BadRequiredArg):
            Link(url=None)

    def test_link_link_spring_is_not_none(self):
        link = self.create_link('http://yandex.ru')
        self.assertIsNotNone(link.spring)

    def test_link_link_spring_composition(self):
        link = self.create_link('http://yandex.ru')
        for each in link.spring:
            self.assertIn(each, ascii_letters + digits)

    def test_link_link_spring_uniqueness(self):
        link_1 = self.create_link('http://yandex.ru')
        link_2 = self.create_link('http://google.com')
        self.assertNotEqual(link_1.spring, link_2.spring)

    def test_link_link_spring_uniqueness_in_database(self):
        link_1 = Link(url='http://yandex.ru')
        link_2 = Link(url='http://google.com')
        link_1.spring = link_2.spring
        with self.assertRaises(IntegrityError):
            db.session.add(link_1)
            db.session.add(link_2)
            db.session.commit()

    def test_link_link_created_is_not_none(self):
        link = self.create_link('http://yandex.ru')
        self.assertIsNotNone(link.created)

    def test_link_link_created(self):
        link = self.create_link('http://yandex.ru')
        another = self.create_link('http://google.com')
        self.assertTrue(
            (datetime.utcnow() - link.created).total_seconds() < 3)
        self.assertNotEqual(link.created, another.created)

    def test_link_link_clicked_is_not_none(self):
        link = self.create_link('http://yandex.ru')
        self.assertIsNotNone(link.clicked)

    def test_link_link_clicked_initially(self):
        link = self.create_link('http://yandex.ru')
        self.assertEqual(link.clicked, 0)

    def test_link_link_owner_can_be_none(self):
        link = self.create_link('http://yandex.ru')
        self.assertIsNone(link.owner)

    def test_link_link_can_be_owned_by_user(self):
        actor = self.create_user('Actor', 'a@example.com', 'mm')
        link = self.create_owned_link(actor, 'http://yandex.ru')
        self.assertIs(link.owner, actor)

    def test_link_link_owner_url_uniqueness(self):
        actor = self.create_user('Actor', 'a@example.com', 'mm')
        self.create_owned_link(actor, 'http://yandex.ru')
        with self.assertRaises(IntegrityError):
            self.create_owned_link(actor, 'http://yandex.ru')

    def test_link_link_repeated_by_two_owners(self):
        actor = self.create_user('Actor', 'a@example.com', 'ss')
        vector = self.create_user('Vector', 'v@example.com', 'vv')
        url = 'http://yandex.com'
        a_link = self.create_owned_link(actor, url)
        v_link = self.create_owned_link(vector, url)
        self.assertEqual(a_link.url, v_link.url)
        self.assertNotEqual(a_link.spring, v_link.spring)

    def test_link_link_representation(self):
        link = self.create_link('http://yandex.ru')
        self.assertEqual(repr(link), '<Link: yandex.ru>')

    def test_link_link_alias(self):
        link = self.create_link('http://yandex.ru')
        self.assertNotIn('http://', link.gen_alias())
        self.assertIn(link.spring, link.gen_alias())

    def test_link_link_parse_url_with_short_url(self):
        link = self.create_link('http://yandex.ru')
        self.assertEqual(link.parse_url(), 'yandex.ru')

    def test_link_link_parse_url_with_long_url(self):
        url = 'http://avm.pythonanywhere.com/links/avm'
        link = self.create_link(url)
        self.assertEqual(len(link.parse_url()), 30)
        self.assertIn('~', link.parse_url())
        self.assertIn(link.parse_url()[:-1], url)

from time import sleep

from flask import url_for
from flask_login import current_user

from journal import db
from journal.models.auth import User
from tests.cases import Views


class LoginLogout(Views):
    bad_ = 'Неверный логин или пароль, вход невозможен.'

    def test_login_login_page_is_available(self):
        response = self.client.get(url_for('auth.login'))
        response_text = response.get_data(as_text=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<form class="form" method="POST" role="form">', response_text)
        self.assertIn(
            '<input class="form-control" id="login" name="login"',
            response_text)
        self.assertIn(
            '<input class="form-control" id="password" name="password"',
            response_text)

    def test_login_user_can_log_in_with_username(self):
        actor = self.create_user('Actor', 'a@example.com', 'ss')
        with self.client:
            response = self.log_the_user_in(actor.username, 'ss')
            self.assertEqual(current_user, actor)
            self.assertEqual(response.status_code, 200)

    def test_login_user_can_log_in_with_email_address(self):
        actor = self.create_user('Actor', 'a@example.com', 'aa')
        with self.client:
            response = self.log_the_user_in_email(actor.account.address, 'aa')
            self.assertEqual(current_user, actor)
            self.assertEqual(response.status_code, 200)

    def test_login_user_login_redirects_properly(self):
        actor = self.create_user('Actor', 'a@example.com', 'as')
        with self.client:
            response = self.log_the_user_in(
                actor.username, 'as', redirects=False)
            self.assertEqual(response.status_code, 302)
            self.assertEqual(
                url_for('main.index', _external=True),
                response.headers.get('Location'))

    def test_login_login_page_redirects_authenticated_users(self):
        self.create_user('Actor', 'a@example.com', 'aa')
        with self.client:
            self.log_the_user_in('Actor', 'aa')
            self.assertTrue(current_user.is_authenticated)
            response = self.client.get(url_for('auth.login'))
            self.assertEqual(response.status_code, 302)
            self.assertEqual(
                url_for('main.index', _external=True),
                response.headers.get('Location'))

    def test_login_unknown_user_cannot_log_in(self):
        with self.client:
            response = self.log_the_user_in('Stranger', 'ts')
            self.assertEqual(response.status_code, 200)
            self.assertFalse(current_user.is_authenticated)
            self.assertIn(self.bad_, response.get_data(as_text=True))

    def test_login_user_cannot_login_with_bad_password(self):
        actor = self.create_user('Actor', 'a@example.com', 'ss')
        with self.client:
            response = self.log_the_user_in(actor.username, 'mm')
            self.assertEqual(response.status_code, 200)
            self.assertTrue(current_user.is_anonymous)
            self.assertIn(self.bad_, response.get_data(as_text=True))

    def test_login_user_cannot_login_with_bad_username(self):
        self.create_user('Actor', 'a@example.com', 'mm')
        with self.client:
            response = self.log_the_user_in('Hector', 'mm')
            self.assertEqual(response.status_code, 200)
            self.assertTrue(current_user.is_anonymous)
            self.assertIn(self.bad_, response.get_data(as_text=True))

    def test_login_user_cannot_login_with_bad_address(self):
        self.create_user('Actor', 'a@example.com', 'mm')
        with self.client:
            response = self.log_the_user_in_email('b@example.com', 'mm')
            self.assertEqual(response.status_code, 200)
            self.assertTrue(current_user.is_anonymous)
            self.assertIn(self.bad_, response.get_data(as_text=True))

    def test_login_user_last_visit_changes_after_login(self):
        actor = self.create_user('Actor', 'a@example.com', 'ww')
        l_v = actor.last_visit
        self.log_the_user_in(actor.username, 'ww')
        self.assertGreater(actor.last_visit, l_v)

    def test_login_user_cannot_login_without_password(self):
        self.create_user('Actor', 'a@example.com', 'mm')
        with self.client:
            response = self.log_the_user_in('Actor', '')
            self.assertIn(
                '<p class="error">без пароля не пустит</p>',
                response.get_data(as_text=True))
            self.assertTrue(current_user.is_anonymous)

    def test_login_user_cannot_login_without_username_or_email(self):
        self.create_user('Actor', 'a@example.com', 'oo')
        with self.client:
            response = self.log_the_user_in('', 'oo')
            self.assertIn(
                '<p class="error">введите псевдоним или адрес почты</p>',
                response.get_data(as_text=True))
            self.assertTrue(current_user.is_anonymous)

    def test_login_pariah_cannot_log_in(self):
        actor = self.create_user(
            'Actor', 'a@example.com', 'ss', group_title=self.groups.pariah)
        with self.client:
            response = self.log_the_user_in(actor.username, 'ss')
            response_text = response.get_data(as_text=True)
            self.assertEqual(response.status_code, 200)
            self.assertTrue(current_user.is_anonymous)
        # make sure the pariah user turns out on the login page again
        self.assertIn(
            'Ваше присутствие в сервисе нежелательно.',
            response.get_data(as_text=True))
        self.assertIn(
            '<form class="form" method="POST" role="form">', response_text)
        self.assertIn(
            '<input class="form-control" id="login" name="login"',
            response_text)
        self.assertIn(
            '<input class="form-control" id="password" name="password"',
            response_text)

    def test_login_taciturn_can_log_in(self):
        actor = self.create_user(
            'Actor', 'a@example.com', 'ss', group_title=self.groups.taciturn)
        with self.client:
            response = self.log_the_user_in(actor.username, 'ss')
            self.assertEqual(response.status_code, 200)
            self.assertEqual(current_user, actor)

    def test_login_commentator_can_log_in(self):
        actor = self.create_user(
            'Actor', 'a@example.com', 'ss', group_title=self.groups.commentator)
        with self.client:
            response = self.log_the_user_in(actor.username, 'ss')
            self.assertEqual(response.status_code, 200)
            self.assertEqual(current_user, actor)

    def test_login_curator_can_log_in(self):
        actor = self.create_user(
            'Actor', 'a@example.com', 'ss', group_title=self.groups.curator)
        with self.client:
            response = self.log_the_user_in(actor.username, 'ss')
            self.assertEqual(response.status_code, 200)
            self.assertEqual(current_user, actor)

    def test_login_keeper_can_log_in(self):
        actor = self.create_user(
            'Actor', 'a@example.com', 'ss', group_title=self.groups.keeper)
        with self.client:
            response = self.log_the_user_in(actor.username, 'ss')
            self.assertEqual(response.status_code, 200)
            self.assertEqual(current_user, actor)

    def test_login_root_can_log_in(self):
        actor = self.create_user(
            'Actor', 'a@example.com', 'ss', group_title=self.groups.root)
        with self.client:
            response = self.log_the_user_in(actor.username, 'ss')
            self.assertEqual(response.status_code, 200)
            self.assertEqual(current_user, actor)

    def test_logout_user_can_logout(self):
        self.create_user('Actor', 'a@example.com', 'wi')
        with self.client:
            self.log_the_user_in('Actor', 'wi')
            self.assertTrue(current_user.is_authenticated)
            response = self.client.get(url_for('auth.logout'))
            self.assertTrue(current_user.is_anonymous)
            self.assertEqual(response.status_code, 302)
            self.assertEqual(
                url_for('main.index', _external=True),
                response.headers.get('Location'))


class GetPassword(Views):
    success_ = 'На Ваш адрес выслано письмо с инструкциями.'

    def test_get_password_page_by_anonymous_user(self):
        response = self.client.get(url_for('auth.get_password'))
        response_text = response.get_data(as_text=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<form class="form" method="POST" role="form">',
            response_text)
        self.assertIn(
            '<input class="form-control" id="address" name="address"',
            response_text)

    def test_get_password_page_by_authenticated_user(self):
        actor = self.create_user('Actor', 'a@example.com', 'ss')
        with self.client:
            self.log_the_user_in(actor.username, 'ss')
            self.assertTrue(current_user.is_authenticated)
            response = self.client.get(url_for('auth.get_password'))
            self.assertEqual(response.status_code, 302)
            self.assertEqual(
                url_for('main.index', _external=True),
                response.headers.get('Location'))

    def test_get_password_with_currently_created_account_address(self):
        actor = self.create_user('Actor', 'a@example.com', 'ss')
        response = self.client.post(
            url_for('auth.get_password'),
            data={'address': actor.account.address},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            'Сервис временно недоступен, попробуйте зайти позже.',
            response.get_data(as_text=True))

    def test_get_password_with_currently_created_acc_redirects_properly(self):
        actor = self.create_user('Actor', 'a@example.com', 'sm')
        response = self.client.post(
            url_for('auth.get_password'),
            data={'address': actor.account.address},
            follow_redirects=False)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            url_for('auth.get_password', _external=True),
            response.headers.get('Location'))

    def test_get_password_with_swapped_address(self):
        vector = self.create_user('Vector', 'v@example.com', 'ss')
        vector.account.swap = 'a@example.com'
        db.session.add(vector)
        db.session.commit()
        response = self.client.post(
            url_for('auth.get_password'),
            data={'address': 'a@example.com'},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            'Адрес в свопе, выберите другой или попробуйте позже.',
            response.get_data(as_text=True))

    def test_get_password_with_swapped_address_redirects_properly(self):
        vector = self.create_user('Vector', 'v@example.com', 'ss')
        vector.account.swap = 'a@example.com'
        db.session.add(vector)
        db.session.commit()
        response = self.client.post(
            url_for('auth.get_password'),
            data={'address': 'a@example.com'},
            follow_redirects=False)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            url_for('auth.get_password', _external=True),
            response.headers.get('Location'))

    def test_get_password_with_unknown_address(self):
        response = self.client.post(
            url_for('auth.get_password'),
            data={'address': 'a@example.com'},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            self.success_, response.get_data(as_text=True))

    def test_get_password_with_unknown_address_redirects_properly(self):
        response = self.client.post(
            url_for('auth.get_password'),
            data={'address': 'a@example.com'},
            follow_redirects=False)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            url_for('auth.login', _external=True),
            response.headers.get('Location'))

    def test_get_password_with_old_account_address(self):
        actor = self.create_old_user('Actor', 'a@example.com', 'ss')
        response = self.client.post(
            url_for('auth.get_password'),
            data={'address': actor.account.address},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            self.success_, response.get_data(as_text=True))

    def test_get_password_with_old_account_address_redirects_properly(self):
        actor = self.create_old_user('Actor', 'a@example.com', 'ss')
        response = self.client.post(
            url_for('auth.get_password'),
            data={'address': actor.account.address},
            follow_redirects=False)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            url_for('auth.login', _external=True),
            response.headers.get('Location'))


class ResetPassword(Views):
    def test_reset_password_with_random_token(self):
        response = self.client.get(
            url_for('auth.reset_password', token='token'))
        self.assertEqual(response.status_code, 404)

    def test_reset_password_with_expired_token(self):
        actor = self.create_user('Actor', 'a@example.com', 'ss')
        token = actor.account.generate_token(expiration=1)
        sleep(2)
        response = self.client.get(url_for('auth.reset_password', token=token))
        self.assertEqual(response.status_code, 404)

    def test_reset_password_with_tainted_token(self):
        actor = self.create_user('Actor', 'a@example.com', 'ss')
        token = actor.account.generate_token()
        db.session.delete(actor)
        db.session.commit()
        response = self.client.get(url_for('auth.reset_password', token=token))
        self.assertEqual(response.status_code, 404)

    def test_reset_password_with_tainted_token_again(self):
        actor = self.create_user('Actor', 'a@example.com', 'sld')
        token = actor.account.generate_token()
        db.session.delete(actor.account)
        db.session.commit()
        response = self.client.get(url_for('auth.reset_password', token=token))
        self.assertEqual(response.status_code, 404)

    def test_reset_password_with_actual_token(self):
        actor = self.create_user('Actor', 'a@example.com', 'ss')
        token = actor.account.generate_token()
        response = self.client.get(url_for('auth.reset_password', token=token))
        response_text = response.get_data(as_text=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<h3 class="panel-title">Сброс пароля</h3>', response_text)
        self.assertIn(
            '<form class="form" method="POST" role="form">', response_text)

    def test_reset_password_with_actual_token_swapped_account(self):
        actor = self.create_user('Actor', 'a@example.com', 'ss')
        actor.account.swap = 'b@example.com'
        db.session.add(actor)
        db.session.commit()
        token = actor.account.generate_token()
        response = self.client.get(url_for('auth.reset_password', token=token))
        self.assertEqual(response.status_code, 404)

    def test_reset_password_with_actual_token_by_authenticated_user(self):
        actor = self.create_user('Actor', 'a@example.com', 'ss')
        with self.client:
            self.log_the_user_in(actor.username, 'ss')
            response = self.client.get(
                url_for(
                    'auth.reset_password',
                    token=actor.account.generate_token()))
            self.assertEqual(response.status_code, 404)

    def test_reset_password_with_actual_token_repeated_access(self):
        actor = self.create_user('Actor', 'a@example.com', 'sd')
        token = actor.account.generate_token()
        new_password = 'mFc234'
        self.client.post(
            url_for('auth.reset_password', token=token),
            data={'address': 'a@example.com',
                  'password': new_password,
                  'confirmation': new_password},
            follow_redirects=True)
        self.assertTrue(actor.verify_password(new_password))
        response = self.client.get(
            url_for('auth.reset_password', token=token))
        self.assertEqual(response.status_code, 404)

    def test_reset_password_with_empty_form(self):
        actor = self.create_user('Actor', 'a@example.com', 'dd')
        p_hash = actor.password_hash
        token = actor.account.generate_token()
        response = self.client.post(
            url_for('auth.reset_password', token=token),
            data={'address': '',
                  'password': '',
                  'confirmation': ''},
            follow_redirects=True)
        response_text = response.get_data(as_text=True)
        self.assertIn(
            '<p class="error">без адреса никак, увы</p>', response_text)
        self.assertIn(
            '<p class="error">без пароля никак, увы</p>', response_text)
        self.assertEqual(actor.password_hash, p_hash)

    def test_reset_password_with_bad_email(self):
        actor = self.create_user('Actor', 'a@example.com', 'dd')
        p_hash = actor.password_hash
        token = actor.account.generate_token()
        response = self.client.post(
            url_for('auth.reset_password', token=token),
            data={'address': 'b@example.com',
                  'password': 'stMF8cWm',
                  'confirmation': 'stMF8cWm'},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            'Неверный запрос, действие отклонено.',
            response.get_data(as_text=True))
        self.assertEqual(p_hash, actor.password_hash)

    def test_reset_password_with_bad_email_redirection(self):
        actor = self.create_user('Actor', 'a@example.com', 'dd')
        token = actor.account.generate_token()
        response = self.client.post(
            url_for('auth.reset_password', token=token),
            data={'address': 'b@example.com',
                  'password': 'stMF8cWm',
                  'confirmation': 'stMF8cWm'},
            follow_redirects=False)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            url_for('auth.login', _external=True),
            response.headers.get('Location'))

    def test_reset_password_with_no_email(self):
        actor = self.create_user('Actor', 'a@example.com', 'dd')
        p_hash = actor.password_hash
        token = actor.account.generate_token()
        response = self.client.post(
            url_for('auth.reset_password', token=token),
            data={'address': '',
                  'password': 'stMF8cWm',
                  'confirmation': 'stMF8cWm'},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<p class="error">без адреса никак, увы</p>',
            response.get_data(as_text=True))
        self.assertEqual(p_hash, actor.password_hash)

    def test_reset_password_with_unmatched_passwords(self):
        actor = self.create_user('Actor', 'a@example.com', 'dd')
        p_hash = actor.password_hash
        token = actor.account.generate_token()
        response = self.client.post(
            url_for('auth.reset_password', token=token),
            data={'address': 'a@example.com',
                  'password': 'ss',
                  'confirmation': 'aa'},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<p class="error">пароли не совпадают</p>',
            response.get_data(as_text=True))
        self.assertEqual(p_hash, actor.password_hash)

    def test_reset_password_with_proper_data(self):
        old_password = 'dd'
        actor = self.create_user('Actor', 'a@example.com', old_password)
        token = actor.account.generate_token()
        new_password = 'stMF8cWm'
        response = self.client.post(
            url_for('auth.reset_password', token=token),
            data={'address': 'a@example.com',
                  'password': new_password,
                  'confirmation': new_password},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            'У Вас новый пароль.',
            response.get_data(as_text=True))
        self.assertFalse(actor.verify_password(old_password))
        self.assertTrue(actor.verify_password(new_password))

    def test_reset_password_with_proper_data_redirection(self):
        actor = self.create_user('Actor', 'a@example.com', 'dd')
        token = actor.account.generate_token()
        new_password = 'stMF8cWm'
        response = self.client.post(
            url_for('auth.reset_password', token=token),
            data={'address': 'a@example.com',
                  'password': new_password,
                  'confirmation': new_password},
            follow_redirects=False)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            url_for('auth.login', _external=True),
            response.headers.get('Location'))


class CreatePassword(Views):
    def test_create_password_with_random_token(self):
        response = self.client.get(
            url_for('auth.create_password', token='token'))
        self.assertEqual(response.status_code, 404)

    def test_create_password_with_expired_token(self):
        account = self.create_account('a@example.com')
        token = account.generate_token(expiration=1)
        sleep(2)
        response = self.client.get(url_for('auth.create_password', token=token))
        self.assertEqual(response.status_code, 404)

    def test_create_password_with_tainted_token(self):
        account = self.create_account('a@example.com')
        token = account.generate_token()
        db.session.delete(account)
        db.session.commit()
        response = self.client.get(url_for('auth.create_password', token=token))
        self.assertEqual(response.status_code, 404)

    def test_create_password_with_registered_account_token(self):
        actor = self.create_user('Actor', 'a@example.com', 'mm')
        token = actor.account.generate_token()
        response = self.client.get(url_for('auth.create_password', token=token))
        self.assertEqual(response.status_code, 404)

    def test_create_password_with_actual_token_by_authenticated_user(self):
        account = self.create_account('b@example.com')
        token = account.generate_token()
        actor = self.create_user('Actor', 'a@example.com', 'mm')
        with self.client:
            self.log_the_user_in(actor.username, 'mm')
            response = self.client.get(
                url_for('auth.create_password', token=token))
            self.assertEqual(response.status_code, 404)

    def test_create_password_with_actual_token(self):
        account = self.create_account('a@example.com')
        response = self.client.get(
            url_for('auth.create_password', token=account.generate_token()))
        response_text = response.get_data(as_text=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<form class="form" method="POST" role="form">', response_text)
        self.assertIn(
            '<input class="form-control" id="username" name="username"',
            response_text)
        self.assertIn(
            '<input class="form-control" id="password" name="password"',
            response_text)

    def test_create_password_with_proper_data(self):
        username, password = 'Actor', 's3w2MVt7'
        account = self.create_account('a@example.com')
        response = self.client.post(
            url_for('auth.create_password', token=account.generate_token()),
            data={'username': username,
                  'password': password,
                  'confirmation': password},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            'Для пользователя {} пароль успешно создан.'.format(username),
            response.get_data(as_text=True))
        actor = User.query.filter_by(username=username).first()
        self.assertIsNotNone(actor)
        self.assertTrue(actor.verify_password(password))

    def test_create_password_with_proper_data_redirection(self):
        account = self.create_account('a@example.com')
        response = self.client.post(
            url_for('auth.create_password', token=account.generate_token()),
            data={'username': 'Actor',
                  'password': 's3w2MVt7',
                  'confirmation': 's3w2MVt7'},
            follow_redirects=False)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            url_for('auth.login', _external=True),
            response.headers.get('Location'))

    def test_create_password_with_empty_form(self):
        account = self.create_account('a@example.com')
        response = self.client.post(
            url_for('auth.create_password', token=account.generate_token()),
            data={'username': '',
                  'password': '',
                  'confirmation': ''},
            follow_redirects=True)
        response_text = response.get_data(as_text=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<p class="error">без псевдонима никак, увы</p>', response_text)
        self.assertIn(
            '<p class="error">без пароля никак, увы</p>', response_text)
        self.assertEqual(User.query.count(), 0)

    def test_create_password_with_too_short_username(self):
        account = self.create_account('a@example.com')
        response = self.client.post(
            url_for('auth.create_password', token=account.generate_token()),
            data={'username': 'A',
                  'password': 'aa',
                  'confirmation': 'aa'},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<p class="error">от 3-х до 16 знаков</p>',
            response.get_data(as_text=True))
        self.assertEqual(User.query.count(), 0)

    def test_create_password_with_too_long_username(self):
        username = 'Actor123actor_AD-'
        account = self.create_account('a@example.com')
        response = self.client.post(
            url_for('auth.create_password', token=account.generate_token()),
            data={'username': username,
                  'password': 'aa',
                  'confirmation': 'aa'},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<p class="error">от 3-х до 16 знаков</p>',
            response.get_data(as_text=True))
        self.assertEqual(User.query.count(), 0)

    def test_create_password_with_bad_username(self):
        username = '1Actor'
        account = self.create_account('a@example.com')
        response = self.client.post(
            url_for('auth.create_password', token=account.generate_token()),
            data={'username': username,
                  'password': 'aa',
                  'confirmation': 'aa'},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<p class="error">латинские буквы, цифры, дефис, знак',
            response.get_data(as_text=True))
        self.assertEqual(User.query.count(), 0)

    def test_create_password_with_registered_username(self):
        username = 'Actor'
        self.create_user(username, 'a@example.com', 'mm')
        account = self.create_account('aa@example.com')
        response = self.client.post(
            url_for('auth.create_password', token=account.generate_token()),
            data={'username': username,
                  'password': 'aa',
                  'confirmation': 'aa'},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<p class="error">этот псевдоним уже используется</p>',
            response.get_data(as_text=True))
        self.assertEqual(User.query.count(), 1)

    def test_create_password_with_unmatched_passwords(self):
        account = self.create_account('a@example.com')
        response = self.client.post(
            url_for('auth.create_password', token=account.generate_token()),
            data={'username': 'Actor',
                  'password': 'ss',
                  'confirmation': 'aa'},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<p class="error">пароли не совпадают</p>',
            response.get_data(as_text=True))
        self.assertEqual(User.query.count(), 0)


class ChangePassword(Views):
    def test_change_password_by_anonymous_user(self):
        response = self.client.get(url_for('auth.change_password'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            url_for(
                'auth.login',
                next=url_for('auth.change_password', _external=False),
                _external=True),
            response.headers.get('Location'))

    def test_change_password_by_registered_user(self):
        actor = self.create_user('Actor', 'a@example.com', 'smd')
        self.log_the_user_in(actor.username, 'smd')
        response = self.client.get(url_for('auth.change_password'))
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<form class="form" method="POST" role="form">',
            response.get_data(as_text=True))

    def test_change_password_with_bad_current_password(self):
        current, new = 'smd', 'ms'
        actor = self.create_user('Actor', 'a@example.com', current)
        self.log_the_user_in(actor.username, current)
        response = self.client.post(
            url_for('auth.change_password'),
            data={'current': 'vfg',
                  'password': new,
                  'confirmation': new},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<p class="error">текущий пароль недействителен</p>',
            response.get_data(as_text=True))
        self.assertTrue(actor.verify_password(current))
        self.assertFalse(actor.verify_password(new))

    def test_change_password_with_unmatched_passwords(self):
        current, new = 'f2tM', 'zSar'
        actor = self.create_user('Actor', 'a@example.com', current)
        self.log_the_user_in(actor.username, current)
        response = self.client.post(
            url_for('auth.change_password'),
            data={'current': current,
                  'password': new,
                  'confirmation': 'dwl'},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<p class="error">пароли не совпадают</p>',
            response.get_data(as_text=True))
        self.assertTrue(actor.verify_password(current))

    def test_change_password_with_empty_form(self):
        actor = self.create_user('Actor', 'a@example.com', 'aa')
        self.log_the_user_in(actor.username, 'aa')
        response = self.client.post(
            url_for('auth.change_password'),
            data={'current': '',
                  'password': '',
                  'confirmation': ''},
            follow_redirects=True)
        response_text = response.get_data(as_text=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<p class="error">без пароля не пустит</p>', response_text)
        self.assertIn(
            '<p class="error">без пароля никак, увы</p>', response_text)

    def test_change_password_with_proper_data(self):
        current, new = 'f2tM', 'zSar'
        actor = self.create_user('Actor', 'a@example.com', current)
        self.log_the_user_in(actor.username, current)
        response = self.client.post(
            url_for('auth.change_password'),
            data={'current': current,
                  'password': new,
                  'confirmation': new},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            'Уважаемый {}, у Вас новый пароль.'.format(actor.username),
            response.get_data(as_text=True))
        self.assertTrue(actor.verify_password(new))
        self.assertFalse(actor.verify_password(current))

    def test_change_password_with_proper_data_redirection(self):
        current, new = 'f2tM', 'zSar'
        actor = self.create_user('Actor', 'a@example.com', current)
        self.log_the_user_in(actor.username, current)
        response = self.client.post(
            url_for('auth.change_password'),
            data={'current': current,
                  'password': new,
                  'confirmation': new},
            follow_redirects=False)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            url_for(
                'main.show_profile', username=actor.username, _external=True),
            response.headers.get('Location'))


class RequestEmail(Views):
    def test_request_email_by_anonymous_user(self):
        response = self.client.get(url_for('auth.request_email'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            url_for(
                'auth.login',
                next=url_for('auth.request_email', _external=False),
                _external=True),
            response.headers.get('Location'))

    def test_request_email_by_registered_user(self):
        actor = self.create_user('Actor', 'a@example.com', 'ss')
        self.log_the_user_in(actor.username, 'ss')
        response = self.client.get(url_for('auth.request_email'))
        response_text = response.get_data(as_text=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<form class="form" method="POST" role="form">', response_text)
        self.assertIn(
            '<input class="form-control" id="swap" name="swap"', response_text)

    def test_request_email_by_recently_registered_user(self):
        actor = self.create_user('Actor', 'a@example.com', 'mm')
        self.log_the_user_in(actor.username, 'mm')
        response = self.client.post(
            url_for('auth.request_email'),
            data={'swap': 'b@example.com'},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            'Сервис временно недоступен, попробуйте позже.',
            response.get_data(as_text=True))

    def test_request_email_by_recently_registered_user_redirection(self):
        actor = self.create_user('Actor', 'a@example.com', 'mm')
        self.log_the_user_in(actor.username, 'mm')
        response = self.client.post(
            url_for('auth.request_email'),
            data={'swap': 'b@example.com'},
            follow_redirects=False)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            url_for('auth.request_email', _external=True),
            response.headers.get('Location'))

    def test_request_email_with_current_user_email(self):
        current = 'a@example.com'
        actor = self.create_old_user('Actor', current, 'mm')
        self.log_the_user_in(actor.username, 'mm')
        response = self.client.post(
            url_for('auth.request_email'),
            data={'swap': current},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            'Задан Ваш текущий адрес, запрос не имеет смысла.',
            response.get_data(as_text=True))

    def test_request_email_with_current_user_email_redirection(self):
        current = 'a@example.com'
        actor = self.create_old_user('Actor', current, 'mm')
        self.log_the_user_in(actor.username, 'mm')
        response = self.client.post(
            url_for('auth.request_email'),
            data={'swap': current},
            follow_redirects=False)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            url_for('auth.request_email', _external=True),
            response.headers.get('Location'))

    def test_request_email_with_swapped_email(self):
        swapped = 'a@example.com'
        swapper = self.create_user('Hector', 'h@example.com', 'mms')
        swapper.account.swap = swapped
        db.session.add(swapper)
        db.session.commit()
        actor = self.create_old_user('Actor', 'm@example.com', 'vt')
        self.log_the_user_in(actor.username, 'vt')
        response = self.client.post(
            url_for('auth.request_email'),
            data={'swap': swapped},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            'Адрес в свопе, выберите другой или попробуйте позже.',
            response.get_data(as_text=True))

    def test_request_email_with_swapped_email_redirection(self):
        swapped = 'a@example.com'
        swapper = self.create_user('Hector', 'h@example.com', 'mms')
        swapper.account.swap = swapped
        db.session.add(swapper)
        db.session.commit()
        actor = self.create_old_user('Actor', 'm@example.com', 'vt')
        self.log_the_user_in(actor.username, 'vt')
        response = self.client.post(
            url_for('auth.request_email'),
            data={'swap': swapped},
            follow_redirects=False)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            url_for('auth.request_email', _external=True),
            response.headers.get('Location'))

    def test_request_email_with_registered_email(self):
        registered = 'h@example.com'
        self.create_user('Hector', registered, 'ww')
        actor = self.create_old_user('Actor', 'a@example.com', 'ms')
        self.log_the_user_in(actor.username, 'ms')
        response = self.client.post(
            url_for('auth.request_email'),
            data={'swap': registered},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            'Этот адрес уже зарегистрирован, запрос отклонён.',
            response.get_data(as_text=True))

    def test_request_email_with_registered_email_redirection(self):
        registered = 'h@example.com'
        self.create_user('Hector', registered, 'ww')
        actor = self.create_old_user('Actor', 'a@example.com', 'ms')
        self.log_the_user_in(actor.username, 'ms')
        response = self.client.post(
            url_for('auth.request_email'),
            data={'swap': registered},
            follow_redirects=False)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            url_for('auth.request_email', _external=True),
            response.headers.get('Location'))

    def test_request_email_with_requested_email(self):
        requested = self.create_account('a@example.com')
        actor = self.create_old_user('Actor', 'w@example.com', 'ss')
        self.log_the_user_in(actor.username, 'ss')
        response = self.client.post(
            url_for('auth.request_email'),
            data={'swap': requested.address},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            'На {} отослано сообщение с инструкциями.'.format(
                requested.address),
            response.get_data(as_text=True))

    def test_request_email_with_requested_email_redirection(self):
        requested = self.create_account('a@example.com')
        actor = self.create_old_user('Actor', 'w@example.com', 'ss')
        self.log_the_user_in(actor.username, 'ss')
        response = self.client.post(
            url_for('auth.request_email'),
            data={'swap': requested.address},
            follow_redirects=False)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            url_for(
                'main.show_profile', username=actor.username, _external=True),
            response.headers.get('Location'))

    def test_request_email_with_unknown_email(self):
        actor = self.create_old_user('Actor', 'w@example.com', 'ss')
        self.log_the_user_in(actor.username, 'ss')
        response = self.client.post(
            url_for('auth.request_email'),
            data={'swap': 'a@example.com'},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            'На a@example.com отослано сообщение с инструкциями.',
            response.get_data(as_text=True))

    def test_request_email_with_unknown_email_redirection(self):
        actor = self.create_old_user('Actor', 'w@example.com', 'ss')
        self.log_the_user_in(actor.username, 'ss')
        response = self.client.post(
            url_for('auth.request_email'),
            data={'swap': 'a@example.com'},
            follow_redirects=False)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            url_for(
                'main.show_profile', username=actor.username, _external=True),
            response.headers.get('Location'))


class ChangeEmail(Views):
    def test_change_email_by_anonymous_user(self):
        response = self.client.get(url_for('auth.change_email', token='token'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            url_for(
                'auth.login',
                next=url_for(
                    'auth.change_email', token='token', _external=False),
                _external=True),
            response.headers.get('Location'))

    def test_change_email_with_random_token(self):
        actor = self.create_user('Actor', 'b@example.com', 'sm')
        self.swap_account(actor.account, 'a@example.com')
        self.log_the_user_in(actor.username, 'sm')
        response = self.client.get(url_for('auth.change_email', token='token'))
        self.assertEqual(response.status_code, 404)

    def test_change_email_with_strange_actual_token(self):
        vector = self.create_user('Vector', 'v@example.com', 'mm')
        token = vector.account.generate_token()
        actor = self.create_user('Actor', 'aa@example.com', 'aa')
        self.swap_account(actor.account, 'a@example.com')
        self.log_the_user_in(actor.username, 'aa')
        response = self.client.get(url_for('auth.change_email', token=token))
        self.assertEqual(response.status_code, 404)

    def test_change_email_with_actual_token_no_swap(self):
        actor = self.create_user('Actor', 'a@example.com', 'ss')
        token = actor.account.generate_token()
        self.log_the_user_in(actor.username, 'ss')
        response = self.client.get(url_for('auth.change_email', token=token))
        self.assertEqual(response.status_code, 404)

    def test_change_email_with_expired_token(self):
        actor = self.create_user('Actor', 'a@example.com', 'we')
        self.swap_account(actor.account, 'aa@example.com')
        token = actor.account.generate_token(expiration=1)
        sleep(2)
        self.log_the_user_in(actor.username, 'we')
        response = self.client.get(url_for('auth.change_email', token=token))
        self.assertEqual(response.status_code, 404)

    def test_change_email_with_actual_token(self):
        swap = 'aa@example.com'
        actor = self.create_user('Actor', 'a@example.com', 'we')
        self.swap_account(actor.account, swap)
        token = actor.account.generate_token()
        self.log_the_user_in(actor.username, 'we')
        response = self.client.get(url_for('auth.change_email', token=token))
        response_text = response.get_data(as_text=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(swap, response_text)
        self.assertIn(
            '<form class="form" method="POST" role="form">', response_text)
        self.assertIn(
            '<input class="form-control" id="password" name="password"',
            response_text)

    def test_change_email_with_actual_token_and_password(self):
        swap, password = 'aa@example.com', 'eRt8m'
        actor = self.create_user('Actor', 'a@example.com', password)
        self.swap_account(actor.account, swap)
        token = actor.account.generate_token()
        self.log_the_user_in(actor.username, password)
        response = self.client.post(
            url_for('auth.change_email', token=token),
            data={'password': password},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            'Адрес в Вашем аккаунте успешно изменён.',
            response.get_data(as_text=True))
        self.assertEqual(actor.account.address, swap)
        self.assertIsNone(actor.account.swap)

    def test_change_email_with_actual_token_and_password_redirection(self):
        swap, password = 'aa@example.com', 'eRt8m'
        actor = self.create_user('Actor', 'a@example.com', password)
        self.swap_account(actor.account, swap)
        token = actor.account.generate_token()
        self.log_the_user_in(actor.username, password)
        response = self.client.post(
            url_for('auth.change_email', token=token),
            data={'password': password},
            follow_redirects=False)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            url_for(
                'main.show_profile', username=actor.username, _external=True),
            response.headers.get('Location'))

    def test_change_email_with_actual_token_and_no_password(self):
        swap, password = 'aa@example.com', 'eRt8m'
        actor = self.create_user('Actor', 'a@example.com', password)
        self.swap_account(actor.account, swap)
        token = actor.account.generate_token()
        self.log_the_user_in(actor.username, password)
        response = self.client.post(
            url_for('auth.change_email', token=token),
            data={'password': ''},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<p class="error">без пароля не пустит</p>',
            response.get_data(as_text=True))
        self.assertEqual(actor.account.swap, swap)

    def test_change_email_with_actual_token_and_bad_password(self):
        swap = 'aa@example.com'
        actor = self.create_user('Actor', 'a@example.com', 'eRt8m')
        self.swap_account(actor.account, swap)
        token = actor.account.generate_token()
        self.log_the_user_in(actor.username, 'eRt8m')
        response = self.client.post(
            url_for('auth.change_email', token=token),
            data={'password': 'ss'},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<p class="error">текущий пароль недействителен</p>',
            response.get_data(as_text=True))
        self.assertEqual(actor.account.swap, swap)

from flask import url_for
from flask_login import current_user

from tests.cases import Views


class Jump(Views):
    def test_jump_with_random_spring(self):
        response = self.client.get(url_for('main.jump', spring='random'))
        self.assertEqual(response.status_code, 404)

    def test_jump_by_anonymous_user(self):
        actor = self.create_user('Actor', 'a@example.com', 'ms')
        alias = self.create_owned_link(actor, 'http://example.com')
        response = self.client.get(url_for('main.jump', spring=alias.spring))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(alias.url, response.headers.get('Location'))
        self.assertEqual(alias.clicked, 1)

    def test_jump_by_authenticated_user_with_foreign_spring(self):
        vector = self.create_user('Vector', 'v@example.com', 'mw')
        alias = self.create_owned_link(vector, 'http://example.com')
        actor = self.create_user('Actor', 'a@example.com', 'ww')
        with self.client:
            self.log_the_user_in(actor.username, 'ww')
            self.assertEqual(current_user.username, actor.username)
        response = self.client.get(url_for('main.jump', spring=alias.spring))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(alias.url, response.headers.get('Location'))
        self.assertEqual(alias.clicked, 1)

    def test_jump_repeated_jump_by_anonymous_user(self):
        actor = self.create_user('Actor', 'a@example.com', 'ms')
        alias = self.create_owned_link(actor, 'http://example.com')
        self.client.get(url_for('main.jump', spring=alias.spring))
        response = self.client.get(url_for('main.jump', spring=alias.spring))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(alias.url, response.headers.get('Location'))
        self.assertEqual(alias.clicked, 1)

    def test_jump_repeated_jump_by_authenticated_user_with_foreign_spring(self):
        vector = self.create_user('Vector', 'v@example.com', 'mw')
        alias = self.create_owned_link(vector, 'http://example.com')
        actor = self.create_user('Actor', 'a@example.com', 'ww')
        with self.client:
            self.log_the_user_in(actor.username, 'ww')
            self.assertEqual(current_user.username, actor.username)
        self.client.get(url_for('main.jump', spring=alias.spring))
        response = self.client.get(url_for('main.jump', spring=alias.spring))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(alias.url, response.headers.get('Location'))
        self.assertEqual(alias.clicked, 1)

    def test_jump_by_spring_alias_owner(self):
        actor = self.create_user('Actor', 'a@example.com', 'dd')
        alias = self.create_owned_link(actor, 'http://example.com')
        with self.client:
            self.log_the_user_in(actor.username, 'dd')
            self.assertEqual(current_user.username, actor.username)
        response = self.client.get(url_for('main.jump', spring=alias.spring))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(alias.url, response.headers.get('Location'))
        self.assertEqual(alias.clicked, 0)

from datetime import datetime
from hashlib import md5
from time import sleep

from sqlalchemy.exc import IntegrityError

from journal import db
from journal.models.auth import Account, Group, Permission, User
from journal.models.exc import BadRequiredArg, NoneRequiredArg
from tests.cases import ServiceModels, Models


class Permissions(ServiceModels):
    def test_permission_there_is_no_permissions_initially(self):
        self.assertEqual(Permission.query.count(), 0)

    def test_permission_permission_creation(self):
        permission = 'do something'
        db.session.add(Permission(kind=permission))
        db.session.commit()
        self.assertIsNotNone(
            Permission.query.filter_by(kind=permission).first())

    def test_permission_permission_kind_uniqueness(self):
        permission = 'do something'
        db.session.add(Permission(kind=permission))
        db.session.commit()
        with self.assertRaises(IntegrityError):
            db.session.add(Permission(kind=permission))
            db.session.commit()

    def test_permission_raises_without_required_argument(self):
        with self.assertRaises(NoneRequiredArg):
            Permission()

    def test_permission_raises_with_bad_argument(self):
        with self.assertRaises(BadRequiredArg):
            Permission(kind='')
        with self.assertRaises(BadRequiredArg):
            Permission(kind=None)
        with self.assertRaises(BadRequiredArg):
            Permission(kind=['Bad argument'])

    def test_permission_insert_permissions(self):
        Permission.insert_permissions(self.permissions)
        self.assertEqual(Permission.query.count(), len(self.permissions))
        for permission in self.permissions:
            self.assertIsNotNone(
                Permission.query.filter_by(kind=permission).first())

    def test_permission_repeated_insert_permissions(self):
        Permission.insert_permissions(self.permissions)
        Permission.insert_permissions(self.permissions)
        self.assertEqual(Permission.query.count(), len(self.permissions))

    def test_permission_permission_representation_and_order(self):
        Permission.insert_permissions(self.permissions)
        for step, each in enumerate(Permission.query):
            self.assertEqual(
                repr(each), '<Permission: {}>'.format(self.permissions[step]))


class Groups(ServiceModels):
    def test_group_there_is_no_groups_initially(self):
        self.assertEqual(Group.query.count(), 0)

    def test_group_group_creation(self):
        db.session.add(Group(title=self.groups.taciturn))
        db.session.commit()
        g = Group.query.filter_by(title=self.groups.taciturn).first()
        self.assertIsNotNone(g)
        self.assertIs(g.default, False)
        self.assertEqual(g.permissions.count(), 0)
        self.assertEqual(g.users.count(), 0)

    def test_group_group_title_uniqueness(self):
        db.session.add(Group(title=self.groups.taciturn))
        db.session.commit()
        with self.assertRaises(IntegrityError):
            db.session.add(Group(title=self.groups.taciturn))
            db.session.commit()

    def test_group_raises_without_required_argument(self):
        with self.assertRaises(NoneRequiredArg):
            Group()

    def test_group_raises_with_bad_argument(self):
        with self.assertRaises(BadRequiredArg):
            Group(title='')
        with self.assertRaises(BadRequiredArg):
            Group(title=None)
        with self.assertRaises(BadRequiredArg):
            Group(title=['Newbies'])

    def test_group_inserting_groups(self):
        Group.insert_groups(self.permissions, self.groups, Permission)
        self.assertEqual(Group.query.count(), len(self.groups))
        for group in self.groups:
            self.assertIsNotNone(
                Group.query.filter_by(title=group).first())

    def test_group_repeated_inserting_groups(self):
        Group.insert_groups(self.permissions, self.groups, Permission)
        Group.insert_groups(self.permissions, self.groups, Permission)
        self.assertEqual(Group.query.count(), len(self.groups))
        for g in Group.query:
            for p in Permission.query:
                if p in g.permissions:
                    self.assertEqual(g.permissions.all().count(p), 1)

    def test_group_inserting_pariah(self):
        Group.insert_groups(self.permissions, self.groups, Permission)
        g = Group.query.filter_by(title=self.groups.pariah).first()
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.CANNOT_LOG_IN)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.READ_JOURNAL).first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.FOLLOW_USERS).first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.SEND_PM).first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.WRITE_COMMENTARY)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.CREATE_LINK_ALIAS)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.CREATE_ENTITY)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.BLOCK_ENTITY).first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.REMOVE_COMMENTARY)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.CHANGE_USER_ROLE)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.UPLOAD_PICTURES)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.MAKE_ANNOUNCEMENT)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.ADMINISTER_SERVICE)
            .first())

    def test_group_inserting_taciturn(self):
        Group.insert_groups(self.permissions, self.groups, Permission)
        g = Group.query.filter_by(title=self.groups.taciturn).first()
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.CANNOT_LOG_IN)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.READ_JOURNAL).first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.FOLLOW_USERS).first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.SEND_PM).first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.WRITE_COMMENTARY)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.CREATE_LINK_ALIAS)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.CREATE_ENTITY)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.BLOCK_ENTITY).first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.REMOVE_COMMENTARY)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.CHANGE_USER_ROLE)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.UPLOAD_PICTURES)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.MAKE_ANNOUNCEMENT)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.ADMINISTER_SERVICE)
            .first())

    def test_group_inserting_commentator(self):
        Group.insert_groups(self.permissions, self.groups, Permission)
        g = Group.query.filter_by(title=self.groups.commentator).first()
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.CANNOT_LOG_IN)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.READ_JOURNAL).first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.FOLLOW_USERS).first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.SEND_PM).first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.WRITE_COMMENTARY)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.CREATE_LINK_ALIAS)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.CREATE_ENTITY)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.BLOCK_ENTITY).first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.REMOVE_COMMENTARY)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.CHANGE_USER_ROLE)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.UPLOAD_PICTURES)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.MAKE_ANNOUNCEMENT)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.ADMINISTER_SERVICE)
            .first())

    def test_group_inserting_blogger(self):
        Group.insert_groups(self.permissions, self.groups, Permission)
        g = Group.query.filter_by(title=self.groups.blogger).first()
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.CANNOT_LOG_IN)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.READ_JOURNAL).first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.FOLLOW_USERS).first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.SEND_PM).first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.WRITE_COMMENTARY)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.CREATE_LINK_ALIAS)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.CREATE_ENTITY)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.BLOCK_ENTITY).first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.REMOVE_COMMENTARY)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.CHANGE_USER_ROLE)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.UPLOAD_PICTURES)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.MAKE_ANNOUNCEMENT)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.ADMINISTER_SERVICE)
            .first())

    def test_group_inserting_curator(self):
        Group.insert_groups(self.permissions, self.groups, Permission)
        g = Group.query.filter_by(title=self.groups.curator).first()
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.CANNOT_LOG_IN)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.READ_JOURNAL).first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.FOLLOW_USERS).first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.SEND_PM).first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.WRITE_COMMENTARY)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.CREATE_LINK_ALIAS)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.CREATE_ENTITY)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.BLOCK_ENTITY).first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.REMOVE_COMMENTARY)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.CHANGE_USER_ROLE)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.UPLOAD_PICTURES)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.MAKE_ANNOUNCEMENT)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.ADMINISTER_SERVICE)
            .first())

    def test_group_inserting_keeper(self):
        Group.insert_groups(self.permissions, self.groups, Permission)
        g = Group.query.filter_by(title=self.groups.keeper).first()
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.CANNOT_LOG_IN)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.READ_JOURNAL).first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.FOLLOW_USERS).first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.SEND_PM).first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.WRITE_COMMENTARY)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.CREATE_LINK_ALIAS)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.CREATE_ENTITY)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.BLOCK_ENTITY).first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.REMOVE_COMMENTARY)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.CHANGE_USER_ROLE)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.UPLOAD_PICTURES)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.MAKE_ANNOUNCEMENT)
            .first())
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.ADMINISTER_SERVICE)
            .first())

    def test_group_inserting_root(self):
        Group.insert_groups(self.permissions, self.groups, Permission)
        g = Group.query.filter_by(title=self.groups.root).first()
        self.assertIsNone(
            g.permissions.filter_by(kind=self.permissions.CANNOT_LOG_IN)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.READ_JOURNAL).first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.FOLLOW_USERS).first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.SEND_PM).first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.WRITE_COMMENTARY)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.CREATE_LINK_ALIAS)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.CREATE_ENTITY)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.BLOCK_ENTITY).first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.REMOVE_COMMENTARY)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.CHANGE_USER_ROLE)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.UPLOAD_PICTURES)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.MAKE_ANNOUNCEMENT)
            .first())
        self.assertIsNotNone(
            g.permissions.filter_by(kind=self.permissions.ADMINISTER_SERVICE)
            .first())

    def test_group_groups_representation_and_order(self):
        Group.insert_groups(self.permissions, self.groups, Permission)
        for step, each in enumerate(Group.query):
            self.assertEqual(
                repr(each), '<Group: {}>'.format(self.groups[step]))


class Accounts(Models):
    def test_account_there_is_no_accounts_initially(self):
        self.assertEqual(Account.query.count(), 0)

    def test_account_account_creation(self):
        account = self.create_account('actor@example.com')
        self.assertIsNotNone(Account.query.get(account.id))

    def test_account_account_address_uniqueness(self):
        self.create_account('actor@example.com')
        with self.assertRaises(IntegrityError):
            self.create_account('actor@example.com')

    def test_account_account_initial_ava_hash(self):
        account = self.create_account('a@example.com')
        self.assertIsNotNone(account.ava_hash)
        self.assertEqual(
            account.ava_hash,
            md5('a@example.com'.encode('utf-8')).hexdigest())

    def test_account_account_ava_hash_after_address_change(self):
        account = self.create_account('a@example.com')
        account.address = 'b@example.com'
        self.assertEqual(
            account.ava_hash,
            md5('b@example.com'.encode('utf-8')).hexdigest())

    def test_account_raises_without_required_argument(self):
        with self.assertRaises(NoneRequiredArg):
            Account()

    def test_account_raises_with_bad_argument(self):
        with self.assertRaises(BadRequiredArg):
            Account(address='')
        with self.assertRaises(BadRequiredArg):
            Account(address=None)
        with self.assertRaises(BadRequiredArg):
            Account(address=['actor@example.com'])

    def test_account_initial_account_swap(self):
        account = self.create_account('actor@example.com')
        self.assertIsNone(account.swap)

    def test_account_account_swap_during_creation(self):
        account = Account(address='actor@example.com', swap='ector@example.com')
        db.session.add(account)
        db.session.commit()
        self.assertIsNone(account.swap)

    def test_account_account_swap_can_be_assigned(self):
        account = self.create_account('actor@example.com')
        account.swap = 'hector@example.com'
        db.session.add(account)
        db.session.commit()
        swapped = Account.query.filter_by(swap=account.swap).first()
        self.assertIs(swapped, account)

    def test_account_account_timestamp(self):
        account = self.create_account('actor@example.com')
        another = self.create_account('hector@example.com')
        self.assertTrue(
            (datetime.utcnow() - account.requested).total_seconds() < 3)
        self.assertNotEqual(account.requested, another.requested)

    def test_account_account_representation(self):
        account = self.create_account('actor@example.com')
        self.assertEqual(repr(account), '<Account: actor@example.com>')

    def test_account_account_can_relate_to_no_user(self):
        account = self.create_account('actor@example.com')
        self.assertIsNone(account.user)

    def test_account_account_can_relate_to_user(self):
        account = self.create_account('actor@example.com')
        user = User(username='Actor', password='aa')
        user.account = account
        db.session.add(user)
        db.session.commit()
        self.assertIs(user.account, account)

    def test_account_user_can_relate_to_account(self):
        user = User(username='Actor', password='aa')
        db.session.add(user)
        db.session.commit()
        account = self.create_account('actor@example.com')
        account.user = user
        db.session.add(account)
        db.session.commit()
        self.assertIs(account.user, user)

    def test_account_account_can_relate_to_only_user(self):
        account = self.create_account('actor@example.com')
        actor = User(username='Actor', password='aa')
        vector = User(username='Vector', password='mm')
        account.user = vector
        account.user = actor
        db.session.add(account)
        db.session.commit()
        self.assertIs(account, actor.account)
        self.assertIsNone(vector.account)

    def test_account_token_can_be_generated(self):
        account = self.create_account('actor@example.com')
        self.assertIsNotNone(account.generate_token())

    def test_account_account_token_uniqueness(self):
        account = self.create_account('actor@example.com')
        another = self.create_account('hector@example.com')
        self.assertNotEqual(account.generate_token(), another.generate_token())

    def test_account_account_tokens_are_different_in_time_line(self):
        account = self.create_account('actor@example.com')
        token = account.generate_token()
        sleep(1)
        another = account.generate_token()
        self.assertNotEqual(token, another)

    def test_account_get_ava_url(self):
        address = 'a@example.com'
        h = md5(address.encode('utf-8')).hexdigest()
        account = self.create_account(address)
        with self.app.test_request_context('/'):
            g = account.get_ava_url()
            g_32 = account.get_ava_url(size=32)
            g_pg = account.get_ava_url(rating='pg')
            g_retro = account.get_ava_url(default='retro')
        with self.app.test_request_context('/', base_url='https://example.com'):
            g_ssl = account.get_ava_url()
        self.assertIn('http://www.gravatar.com/avatar/' + h, g)
        self.assertIn('s=32', g_32)
        self.assertIn('r=pg', g_pg)
        self.assertIn('d=retro', g_retro)
        self.assertIn('https://secure.gravatar.com/avatar/' + h, g_ssl)

    def test_account_parse_address_with_short_address(self):
        address = 'shortessone@pythonanywhere.com'
        account = self.create_account(address)
        self.assertEqual(address, account.parse_address())

    def test_account_parse_address_with_long_address(self):
        address = 'someaddres@tremendouslyhuge.com'
        account = self.create_account(address)
        parsed = account.parse_address()
        self.assertEqual(len(parsed), 30)
        self.assertIn('someaddres@~', parsed)

    def test_account_parse_address_with_long_address_1(self):
        address = 'tremendouslyhugeaddress@example.com'
        account = self.create_account(address)
        parsed = account.parse_address()
        self.assertEqual(len(parsed), 30)
        self.assertIn('~@example.com', parsed)

    def test_account_parse_address_with_long_address_2(self):
        address = 'tremendouslyhugeaddress@tremendouslyhuge.com'
        account = self.create_account(address)
        parsed = account.parse_address()
        self.assertEqual(len(parsed), 30)
        self.assertIn('~@~', parsed)


class Users(Models):
    def test_user_there_is_no_users_initially(self):
        self.assertEqual(User.query.count(), 0)

    def test_user_user_creation(self):
        actor = self.create_user('Actor', 'actor@example.com', 'aa')
        self.assertIsNotNone(User.query.get(actor.id))

    def test_user_user_username_uniqueness(self):
        self.create_user('Actor', 'actor@example.com', 'bb')
        with self.assertRaises(IntegrityError):
            self.create_user('Actor', 'vector@example.com', 'mm')

    def test_user_raises_without_required_argument(self):
        with self.assertRaises(NoneRequiredArg):
            User()
        with self.assertRaises(NoneRequiredArg):
            User(username='Actor')
        with self.assertRaises(NoneRequiredArg):
            User(password='aa')

    def test_user_raises_with_bad_username(self):
        with self.assertRaises(BadRequiredArg):
            User(username='', password='aa')
        with self.assertRaises(BadRequiredArg):
            User(username=None, password='aa')
        with self.assertRaises(BadRequiredArg):
            User(username=['Hector'], password='aa')

    def test_user_raises_with_bad_password(self):
        with self.assertRaises(BadRequiredArg):
            User(username='Actor', password='')
        with self.assertRaises(BadRequiredArg):
            User(username='Actor', password=None)
        with self.assertRaises(BadRequiredArg):
            User(username='Actor', password=['aa'])

    def test_user_user_password_cannot_be_read(self):
        actor = self.create_user('Actor', 'actor@example.com', 'aa')
        with self.assertRaises(AttributeError):
            print(actor.password)

    def test_user_user_password_hash(self):
        actor = self.create_user('Actor', 'actor@example.com', 'aa')
        self.assertIsNotNone(actor.password_hash)
        self.assertNotEqual(actor.password_hash, 'aa')

    def test_user_user_password_can_be_verified(self):
        actor = self.create_user('Actor', 'actor@example.com', 'aa')
        self.assertTrue(actor.verify_password('aa'))
        self.assertFalse(actor.verify_password('bb'))

    def test_user_user_password_salts_are_random(self):
        actor = self.create_user('Actor', 'actor@example.com', 'aa')
        vector = self.create_user('Vector', 'vector@example.com', 'aa')
        self.assertNotEqual(actor.password_hash, vector.password_hash)

    def test_user_user_initial_timestamps(self):
        actor = self.create_user('Actor', 'actor@example.com', 'aa')
        self.assertTrue(
            (datetime.utcnow() - actor.registered).total_seconds() < 3)
        self.assertTrue(
            (datetime.utcnow() - actor.registered).total_seconds() < 3)

    def test_user_ping(self):
        actor = self.create_user('Actor', 'actor@example.com', 'aa')
        l_v = actor.last_visit
        actor.ping()
        self.assertTrue(actor.last_visit > l_v)

    def test_user_user_representation(self):
        actor = self.create_user('Actor', 'actor@example.com', 'aa')
        self.assertEqual(repr(actor), '<User: Actor>')

    def test_user_default_user_group(self):
        actor = self.create_user('Actor', 'actor@example.com', 'aa')
        self.assertTrue(actor.group.default)
        self.assertEqual(actor.group.title, self.groups.blogger)

    def test_user_is_root(self):
        for each in self.groups:
            if each != self.groups.root:
                actor = self.create_user('A', 'a@example.com', 'aa', each)
                self.assertFalse(actor.is_root())
                db.session.delete(actor.account)
                db.session.delete(actor)
        actor = self.create_user('A', 'a@example.com', 'aa', self.groups.root)
        self.assertTrue(actor.is_root())

    def test_user_users_in_not_default_groups(self):
        for each in self.groups:
            if each != self.groups.blogger:
                actor = self.create_user(
                    'A', 'a@example.com', 'aa', each)
                self.assertFalse(actor.group.default)
                self.assertEqual(actor.group.title, each)
                db.session.delete(actor.account)
                db.session.delete(actor)

    def test_user_pariah_permissions(self):
        actor = self.create_user('A', 'a@example.com', 'aa', self.groups.pariah)
        self.assertTrue(actor.can(self.permissions.CANNOT_LOG_IN))
        self.assertFalse(actor.can(self.permissions.READ_JOURNAL))
        self.assertFalse(actor.can(self.permissions.FOLLOW_USERS))
        self.assertFalse(actor.can(self.permissions.SEND_PM))
        self.assertFalse(actor.can(self.permissions.WRITE_COMMENTARY))
        self.assertFalse(actor.can(self.permissions.CREATE_LINK_ALIAS))
        self.assertFalse(actor.can(self.permissions.CREATE_ENTITY))
        self.assertFalse(actor.can(self.permissions.BLOCK_ENTITY))
        self.assertFalse(actor.can(self.permissions.REMOVE_COMMENTARY))
        self.assertFalse(actor.can(self.permissions.CHANGE_USER_ROLE))
        self.assertFalse(actor.can(self.permissions.UPLOAD_PICTURES))
        self.assertFalse(actor.can(self.permissions.MAKE_ANNOUNCEMENT))
        self.assertFalse(actor.can(self.permissions.ADMINISTER_SERVICE))

    def test_user_taciturn_permissions(self):
        actor = self.create_user(
            'A', 'a@example.com', 'aa', self.groups.taciturn)
        self.assertFalse(actor.can(self.permissions.CANNOT_LOG_IN))
        self.assertTrue(actor.can(self.permissions.READ_JOURNAL))
        self.assertTrue(actor.can(self.permissions.FOLLOW_USERS))
        self.assertFalse(actor.can(self.permissions.SEND_PM))
        self.assertFalse(actor.can(self.permissions.WRITE_COMMENTARY))
        self.assertFalse(actor.can(self.permissions.CREATE_LINK_ALIAS))
        self.assertFalse(actor.can(self.permissions.CREATE_ENTITY))
        self.assertFalse(actor.can(self.permissions.BLOCK_ENTITY))
        self.assertFalse(actor.can(self.permissions.REMOVE_COMMENTARY))
        self.assertFalse(actor.can(self.permissions.CHANGE_USER_ROLE))
        self.assertFalse(actor.can(self.permissions.UPLOAD_PICTURES))
        self.assertFalse(actor.can(self.permissions.MAKE_ANNOUNCEMENT))
        self.assertFalse(actor.can(self.permissions.ADMINISTER_SERVICE))

    def test_user_commentator_permissions(self):
        actor = self.create_user(
            'A', 'a@example.com', 'aa', self.groups.commentator)
        self.assertFalse(actor.can(self.permissions.CANNOT_LOG_IN))
        self.assertTrue(actor.can(self.permissions.READ_JOURNAL))
        self.assertTrue(actor.can(self.permissions.FOLLOW_USERS))
        self.assertTrue(actor.can(self.permissions.SEND_PM))
        self.assertTrue(actor.can(self.permissions.WRITE_COMMENTARY))
        self.assertTrue(actor.can(self.permissions.CREATE_LINK_ALIAS))
        self.assertFalse(actor.can(self.permissions.CREATE_ENTITY))
        self.assertFalse(actor.can(self.permissions.BLOCK_ENTITY))
        self.assertFalse(actor.can(self.permissions.REMOVE_COMMENTARY))
        self.assertFalse(actor.can(self.permissions.CHANGE_USER_ROLE))
        self.assertFalse(actor.can(self.permissions.UPLOAD_PICTURES))
        self.assertFalse(actor.can(self.permissions.MAKE_ANNOUNCEMENT))
        self.assertFalse(actor.can(self.permissions.ADMINISTER_SERVICE))

    def test_user_blogger_permissions(self):
        actor = self.create_user('A', 'a@example.com', 'aa')
        self.assertFalse(actor.can(self.permissions.CANNOT_LOG_IN))
        self.assertTrue(actor.can(self.permissions.READ_JOURNAL))
        self.assertTrue(actor.can(self.permissions.FOLLOW_USERS))
        self.assertTrue(actor.can(self.permissions.SEND_PM))
        self.assertTrue(actor.can(self.permissions.WRITE_COMMENTARY))
        self.assertTrue(actor.can(self.permissions.CREATE_LINK_ALIAS))
        self.assertTrue(actor.can(self.permissions.CREATE_ENTITY))
        self.assertFalse(actor.can(self.permissions.BLOCK_ENTITY))
        self.assertFalse(actor.can(self.permissions.REMOVE_COMMENTARY))
        self.assertFalse(actor.can(self.permissions.CHANGE_USER_ROLE))
        self.assertFalse(actor.can(self.permissions.UPLOAD_PICTURES))
        self.assertFalse(actor.can(self.permissions.MAKE_ANNOUNCEMENT))
        self.assertFalse(actor.can(self.permissions.ADMINISTER_SERVICE))

    def test_user_curator_permissions(self):
        actor = self.create_user(
            'A', 'a@example.com', 'aa', self.groups.curator)
        self.assertFalse(actor.can(self.permissions.CANNOT_LOG_IN))
        self.assertTrue(actor.can(self.permissions.READ_JOURNAL))
        self.assertTrue(actor.can(self.permissions.FOLLOW_USERS))
        self.assertTrue(actor.can(self.permissions.SEND_PM))
        self.assertFalse(actor.can(self.permissions.WRITE_COMMENTARY))
        self.assertTrue(actor.can(self.permissions.CREATE_LINK_ALIAS))
        self.assertFalse(actor.can(self.permissions.CREATE_ENTITY))
        self.assertTrue(actor.can(self.permissions.BLOCK_ENTITY))
        self.assertTrue(actor.can(self.permissions.REMOVE_COMMENTARY))
        self.assertTrue(actor.can(self.permissions.CHANGE_USER_ROLE))
        self.assertFalse(actor.can(self.permissions.UPLOAD_PICTURES))
        self.assertFalse(actor.can(self.permissions.MAKE_ANNOUNCEMENT))
        self.assertFalse(actor.can(self.permissions.ADMINISTER_SERVICE))

    def test_user_keeper_permissions(self):
        actor = self.create_user(
            'A', 'a@example.com', 'aa', self.groups.keeper)
        self.assertFalse(actor.can(self.permissions.CANNOT_LOG_IN))
        self.assertTrue(actor.can(self.permissions.READ_JOURNAL))
        self.assertTrue(actor.can(self.permissions.FOLLOW_USERS))
        self.assertTrue(actor.can(self.permissions.SEND_PM))
        self.assertTrue(actor.can(self.permissions.WRITE_COMMENTARY))
        self.assertTrue(actor.can(self.permissions.CREATE_LINK_ALIAS))
        self.assertTrue(actor.can(self.permissions.CREATE_ENTITY))
        self.assertFalse(actor.can(self.permissions.BLOCK_ENTITY))
        self.assertFalse(actor.can(self.permissions.REMOVE_COMMENTARY))
        self.assertTrue(actor.can(self.permissions.CHANGE_USER_ROLE))
        self.assertTrue(actor.can(self.permissions.UPLOAD_PICTURES))
        self.assertTrue(actor.can(self.permissions.MAKE_ANNOUNCEMENT))
        self.assertFalse(actor.can(self.permissions.ADMINISTER_SERVICE))

    def test_user_root_permissions(self):
        actor = self.create_user(
            'A', 'a@example.com', 'aa', self.groups.root)
        self.assertFalse(actor.can(self.permissions.CANNOT_LOG_IN))
        self.assertTrue(actor.can(self.permissions.READ_JOURNAL))
        self.assertTrue(actor.can(self.permissions.FOLLOW_USERS))
        self.assertTrue(actor.can(self.permissions.SEND_PM))
        self.assertTrue(actor.can(self.permissions.WRITE_COMMENTARY))
        self.assertTrue(actor.can(self.permissions.CREATE_LINK_ALIAS))
        self.assertTrue(actor.can(self.permissions.CREATE_ENTITY))
        self.assertTrue(actor.can(self.permissions.BLOCK_ENTITY))
        self.assertTrue(actor.can(self.permissions.REMOVE_COMMENTARY))
        self.assertTrue(actor.can(self.permissions.CHANGE_USER_ROLE))
        self.assertTrue(actor.can(self.permissions.UPLOAD_PICTURES))
        self.assertTrue(actor.can(self.permissions.MAKE_ANNOUNCEMENT))
        self.assertTrue(actor.can(self.permissions.ADMINISTER_SERVICE))

    def test_user_can_have_links(self):
        actor = self.create_user('Actor', 'a@example.com', 'ms')
        link = self.create_owned_link(actor, 'http://yandex.ru')
        self.assertTrue(actor.links.all())
        self.assertIn(link, actor.links.all())

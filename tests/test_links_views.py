from random import choice
from string import ascii_letters

from flask import json, url_for

from journal.models.links import Link
from tests.cases import Views


class ShowLinks(Views):
    no_aliases = 'У Вас нет ни одного алиаса.'
    new_alias = 'Новый алиас успешно создан.'

    def test_show_links_by_anonymous_user(self):
        response = self.client.get(
            url_for('links.show_links'), follow_redirects=False)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            url_for(
                'auth.login', next=url_for('links.show_links', _external=False),
                _external=True),
            response.headers.get('Location'))

    def test_show_links_by_taciturn_user(self):
        actor = self.create_user(
            'Actor', 'a@example.com', 'aa', group_title=self.groups.taciturn)
        self.log_the_user_in(actor.username, 'aa')
        response = self.client.get(url_for('links.show_links'))
        self.assertEqual(response.status_code, 403)

    def test_show_links_by_default_user(self):
        actor = self.create_user('Actor', 'a@example.com', 'aa')
        self.log_the_user_in(actor.username, 'aa')
        response = self.client.get(url_for('links.show_links'))
        self.assertEqual(response.status_code, 200)
        self.assertIn(self.no_aliases, response.get_data(as_text=True))

    def test_show_links_does_not_show_other_users_link(self):
        vector = self.create_user('Vector', 'v@example.com', 'vv')
        vector_link = self.create_owned_link(
            vector, 'http://yandex.ru/passport')
        actor = self.create_user('Actor', 'a@example.com', 'ss')
        self.log_the_user_in(actor.username, 'ss')
        response = self.client.get(url_for('links.show_links'))
        response_text = response.get_data(as_text=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(self.no_aliases, response_text)
        self.assertNotIn(vector_link.url, response_text)
        self.assertNotIn(
            url_for('main.jump', spring=vector_link.spring, _external=True),
            response_text)

    def test_show_links_shows_current_users_links(self):
        actor = self.create_user('Actor', 'a@example.com', 'mm')
        actor_link = self.create_owned_link(actor, 'http://example.com/pages')
        self.log_the_user_in(actor.username, 'mm')
        response = self.client.get(url_for('links.show_links'))
        response_text = response.get_data(as_text=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(actor_link.url, response_text)
        self.assertIn(
            url_for('main.jump', spring=actor_link.spring, _external=True),
            response_text)
        self.assertNotIn(self.no_aliases, response_text)

    def test_show_links_alias_creation_with_unknown_url(self):
        actor = self.create_user('Actor', 'a@example.com', 'vs')
        self.log_the_user_in(actor.username, 'vs')
        response = self.client.post(
            url_for('links.show_links'),
            data={'url': 'https://google.com'},
            follow_redirects=True)
        response_text = response.get_data(as_text=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn('Создаю новый алиас.', response_text)
        # self.assertEqual(actor.links.count(), 1)
        # self.assertIn('https://google.com', response_text)
        # self.assertNotIn(self.no_aliases, response_text)

    # def test_show_links_alias_creation_with_unknown_url_redirection(self):
    #     actor = self.create_user('Actor', 'a@example.com', 'vs')
    #     self.log_the_user_in(actor.username, 'vs')
    #     response = self.client.post(
    #         url_for('links.show_links'),
    #         data={'url': 'https://google.com'},
    #         follow_redirects=False)
    #     self.assertEqual(response.status_code, 200)
    #     self.assertIn(
    #         url_for('main.redirects', _external=True),
    #         response.headers.get('Location'))

    def test_show_links_alias_creation_with_repeated_url(self):
        actor = self.create_user('Actor', 'a@example.com', 'ms')
        link = self.create_owned_link(actor, 'https://example.com')
        self.log_the_user_in(actor.username , 'ms')
        response = self.client.post(
            url_for('links.show_links'),
            data={'url': link.url},
            follow_redirects=False)
        response_text = response.get_data(as_text=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn('Алиас был создан ранее.', response_text)
        self.assertIn(
            '<h3 class="panel-title">Ранее созданный алиас</h3>', response_text)

    def test_show_links_pagination_widget(self):
        actor = self.create_user('Actor', 'a@example.com', 'vs')
        for each in ('http://yandex.ru', 'http://google.com',
                     'http://example.com', 'http://gmail.com'):
            self.create_owned_link(actor, each)
        self.log_the_user_in(actor.username, 'vs')
        response = self.client.get(url_for('links.show_links', page=2))
        response_text = response.get_data(as_text=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn('<ul class="pagination pagination-sm">', response_text)
        self.assertNotIn(
            '<h3 class="panel-title">Создайте свой новый алиас</h3>',
            response_text)
        self.assertIn('http://yandex.ru', response_text)

    def test_show_links_alias_creation_with_bad_protocol(self):
        actor = self.create_user('Actor', 'a@example.com', 'dv')
        self.log_the_user_in(actor.username, 'dv')
        response = self.client.post(
            url_for('links.show_links'),
            data={'url': 'ftp://yandex.ru'}, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<p class="error">поддерживаются только http и https ссылки</p>',
            response.get_data(as_text=True))
        self.assertIsNone(Link.query.filter_by(url='ftp://yandex.ru').first())

    def test_show_links_alias_creation_with_no_url(self):
        actor = self.create_user('Actor', 'a@example.com', 'dv')
        self.log_the_user_in(actor.username, 'dv')
        response = self.client.post(
            url_for('links.show_links'),
            data={'url': ''}, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<p class="error">обязательное для заполнения поле</p>',
            response.get_data(as_text=True))
        self.assertEqual(Link.query.count(), 0)

    def test_show_links_alias_creation_with_bad_url(self):
        actor = self.create_user('Actor', 'a@example.com', 'dv')
        self.log_the_user_in(actor.username, 'dv')
        response = self.client.post(
            url_for('links.show_links'),
            data={'url': 'https://this is not a valid url'},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<p class="error">здесь может быть только url-адрес</p>',
            response.get_data(as_text=True))
        self.assertEqual(Link.query.count(), 0)

    def test_show_links_alias_creation_with_too_long_url(self):
        url = 'https://example.com/' + ''.join(
            choice(ascii_letters) for _ in range(512))
        actor = self.create_user('Actor', 'a@example.com', 'dv')
        self.log_the_user_in(actor.username, 'dv')
        response = self.client.post(
            url_for('links.show_links'),
            data={'url': url},
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            '<p class="error">длина url не более 512 знаков</p>',
            response.get_data(as_text=True))
        self.assertEqual(Link.query.count(), 0)

    # def test_show_links_one_url_two_users(self):
    #     vector = self.create_user('Vector', 'v@example.com', 'mm')
    #     alias = self.create_owned_link(vector, 'http://yandex.ru')
    #     actor = self.create_user('Actor', 'a@example.com', 'dd')
    #     self.log_the_user_in(actor.username, 'dd')
    #     response = self.client.post(
    #         url_for('links.show_links'),
    #         data={'url': alias.url}, follow_redirects=True)
    #     self.assertEqual(response.status_code, 200)
    #     self.assertEqual(actor.links.count(), 1)
    #     self.assertEqual(vector.links.count(), 1)
    #     self.assertNotEqual(actor.links[0].spring, alias.spring)


class RemoveAlias(Views):
    def test_remove_alias_with_get_request(self):
        response = self.client.get(url_for('links.remove_alias'))
        self.assertEqual(response.status_code, 405)

    def test_remove_alias_with_no_alias(self):
        response = self.client.post(
            url_for('links.remove_alias'),
            data={'page': 1, 'last': 0})
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 200)
        self.assertTrue(result['empty'])

    def test_remove_alias_from_the_first_page(self):
        actor = self.create_user('Actor', 'a@example.com', 'vs')
        link = self.create_owned_link(actor, 'http://example.com')
        response = self.client.post(
            url_for('links.remove_alias'),
            data={'suffix': link.spring})
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 200)
        self.assertFalse(result['empty'])
        self.assertIsNotNone(result.get('task_id'))
        self.assertEqual(
            result.get('redirect'),
            url_for('links.show_links', _external=False))

    def test_remove_alias_from_the_second_page(self):
        actor = self.create_user('Actor', 'a@example.com', 'ds')
        link = self.create_owned_link(actor, 'http://yandex.ru')
        for each in ('http://rambler.ru', 'http://google.com',
                     'http://example.com', 'http://mail.ru'):
            self.create_owned_link(actor, each)
        response = self.client.post(
            url_for('links.remove_alias'),
            data={'suffix': link.spring, 'page': 2, 'last': 0})
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 200)
        self.assertFalse(result['empty'])
        self.assertIsNotNone(result.get('task_id'))
        self.assertEqual(
            result.get('redirect'),
            url_for('links.show_links', page=2, _external=False))

    def test_remove_alias_last_alias_from_the_second_page(self):
        actor = self.create_user('Actor', 'a@example.com', 'ds')
        link = self.create_owned_link(actor, 'http://yandex.ru')
        for each in ('http://rambler.ru', 'http://google.com',
                     'http://example.com'):
            self.create_owned_link(actor, each)
        response = self.client.post(
            url_for('links.remove_alias'),
            data={'suffix': link.spring, 'page': 2, 'last': 1})
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 200)
        self.assertFalse(result['empty'])
        self.assertIsNotNone(result.get('task_id'))
        self.assertEqual(
            result.get('redirect'),
            url_for('links.show_links', _external=False))

    def test_remove_alias_with_bad_spring(self):
        actor = self.create_user('Actor', 'a@example.com', 'ss')
        link = self.create_owned_link(actor, 'http://example.com')
        response = self.client.post(
            url_for('links.remove_alias'),
            data={'page': 1, 'last': 0, 'suffix': 'vNm1d'})
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 200)
        self.assertTrue(result['empty'])

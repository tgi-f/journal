import unittest

from datetime import datetime, timedelta

from flask import url_for

from journal import create_app, db
from journal.models.auth import Account, Group, Permission, User
from journal.models.auth_units import groups, permissions
from journal.models.links import Link


class RootTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing', None)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()


class ServiceModels(RootTestCase):
    permissions = permissions
    groups = groups


class Models(ServiceModels):
    def setUp(self):
        ServiceModels.setUp(self)
        Group.insert_groups(self.permissions, self.groups, Permission)

    def create_account(self, address):
        account = Account(address=address)
        db.session.add(account)
        db.session.commit()
        return account

    def create_user(self, username, address, password, group_title=None):
        group = None
        if group_title:
            group = Group.query.filter_by(title=group_title).first()
        account = Account(address=address)
        user = User(username=username, password=password)
        user.account = account
        if group:
            user.group = group
        db.session.add(user)
        db.session.commit()
        return user

    def create_link(self, url):
        link = Link(url=url)
        db.session.add(link)
        db.session.commit()
        return link

    def create_owned_link(self, owner, url):
        link = Link(url=url)
        link.owner = owner
        db.session.add(link)
        db.session.commit()
        return link


class Views(Models):
    def setUp(self):
        Models.setUp(self)
        self.client = self.app.test_client(use_cookies=True)

    def log_the_user_in(self, username, password, redirects=True):
        return self.client.post(
            url_for('auth.login'),
            data={'login': username,
                  'password': password},
            follow_redirects=redirects)

    def log_the_user_in_email(self, address, password, redirects=True):
        return self.client.post(
            url_for('auth.login'),
            data={'login': address, 'password': password},
            follow_redirects=redirects)

    def create_old_user(self, username, address, password):
        user = self.create_user(username, address, password)
        user.account.requested = datetime.utcnow() - timedelta(days=2)
        db.session.add(user)
        db.session.commit()
        return user

    def swap_account(self, account, swap):
        account.swap = swap
        db.session.add(account)
        db.session.commit()

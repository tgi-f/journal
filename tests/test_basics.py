from flask import current_app

from tests.cases import RootTestCase


class Basics(RootTestCase):
    def test_application_exists(self):
        self.assertIsNotNone(current_app)

    def test_application_is_testing(self):
        self.assertTrue(current_app.config.get('TESTING'))
        self.assertFalse(current_app.config.get('DEBUG'))

    def test_index_endpoint(self):
        i = [rule for rule in current_app.url_map.iter_rules()
             if rule.rule == '/'][0]
        self.assertEqual(i.endpoint, 'main.index')

    def test_index_view_exists(self):
        self.assertIn('main.index', current_app.view_functions)

from celery import Celery
from flask import Flask, session
from flask_assets import Environment
from flask_login import LoginManager
from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect

from config import Config, config


assets = Environment()
celery = Celery(
    __name__,
    broker=Config.CELERY_BROKER_URL,
    backend=Config.CELERY_RESULT_BACKEND)
db = SQLAlchemy()
csrf = CSRFProtect()
mail = Mail()
lm = LoginManager()
lm.login_view = 'auth.login'
lm.login_message = 'Запрошенная страница доступна только зарегистрированным\
                    пользователям.'


def init_celery(app, cel):
    """
    This function initializes the given Celery instance with the given Flask
    application instance and returns the initialized Celery instance.
    Args:
        app: the Flask application instance
        cel: the Celery instance being initialized

    Returns: the initialized Celery instance

    """
    cel.conf.update(app.config)
    base = cel.Task

    class ContextTask(base):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return base.__call__(self, *args, **kwargs)

    cel.Task = ContextTask
    return cel


def create_app(config_name, cel):
    """
    This factory creates the Flask application, initializes its modules and
    blueprints, then initializes the given Celery instance (cel) and returns
    both instances (Flask and Celery).
    Args:
        config_name: the configuration name, can be 'production', 'testing',
                     'development' or 'default'
        cel: the Celery instance or None

    Returns: the application instance and Celery instance, if cel is not None

    """
    app = Flask(__name__)
    app.before_request(lambda: setattr(session, 'permanent', True))
    conf_obj = config[config_name]
    app.config.from_object(conf_obj)
    conf_obj.init_app(app)
    assets.init_app(app)
    db.init_app(app)
    csrf.init_app(app)
    lm.init_app(app)
    mail.init_app(app)
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)
    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint, url_prefix='/auth')
    from .links import links as links_blueprint
    app.register_blueprint(links_blueprint, url_prefix='/links')
    from .pictures import pictures as pictures_blueprint
    app.register_blueprint(pictures_blueprint, url_prefix='/pictures')
    from .blogs import blogs as blogs_blueprint
    app.register_blueprint(blogs_blueprint, url_prefix='/blogs')
    from .lists import lists as lists_blueprint
    app.register_blueprint(lists_blueprint, url_prefix='/lists')
    from .labels import labels as labels_blueprint
    app.register_blueprint(labels_blueprint, url_prefix='/labels')
    from .commentaries import commentaries as commentaries_blueprint
    app.register_blueprint(commentaries_blueprint, url_prefix='/commentaries')
    from .pm import pm as pm_blueprint
    app.register_blueprint(pm_blueprint, url_prefix='/pm')
    from .announce import announce as announce_blueprint
    app.register_blueprint(announce_blueprint, url_prefix='/announce')
    from .admin import admin as admin_blueprint
    app.register_blueprint(admin_blueprint, url_prefix='/admin')
    if cel:
        cel = init_celery(app, cel)
        return app, cel
    return app

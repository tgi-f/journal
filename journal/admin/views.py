from datetime import datetime

from flask import (
    current_app, flash, jsonify, redirect, render_template,
    request, send_from_directory, url_for)
from flask_login import current_user

from . import admin
from .forms import ChangeDefaultGroup, ChangeGroup, CreateUser
from .tools import check_permissions, make_choices
from .. import db
from ..decoration import root_required
from ..models.auth import Account, Group, User
from ..models.auth_units import groups
from ..models.pictures import Picture
from ..tasks.admin import clean_user


@admin.route('/default-group', methods=['GET', 'POST'])
@root_required
def change_default_group():
    default = Group.query.filter_by(default=True).first_or_404()
    choices = [(group.id, group.title) for group in
               Group.query.filter(
                   Group.title.in_(
                       [groups.commentator, groups.blogger, groups.keeper]))
               .order_by(Group.id.desc())]
    form = ChangeDefaultGroup(choices)
    if form.validate_on_submit():
        current = Group.query.get(form.group.data)
        default.default = False
        current.default = True
        db.session.add_all((current, default))
        db.session.commit()
        flash('Группа "{}" установлена для всех новых пользователей.'
              .format(current.title))
        return redirect(url_for('admin.change_default_group'))
    form.group.data = default.id
    return render_template('admin/group.html', now=datetime.utcnow(), form=form)


@admin.route('/society', methods=['GET', 'POST'])
@root_required
def show_users():
    form = CreateUser()
    if form.validate_on_submit():
        acc = Account.query.filter_by(address=form.address.data).first()
        if not acc:
            acc = Account(address=form.address.data)
            db.session.add(acc)
        user = User(
            username=form.username.data, password=form.password.data)
        user.account = acc
        db.session.add(user)
        db.session.commit()
        flash('Для пользователя {} создан аккаунт.'
              .format(user.username))
        return redirect(url_for('admin.show_users'))
    pagination = User.query.order_by(User.last_visit.desc()).paginate(
        page=request.args.get('page', 1, type=int),
        per_page=current_app.config.get('LINKS_PER_PAGE', 3),
        error_out=True)
    return render_template(
        'admin/society.html', now=datetime.utcnow(),
        pagination=pagination, form=form)


@admin.route('/society/find-users', methods=['POST'])
def find_users():
    result = {'empty': True}
    found = User.query.filter(User.username.like(
        '{}%'.format(request.form.get('value', None, type=str)))).order_by(
        User.last_visit.desc())
    if current_user.is_root():
        result = {'empty': False,
                  'html': render_template('admin/found.html', found=found)}
    return jsonify(result)


@admin.route('/society/find-picture', methods=['POST'])
def find_picture():
    result = {'empty': True}
    picture = Picture.query.filter_by(
        suffix=request.form.get('suffix', 'random-S', type=str)).first()
    if current_user.is_root():
        result = {'empty': False,
                  'html': render_template(
                      'admin/picture.html', picture=picture)}
    return jsonify(result)


@admin.route('/society/pictures', methods=['GET', 'POST'])
@root_required
def show_pictures():
    pagination = Picture.query.filter(Picture.suffix.isnot(None))\
        .order_by(Picture.uploaded.desc()).paginate(
        page=request.args.get('page', 1, type=int),
        per_page=current_app.config.get('LINKS_PER_PAGE', 3),
        error_out=True)
    return render_template('admin/pictures.html', pagination=pagination)


@admin.route('/society/slides/off', methods=['POST'])
def change_status():
    result = {'empty': True}
    picture = Picture.query.filter_by(
        suffix=request.form.get('suffix', 'random_S', type=str)).first()
    if picture.album.public and current_user.is_root():
        picture.album.public = False
        db.session.add(picture.album)
        db.session.commit()
        result = {'empty': False,
                  'redirect': url_for(
                      'admin.show_pictures',
                      page=request.form.get('page', None, type=int))}
    return jsonify(result)


@admin.route('/change-user-group', methods=['POST'])
def change_user_group():
    target = User.query.filter_by(
        username=request.form.get('username', None, type=str)).first()
    choices, permitted = make_choices(target, current_user)
    form = ChangeGroup(choices)
    form.group.data = target.group.id
    return render_template(
        'admin/change_group.html', form=form,
        target=target, permitted=permitted)


@admin.route('/make-change', methods=['POST'])
def make_group_change():
    target = User.query.filter_by(
        username=request.form.get('name', None, type=str)).first()
    group = Group.query.get(request.form.get('group', None, type=int))
    if target and group and check_permissions(current_user, target, group):
        if target.group != group:
            target.group = group
            db.session.add(target)
            db.session.commit()
            if group.title in (groups.curator, groups.root):
                clean_user.delay(target.username)
        flash('Пользователь {} в группе {}.'
              .format(target.username, group.title))
    else:
        flash('Действие не позволено!')
    result = {'redirect': url_for(
        'main.show_profile', username=target.username)}
    return jsonify(result)


@admin.route('/access.log')
@root_required
def show_log():
    return send_from_directory(
        current_app.config.get('SERVER_LOG_DIR', '/var/log/nginx/'),
        current_app.config.get('SERVER_LOG_FILE', 'access.log'),
        mimetype='text/plain')


@admin.route('/previous.log')
@root_required
def show_previous_log():
    return send_from_directory(
        current_app.config.get('SERVER_LOG_DIR', '/var/log/nginx/'),
        current_app.config.get('PREVIOUS_LOG_FILE', 'access.log.1'),
        mimetype='text/plain')


@admin.route('/worker.log')
@root_required
def show_worker_log():
    return send_from_directory(
        current_app.config.get('WORKER_LOG_DIR', '/var/log/celery/'),
        current_app.config.get('WORKER_LOG_FILE', 'worker.log'),
        mimetype='text/plain')

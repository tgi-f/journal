from ..models.auth import Group
from ..models.auth_units import groups, permissions


def make_choices(target, current_user):
    choices = [(group.id, group.title) for group in
               Group.query.order_by(Group.id.desc())]
    permitted = True
    if current_user.is_curator():
        choices = [(group.id, group.title) for group in
                   Group.query.filter(
                       Group.title.in_([groups.pariah, groups.taciturn,
                                        groups.commentator, groups.blogger]))
                   .order_by(Group.id.desc())]
        if target and target.is_keeper():
            permitted = False
    elif current_user.is_keeper():
        choices = [(group.id, group.title) for group in
                   Group.query.filter(
                       Group.title.in_([groups.pariah, groups.taciturn,
                                        groups.commentator, groups.blogger,
                                        groups.curator, groups.keeper]))
                   .order_by(Group.id.desc())]
    if not current_user.can(permissions.CHANGE_USER_ROLE):
        permitted = False
    return choices, permitted


def check_permissions(current_user, target, group):
    if current_user == target:
        return False
    if not current_user.can(permissions.CHANGE_USER_ROLE):
        return False
    if current_user.is_curator() and (target.is_keeper() or target.is_root()):
        return False
    if current_user.is_keeper() and target.is_root():
        return False
    if current_user.is_curator() and \
            group.title in (groups.keeper, groups.root):
        return False
    if current_user.is_keeper() and group.title in (groups.root,):
        return False
    return True

from flask_wtf import FlaskForm
from wtforms.fields import SelectField, StringField, SubmitField
from wtforms.validators import (
    DataRequired, Email, Length, Regexp, ValidationError)

from ..auth.forms import Passwords
from ..models.auth import Account, User


class ChangeDefaultGroup(FlaskForm):
    group = SelectField('Группа по умолчанию:', coerce=int)
    submit = SubmitField('Установить')

    def __init__(self, choices, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.group.choices = choices


class CreateUser(Passwords):
    username = StringField(
        'Псевдоним:',
        validators=[DataRequired(message='без псевдонима никак, увы'),
                    Length(min=3, max=16,
                           message='от 3-х до 16-ти знаков'),
                    Regexp(r'^[A-Za-z][a-zA-Z0-9\-_.]*$',
                           message='латинские буквы, цифры, дефис, знак\
                                   подчеркивания, точка, первый символ - \
                                   латинская буква')])
    address = StringField(
        'Емэйл Адрес:',
        validators=[DataRequired(message='без адреса никак, увы'),
                    Email(message='нужен адрес электронной почты'),
                    Length(max=128,
                           message='максимальная длина адреса - 128 знаков')])
    submit = SubmitField('Создать')

    def validate_username(self, field):
        if User.query.filter_by(username=field.data).first():
            raise ValidationError('этот псевдоним уже используется')

    def validate_address(self, field):
        acc = Account.query.filter_by(address=field.data).first()
        if acc and acc.user:
            raise ValidationError('этот адрес уже зарегистрирован')


class ChangeGroup(FlaskForm):
    target = StringField(
        'Username:',
        validators=[DataRequired(message='обязательное поле')])
    group = SelectField('Группа:', coerce=int)
    submit = SubmitField('Утвердить')

    def __init__(self, choices, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.group.choices = choices

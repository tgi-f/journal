from flask_login import current_user
from flask_wtf import FlaskForm
from wtforms.fields import BooleanField, PasswordField, StringField, SubmitField
from wtforms.validators import (
    DataRequired, Email, EqualTo, Length, Regexp, ValidationError)

from ..models.auth import User


class Password(FlaskForm):
    password = PasswordField(
        'Пароль:',
        validators=[DataRequired(message='без пароля не пустит')])


class CaptchaGroup:
    captcha = StringField(
        'Код картинки:',
        validators=[DataRequired(message='введите код с картинки')])
    suffix = StringField(
        'Суфикс:',
        validators=[DataRequired(message='обязательное поле')])


class LoginForm(Password, CaptchaGroup):
    login = StringField(
        'Логин:',
        validators=[DataRequired(message='введите псевдоним или адрес почты')])
    remember_me = BooleanField('Хранить сессию')
    submit = SubmitField('Войти в сервис')


class GetPassword(FlaskForm, CaptchaGroup):
    address = StringField(
        'Емэйл адрес:',
        validators=[DataRequired(message='без адреса никак, увы'),
                    Email(message='нужен адрес электронной почты'),
                    Length(
                        max=128,
                        message='максимальная длина адреса 128 знаков')])
    submit = SubmitField('Получить пароль')


class Passwords(FlaskForm):
    password = PasswordField(
        'Новый пароль:',
        validators=[DataRequired(message='без пароля никак, увы'),
                    EqualTo('confirmation', message='пароли не совпадают')])
    confirmation = PasswordField(
        'Повторить:',
        validators=[DataRequired(message='без пароля никак, увы'),
                    EqualTo('password', message='пароли не совпадают')])


class CreatePassword(Passwords, CaptchaGroup):
    username = StringField(
        'Псевдоним:',
        validators=[DataRequired(message='без псевдонима никак, увы'),
                    Length(
                        min=3, max=16,
                        message='от 3-х до 16-ти знаков'),
                    Regexp(
                        r'^[A-Za-z][a-zA-Z0-9\-_.]*$',
                        message='латинские буквы, цифры, дефис, знак\
                         подчеркивания, точка, первый символ - латинская буква'
                    )])
    submit = SubmitField('Создать пароль')

    def validate_username(self, field):
        if User.query.filter_by(username=field.data).first():
            raise ValidationError('этот псевдоним уже используется')


class ResetPassword(Passwords, CaptchaGroup):
    address = StringField(
        'Емэйл Адрес:',
        validators=[DataRequired(message='без адреса никак, увы'),
                    Email(message='нужен адрес электронной почты'),
                    Length(
                        max=128,
                        message='максимальная длина адреса - 128 знаков')])
    submit = SubmitField('Обновить пароль')


class ChangePassword(Passwords):
    current = PasswordField(
        'Текущий пароль:',
        validators=[DataRequired(message='без пароля не пустит')])
    submit = SubmitField('Сменить пароль')

    def validate_current(self, field):
        if not current_user.verify_password(field.data):
            raise ValidationError('текущий пароль недействителен')


class EmailRequest(FlaskForm):
    swap = StringField(
        'Новый адрес:',
        validators=[DataRequired(message='без адреса никак, увы'),
                    Email(message='нужен адрес электронной почты'),
                    Length(
                        max=128,
                        message='максимальная длина адреса 128 знаков')])
    submit = SubmitField('Отправить запрос')


class ChangeEmail(Password, CaptchaGroup):
    submit = SubmitField('Сменить адрес')

    def validate_password(self, field):
        if not current_user.verify_password(field.data):
            raise ValidationError('текущий пароль недействителен')

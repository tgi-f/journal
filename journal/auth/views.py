from datetime import datetime, timedelta

from flask import (
    abort, current_app, flash, redirect, render_template, request, url_for)
from flask_login import current_user, login_required, login_user, logout_user
from sqlalchemy.sql import func
from validate_email import validate_email

from . import auth
from .forms import (
    CreatePassword, ChangeEmail, ChangePassword, EmailRequest,
    GetPassword, LoginForm, ResetPassword)
from .tools import check_token, define_target
from .. import db
from ..models.auth import Account, User
from ..models.auth_units import permissions
from ..models.cache import connect, assign_cache
from ..models.captcha import Captcha
from ..tasks.auth import (
    change_pattern, remove_swap, request_password, request_email_change)


@auth.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    captcha = Captcha.query.order_by(func.random()).first()
    rc = connect(current_app.config.get('CACHE_HOST', 'localhost'))
    form = LoginForm()
    if form.validate_on_submit():
        user = None
        if validate_email(form.login.data):
            acc = Account.query.filter_by(address=form.login.data).first()
            if acc:
                user = acc.user
        else:
            user = User.query.filter_by(username=form.login.data).first()
        if user and user.verify_password(form.password.data):
            value = rc.get(form.suffix.data)
            if not value or value.decode('utf-8') != form.captcha.data:
                flash('Тест провален, либо устарел, попробуйте снова.')
                return redirect(
                    url_for('auth.login', next=request.args.get('next')))
            login_user(user, form.remember_me.data)
            change_pattern.delay(form.suffix.data)
            return redirect(request.args.get('next', url_for('main.index')))
        flash('Неверный логин или пароль, вход невозможен.')
    form.suffix.data = assign_cache(rc, 'login:', captcha.value, 120)
    form.captcha.data = ''
    return render_template(
        'auth/login.html', form=form,
        now=datetime.utcnow(), suffix=captcha.suffix)


@auth.route('/get-password', methods=['GET', 'POST'])
def get_password():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    captcha = Captcha.query.order_by(func.random()).first()
    rc = connect(current_app.config.get('CACHE_HOST', 'localhost'))
    form = GetPassword()
    if form.validate_on_submit():
        acc = Account.query.filter_by(address=form.address.data).first()
        swapped = Account.query.filter_by(swap=form.address.data).first()
        delta = timedelta(
            seconds=round(3600*current_app.config.get('REQUEST_INTERVAL')))
        if acc and datetime.utcnow() - acc.requested < delta:
            flash('Сервис временно недоступен, попробуйте зайти позже.')
            return redirect(url_for('auth.get_password'))
        if swapped:
            flash('Адрес в свопе, выберите другой или попробуйте позже.')
            return redirect(url_for('auth.get_password'))
        if not current_app.config.get('TESTING'):
            value = rc.get(form.suffix.data)
            if not value or value.decode('utf-8') != form.captcha.data:
                flash('Тест провален либо устарел, попробуйте снова.')
                return redirect(url_for('auth.get_password'))
            request_password.delay(
                form.address.data,
                url_for('main.index', _external=True), define_target(acc))
        change_pattern.delay(form.suffix.data)
        flash('На Ваш адрес выслано письмо с инструкциями.')
        return redirect(url_for('auth.login'))
    form.suffix.data = assign_cache(rc, 'request:', captcha.value, 120)
    form.captcha.data = ''
    return render_template(
        'auth/get_password.html', form=form,
        now=datetime.utcnow(), suffix=captcha.suffix)


@auth.route('/reset-password/<token>', methods=['GET', 'POST'])
def reset_password(token):
    if current_user.is_authenticated:
        abort(404)
    source = check_token(token)
    if source is None or source.user is None \
            or source.user.last_visit > source.requested or source.swap:
        abort(404)
    captcha = Captcha.query.order_by(func.random()).first()
    rc = connect(current_app.config.get('CACHE_HOST', 'localhost'))
    form = ResetPassword()
    if form.validate_on_submit():
        value = rc.get(form.suffix.data)
        if not value or value.decode('utf-8') != form.captcha.data:
            flash('Тест провален, либо устарел, попробуйте снова.')
            return redirect(url_for('auth.reset_password', token=token))
        acc = Account.query.filter_by(address=form.address.data).first()
        if acc != source:
            change_pattern.delay(form.suffix.data)
            flash('Неверный запрос, действие отклонено.')
            return redirect(url_for('auth.login'))
        acc.user.password = form.password.data
        acc.user.last_visit = datetime.utcnow()
        db.session.add(acc)
        db.session.commit()
        change_pattern.delay(form.suffix.data)
        flash('У Вас новый пароль.')
        return redirect(url_for('auth.login'))
    form.suffix.data = assign_cache(rc, 'reset:', captcha.value, 120)
    form.captcha.data = ''
    return render_template(
        'auth/reset_password.html', form=form,
        now=datetime.utcnow(), suffix=captcha.suffix)


@auth.route('/create-password/<token>', methods=['GET', 'POST'])
def create_password(token):
    acc = check_token(token)
    if acc is None or current_user.is_authenticated or acc.user:
        abort(404)
    captcha = Captcha.query.order_by(func.random()).first()
    rc = connect(current_app.config.get('CACHE_HOST', 'localhost'))
    form = CreatePassword()
    if form.validate_on_submit():
        value = rc.get(form.suffix.data)
        if not value or value.decode('utf-8') != form.captcha.data:
            flash('Тест провален, либо устарел, попробуйте снова.')
            return redirect(url_for('auth.create_password', token=token))
        user = User(username=form.username.data, password=form.password.data)
        user.account = acc
        db.session.add(user)
        db.session.commit()
        change_pattern.delay(form.suffix.data)
        flash('Для пользователя {} пароль успешно создан.'
              .format(user.username))
        return redirect(url_for('auth.login'))
    form.suffix.data = assign_cache(rc, 'create:', captcha.value, 120)
    form.captcha.data = ''
    return render_template(
        'auth/create_password.html', form=form,
        now=datetime.utcnow(), suffix=captcha.suffix)


@auth.route('/change-password', methods=['GET', 'POST'])
@login_required
def change_password():
    form = ChangePassword()
    if form.validate_on_submit():
        current_user.password = form.password.data
        db.session.add(current_user)
        db.session.commit()
        flash('Уважаемый {}, у Вас новый пароль.'.format(current_user.username))
        return redirect(
            url_for('main.show_profile', username=current_user.username))
    return render_template(
        'auth/change_password.html', form=form, now=datetime.utcnow())


@auth.route('/request-email', methods=['GET', 'POST'])
@login_required
def request_email():
    form = EmailRequest()
    if form.validate_on_submit():
        acc = current_user.account
        delta = timedelta(
            seconds=round(3600*current_app.config.get('REQUEST_INTERVAL')))
        if datetime.utcnow() - acc.requested < delta:
            flash('Сервис временно недоступен, попробуйте позже.')
            return redirect(url_for('auth.request_email'))
        if acc.address == form.swap.data:
            flash('Задан Ваш текущий адрес, запрос не имеет смысла.')
            return redirect(url_for('auth.request_email'))
        swapped = Account.query.filter_by(swap=form.swap.data).first()
        if swapped:
            flash('Адрес в свопе, выберите другой или попробуйте позже.')
            return redirect(url_for('auth.request_email'))
        requested = Account.query.filter_by(address=form.swap.data).first()
        if requested and requested.user:
            flash('Этот адрес уже зарегистрирован, запрос отклонён.')
            return redirect(url_for('auth.request_email'))
        if not current_app.config.get('TESTING'):
            request_email_change.delay(
                form.swap.data, acc.address,
                url_for('main.index', _external=True),
                url_for('auth.change_email', token='token', _external=True))
            remove_swap.apply_async(
                args=[acc.address],
                countdown=round(3600*current_app.config.get('TOKEN_LENGTH', 1)))
        flash('На {} отослано сообщение с инструкциями.'
              .format(form.swap.data))
        return redirect(
            url_for('main.show_profile', username=current_user.username))
    return render_template(
        'auth/request_email.html', form=form, now=datetime.utcnow())


@auth.route('/change-email/<token>', methods=['GET', 'POST'])
@login_required
def change_email(token):
    acc = check_token(token)
    swap = current_user.account.swap
    if acc is None or current_user.account != acc or swap is None:
        abort(404)
    captcha = Captcha.query.order_by(func.random()).first()
    rc = connect(current_app.config.get('CACHE_HOST', 'localhost'))
    form = ChangeEmail()
    if form.validate_on_submit():
        value = rc.get(form.suffix.data)
        if not value or value.decode('utf-8') != form.captcha.data:
            flash('Тест провален, либо устарел, попробуйте снова.')
            return redirect(url_for('auth.change_email', token=token))
        current_user.account.address = swap
        current_user.account.swap = None
        db.session.add(current_user.account)
        db.session.commit()
        change_pattern.delay(form.suffix.data)
        flash('Адрес в Вашем аккаунте успешно изменён.')
        return redirect(
            url_for('main.show_profile', username=current_user.username))
    form.suffix.data = assign_cache(rc, 'change:', captcha.value, 120)
    form.captcha.data = ''
    return render_template(
        'auth/change_email.html', form=form, swap=swap,
        now=datetime.utcnow(), suffix=captcha.suffix)


@auth.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.ping()
        if current_user.can(permissions.CANNOT_LOG_IN):
            logout_user()
            flash('Ваше присутствие в сервисе нежелательно.')
            return redirect(url_for('auth.login'))


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.index'))

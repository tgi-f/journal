from datetime import datetime

from flask import current_app, render_template, url_for
from flask_mail import Message
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, BadData

from .. import db
from ..models.auth import Account


def define_target(account):
    if account and account.user:
        return url_for('auth.reset_password', token='token', _external=True)
    return url_for('auth.create_password', token='token', _external=True)


class Resolver:
    def __init__(self, account, address):
        self.username, self.subj, self.template = self._define(account)
        self.account = self._get_account(account, address)
        db.session.add(self.account)
        db.session.commit()

    def _get_account(self, account, address):
        if account:
            account.swap = None
            account.requested = datetime.utcnow()
            return account
        return Account(address=address)

    def _define(self, account):
        if account and account.user:
            return (
                account.user.username,
                'Password Recovery',
                'emails/auth/reset_password')
        return 'Гость', 'Registration', 'emails/auth/invitation'


def check_token(token):
    s = Serializer(current_app.config.get('SECRET_KEY'))
    try:
        account = Account.query.get(s.loads(token).get('confirm'))
    except BadData:
        account = None
    return account


def resolve_account(swap, address):
    requested = Account.query.filter_by(address=swap).first()
    account = Account.query.filter_by(address=address).first()
    if requested and not requested.user:
        db.session.delete(requested)
    account.swap = swap
    account.requested = datetime.utcnow()
    db.session.add(account)
    db.session.commit()
    return account


def create_message(app, recipients, subject, template, **kwargs):
    """
    This function creates a message and returns it
    Args:
        app: the application
        recipients: a list of recipients, must be a list
        subject: a string that contains the subject
        template: a string that contains the route of a template
        **kwargs:

    Returns: an instance of flask_mail.Message

    """
    msg = Message(
        app.config.get('J_SUBJECT_PREFIX') + subject,
        sender=app.config.get('J_SENDER'),
        recipients=recipients)  # recipients must be a list
    msg.body = render_template(template + '.txt', **kwargs)
    msg.html = render_template(template + '.html', **kwargs)
    return msg

import re

from getpass import getpass

from validate_email import validate_email

from .models.auth import Account, User


MESSAGE = """Username must be from 3 to 16 symbols (latin letters, numbers,
dots, hyphens, underscores) and start with any latin letter.
"""

U = 'Enter your username: '
E = 'Enter your email address: '
P = 'Enter your password: '


def get_username():
    pattern = re.compile(r'[A-Za-z][a-zA-Z0-9\-_.]{2,15}$')
    username = input(U)
    while True:
        if not pattern.match(username):
            print(MESSAGE)
            username = input(U)
            continue
        if User.query.filter_by(username=username).first():
            print('This name is already registered.')
            username = input(U)
            continue
        return username


def get_email():
    address = input(E)
    while True:
        if not validate_email(address):
            print('This is not a valid email address.')
            address = input(E)
            continue
        account = Account.query.filter_by(address=address).first()
        swapped = Account.query.filter_by(swap=address).first()
        if (account and account.user) or swapped:
            print('This email address cannot be registered.')
            address = input(E)
            continue
        return address


def get_password():
    phrase = getpass(P)
    while True:
        confirm = getpass('Confirm the password: ')
        if confirm != phrase:
            print('Passwords must match!')
            phrase = getpass(P)
            continue
        return phrase

import re

from .. import db
from ..models.labels import Label


def set_labels(names, entity):
    ultimate = [name for name in re.split(r' *[,;] *', names) if name]
    for label in entity.labels:
        if label.name not in ultimate:
            entity.labels.remove(label)
            if label.is_empty:
                db.session.delete(label)
    current = [label.name for label in entity.labels]
    for name in ultimate:
        if name not in current:
            label = Label.query.filter_by(name=name).first()
            if not label:
                label = Label(name=name)
                db.session.add(label)
            entity.labels.append(label)

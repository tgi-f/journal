import re

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, ValidationError


class EditLabels(FlaskForm):
    names = StringField('Метки:')
    submit = SubmitField('Установить')

    def validate_names(self, field):
        data = [name for name in re.split(r' *[,;] *', field.data) if name]
        for name in data:
            if not re.match(r'^[\w-]{1,32}$', name):
                message = "'{}' - недопустимая метка".format(name)
                raise ValidationError(message)

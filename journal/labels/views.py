from flask import abort, flash, redirect, render_template, url_for
from flask_login import current_user

from . import labels
from .forms import EditLabels
from .tools import set_labels
from ..decoration import permission_required
from ..models.auth_units import permissions
from ..models.blogs import Item
from ..models.lists import List


@labels.route('/edit/<suffix>', methods=['GET', 'POST'])
@permission_required(permissions.CREATE_ENTITY)
def edit_labels(suffix):
    entity = len(suffix)
    if not 7 <= entity <= 8:
        abort(404)
    d = {7: (List, 'lists.show_list'),
         8: (Item, 'blogs.show_item')}
    target = d[entity][0].query.filter_by(suffix=suffix).first_or_404()
    if target.owner != current_user:
        abort(403)
    form = EditLabels()
    if form.validate_on_submit():
        set_labels(form.names.data, target)
        flash('Метки установлены.')
        return redirect(url_for(d[entity][1], slug=target.slug))
    form.names.data = ', '.join([label.name for label in target.labels])
    return render_template(
        'labels/edit.html', form=form, target=target, entity=entity)

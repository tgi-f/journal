from datetime import datetime

from flask import (
    abort, current_app, flash, jsonify, redirect, render_template,
    request, url_for)
from flask_login import current_user, login_required

from . import blogs
from .forms import BondItem, CreateItem
from .tools import (
    censor, change_target_attr, create_section, parse_data, parse_follow)
from .. import csrf, db
from ..accessories import increment_views
from ..announce.tools import sort_announces, sort_heap_announces
from ..decoration import permission_required
from ..models.auth import Follow, User
from ..models.auth_units import permissions
from ..models.blogs import Item
from ..models.commentaries import Commentary
from ..models.labels import Label
from ..models.lists import List
from ..tasks.blogs import create_item, edit_body, remove_blog_item


@blogs.route('/', methods=['GET', 'POST'])
@login_required
def show_journal():
    if not current_user.can(permissions.CREATE_ENTITY)\
            and not current_user.items.first():
        return redirect(url_for('blogs.show_heap'))
    form = None
    if current_user.can(permissions.CREATE_ENTITY):
        form = CreateItem()
        if form.validate_on_submit():
            task = create_item.delay(
                form.abstract.data, form.body.data,
                form.hidden.data, form.public.data,
                current_user.username)
            message = 'Создаю новый топик блога, дождитесь перенаправления.'
            return render_template(
                'item_redirect.html', entity='item',
                task_id=task.id, message=message, timeout=800)
    pagination = Item.query.filter(Item.owner == current_user)\
        .order_by(Item.edited.desc())\
        .paginate(
            page=request.args.get('page', 1, type=int),
            per_page=current_app.config.get('POSTS_PER_PAGE', 3),
            error_out=True)
    return render_template(
        'blogs/current_blogs.html', form=form, pagination=pagination,
        now=datetime.utcnow(), view=1)


@blogs.route('/tag/<labelname>')
@login_required
def sort_labels(labelname):
    label = Label.query.filter_by(name=labelname).first_or_404()
    pagination = label.items.filter(Item.owner == current_user)\
        .order_by(Item.edited.desc()).paginate(
        page=request.args.get('page', 1, type=int),
        per_page=current_app.config.get('POSTS_PER_PAGE', 3),
        error_out=True)
    if not pagination.items:
        abort(404)
    return render_template(
        'blogs/current_blogs.html', form=None, pagination=pagination,
        view=5, labelname=label.name)


@blogs.route('/society/show/heap')
def show_heap():
    if current_user.can(permissions.BLOCK_ENTITY):
        pagination = Item.query.filter(
            Item.public.is_(True),
            Item.owner_id.isnot(None),
            Item.censored.is_(False)).order_by(Item.published.desc()).paginate(
            page=request.args.get('page', 1, type=int),
            per_page=current_app.config.get('POSTS_PER_PAGE', 3),
            error_out=True)
    else:
        pagination = Item.query.filter(
            Item.hidden.is_(False),
            Item.public.is_(True),
            Item.owner_id.isnot(None),
            Item.censored.is_(False)).order_by(Item.published.desc()).paginate(
            page=request.args.get('page', 1, type=int),
            per_page=current_app.config.get('POSTS_PER_PAGE', 3),
            error_out=True)
    a_s = sort_heap_announces()
    return render_template(
        'blogs/current_blogs.html', pagination=pagination, form=None,
        view=2, a_s=a_s)


@blogs.route('/society/show/heap/tag/<labelname>')
def sort_heap_labels(labelname):
    label = Label.query.filter_by(name=labelname).first_or_404()
    if current_user.can(permissions.BLOCK_ENTITY):
        pagination = label.items.filter(
            Item.public.is_(True),
            Item.owner_id.isnot(None),
            Item.censored.is_(False)).order_by(Item.published.desc()).paginate(
            page=request.args.get('page', 1, type=int),
            per_page=current_app.config.get('POSTS_PER_PAGE', 3),
            error_out=True)
    else:
        pagination = label.items.filter(
            Item.hidden.is_(False),
            Item.public.is_(True),
            Item.owner_id.isnot(None),
            Item.censored.is_(False)).order_by(Item.published.desc()).paginate(
            page=request.args.get('page', 1, type=int),
            per_page=current_app.config.get('POSTS_PER_PAGE', 3),
            error_out=True)
    if not pagination.items:
        abort(404)
    return render_template(
        'blogs/current_blogs.html', form=None, pagination=pagination,
        view=6, labelname=label.name)


@blogs.route('/society/show/censored')
@permission_required(permissions.BLOCK_ENTITY)
def show_censored():
    pagination = Item.query.filter(
        Item.censored.is_(True),
        Item.owner_id.isnot(None)).order_by(Item.published.desc()).paginate(
        page=request.args.get('page', 1, type=int),
        per_page=current_app.config.get('POSTS_PER_PAGE', 3),
        error_out=True)
    return render_template(
        'blogs/current_blogs.html', pagination=pagination, form=None,
        view=9)


@blogs.route('/society/show/tape')
@login_required
def show_tape():
    pagination = current_user.followed_items.paginate(
        page=request.args.get('page', 1, type=int),
        per_page=current_app.config.get('POSTS_PER_PAGE', 3),
        error_out=True)
    if not pagination.items:
        abort(404)
    a_s = sort_heap_announces()
    return render_template(
        'blogs/current_blogs.html', pagination=pagination, form=None,
        view=3, a_s=a_s)


@blogs.route('/society/show/tape/tag/<labelname>')
@login_required
def sort_tape_labels(labelname):
    label = Label.query.filter_by(name=labelname).first_or_404()
    pagination = label.items.join(Follow, Follow.followed_id == Item.owner_id)\
        .filter(Follow.follower_id == current_user.id,
                Item.public.is_(True), Item.censored.is_(False))\
        .order_by(Item.published.desc()).paginate(
        page=request.args.get('page', 1, type=int),
        per_page=current_app.config.get('POSTS_PER_PAGE', 3),
        error_out=True)
    if not pagination.items:
        abort(404)
    return render_template(
        'blogs/current_blogs.html', form=None, pagination=pagination,
        view=7, labelname=label.name)


@blogs.route('/society/<username>')
def show_owner(username):
    target = User.query.filter_by(username=username).first_or_404()
    if current_user == target:
        return redirect(url_for('blogs.show_journal'))
    pagination = target.items.filter(
        Item.hidden.is_(False),
        Item.public.is_(True),
        Item.censored.is_(False)).order_by(
        Item.published.desc()).paginate(
        page=request.args.get('page', 1, type=int),
        per_page=current_app.config.get('POSTS_PER_PAGE', 3),
        error_out=True)
    if not pagination.items:
        abort(404)
    a_s = sort_announces(target)
    return render_template(
        'blogs/current_blogs.html', pagination=pagination, form=None, view=4,
        username=target.username, a_s=a_s)


@blogs.route('/society/<username>/tag/<labelname>')
def sort_owner_labels(username, labelname):
    target = User.query.filter_by(username=username).first_or_404()
    label = Label.query.filter_by(name=labelname).first_or_404()
    if current_user == target:
        return redirect(url_for('blogs.sort_labels', labelname=labelname))
    pagination = label.items.filter(
        Item.owner == target,
        Item.hidden.is_(False),
        Item.public.is_(True),
        Item.censored.is_(False)).order_by(Item.published.desc()).paginate(
        page=request.args.get('page', 1, type=int),
        per_page=current_app.config.get('POSTS_PER_PAGE', 3),
        error_out=True)
    if not pagination.items:
        abort(404)
    return render_template(
        'blogs/current_blogs.html', form=None, pagination=pagination,
        view=8, username=target.username, labelname=label.name)


@blogs.route('/<slug>')
def show_item(slug):
    target = Item.query.filter_by(slug=slug).first_or_404()
    if current_user != target.owner and not target.public:
        if current_user.can(permissions.BLOCK_ENTITY) and target.censored:
            pass
        else:
            abort(403)
    pagination = Commentary.query.filter_by(item_id=target.id)\
        .filter(Commentary.parent_id.is_(None))\
        .order_by(Commentary.timestamp.asc()).paginate(
        page=request.args.get('page', 1, type=int),
        per_page=current_app.config.get('COMMENTS_PER_PAGE', 3),
        error_out=True)
    return render_template(
        'blogs/show_item.html', item=target, pagination=pagination)


@blogs.route('/action/count-clicks', methods=['POST'])
@csrf.exempt
def count_clicks():
    result = {'empty': True}
    target = Item.query.filter_by(
        suffix=request.form.get('suffix', 'rand-S', type=str)).first()
    if target and current_user != target.owner:
        increment_views(target, 'b_clicked')
        result = {'empty': False,
                  'clicks': target.clicked}
    return jsonify(result)


@blogs.route('/action/remove-item', methods=['POST'])
def remove_item():
    result = {'task_id': None}
    target = Item.query.filter_by(
        suffix=request.form.get('suffix', 'rand-S', type=str)).first()
    if target and current_user == target.owner:
        task = remove_blog_item.delay(target.suffix)
        result = {'task_id': task.id,
                  'redirect': url_for('blogs.show_journal')}
    return jsonify(result)


@blogs.route('/action/follow-owner', methods=['POST'])
def follow_owner():
    result = {'empty': True}
    target, endpoint = parse_follow(request)
    if target and current_user.is_authenticated:
        current_user.follow(target.owner)
        result = {'empty': False,
                  'redirect': url_for(endpoint, slug=target.slug)}
        flash('{0} добавлен в Вашу ленту.'.format(target.owner.username))
    return jsonify(result)


@blogs.route('/action/unfollow-owner', methods=['POST'])
def unfollow_owner():
    result = {'empty': True}
    target, endpoint = parse_follow(request)
    if target and current_user.is_authenticated:
        current_user.unfollow(target.owner)
        result = {'empty': False,
                  'redirect': url_for(endpoint, slug=target.slug)}
        flash('{0} удалён из Вашей ленты.'.format(target.owner.username))
    return jsonify(result)


@blogs.route('/action/add-like', methods=['POST'])
def add_like():
    result = {'empty': True}
    target, options, details = parse_data(request)
    if target and current_user.is_authenticated:
        if current_user in target.d_users:
            target.d_users.remove(current_user)
        elif current_user not in target.l_users:
            target.l_users.append(current_user)
        db.session.add(target)
        db.session.commit()
        result = {'empty': False,
                  'options': render_template(options, item=target),
                  'details': render_template(details, item=target),
                  'infoline': None,
                  'c_options': None}
    return jsonify(result)


@blogs.route('/action/add-dislike', methods=['POST'])
def add_dislike():
    result = {'empty': True}
    target, options, details = parse_data(request)
    if target and current_user.is_authenticated:
        if current_user in target.l_users:
            target.l_users.remove(current_user)
        elif current_user not in target.d_users:
            target.d_users.append(current_user)
        db.session.add(target)
        db.session.commit()
        result = {'empty': False,
                  'options': render_template(options, item=target),
                  'details': render_template(details, item=target),
                  'infoline': None,
                  'c_options': None}
    return jsonify(result)


@blogs.route('/action/change-public', methods=['POST'])
def change_public():
    result = {'empty': True}
    target, options, details = parse_data(request)
    if target and current_user == target.owner:
        target = change_target_attr(target, 'public')
        result = {'empty': False,
                  'options': render_template(options, item=target),
                  'details': render_template(details, item=target),
                  'infoline': render_template(
                      'blogs/infoline.html', item=target),
                  'c_options': None}
    return jsonify(result)


@blogs.route('/action/change-hidden', methods=['POST'])
def change_hidden():
    result = {'empty': True}
    target, options, details = parse_data(request)
    if target and current_user == target.owner:
        target = change_target_attr(target, 'hidden')
        result = {'empty': False,
                  'options': render_template(options, item=target),
                  'details': render_template(details, item=target),
                  'infoline': None,
                  'c_options': None}
    return jsonify(result)


@blogs.route('/action/change-censored', methods=['POST'])
def change_censored():
    result = {'empty': True}
    target, _, _ = parse_data(request)
    if target and current_user.can(permissions.BLOCK_ENTITY) and \
            not target.owner.is_root():
        destination, message = censor(target)
        result = {'empty': False,
                  'redirect': destination}
        flash(message)
    return jsonify(result)


@blogs.route('/action/change-comments-state', methods=['POST'])
def change_comments():
    result = {'emtpy': True}
    target, options, details = parse_data(request)
    length = request.form.get('length', 0, type=int)
    if target and current_user == target.owner:
        target = change_target_attr(target, 'commentable')
        result = {'empty': False,
                  'options': render_template(options, item=target),
                  'details': render_template(details, item=target),
                  'infoline': None,
                  'c_options': render_template(
                      'blogs/c_options.html',
                      item=target, page=1, length=length)}
    return jsonify(result)


@blogs.route('/action/edit-abstract', methods=['POST'])
def edit_abstract():
    result = {'empty': True}
    target = Item.query.filter_by(
        suffix=request.form.get('suffix', 'rand-S', type=str)).first()
    abstract = request.form.get('abstract', None, type=str)
    if target and current_user == target.owner and abstract:
        if target.abstract != abstract:
            target.abstract = abstract
            target.slug = target.gen_slug(abstract)
            target.edited = datetime.utcnow()
            db.session.add(target)
            db.session.commit()
            flash('Заголовок топика отредактирован.')
        else:
            flash('Заголовок топика не изменился.')
        result = {'empty': False,
                  'redirect': url_for('blogs.show_item', slug=target.slug)}
    return jsonify(result)


@blogs.route('/action/edit-text', methods=['POST'])
def edit_text():
    result = {'empty': True}
    target = Item.query.filter_by(
        suffix=request.form.get('suffix', 'rand-S', type=str)).first()
    text = request.form.get('text', None, type=str)
    if target and current_user == target.owner and text \
            and len(text.replace('\r\n', '\n')) <= 65000:
        if target.body != text:
            task = edit_body.delay(target.suffix, text)
            task_id = task.id
        else:
            task_id = None
            flash('Текст топика не изменился.')
        result = {'empty': False,
                  'task_id': task_id,
                  'redirect': url_for('blogs.show_item', slug=target.slug)}
    return jsonify(result)


@blogs.route('/action/bond-item', methods=['POST'])
def bond_item():
    result = {'empty': True}
    target = Item.query.filter_by(
        suffix=request.form.get('suffix', 'rand-S', type=str)).first()
    choices = [(list_.id, list_.title) for list_ in
               target.owner.lists.filter_by(open=True)]
    if target and current_user == target.owner:
        form = BondItem(choices)
        if form.validate_on_submit():
            l = List.query.get(form.list_.data)
            create_section(l, target)
            flash('Список дополнен.')
            return redirect(url_for('lists.show_list', slug=l.slug))
        result = {'empty': False,
                  'content': render_template(
                      'blogs/bond_item.html', form=form,
                      choices=choices, suffix=target.suffix)}
    return jsonify(result)

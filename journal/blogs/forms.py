from flask_wtf import FlaskForm
from wtforms import (
    BooleanField, SelectField, StringField, SubmitField, TextAreaField)
from wtforms.validators import DataRequired, Length, ValidationError


class CreateItem(FlaskForm):
    abstract = StringField(
        'Заголовок:',
        validators=[
            DataRequired(message='без заголовка нельзя'),
            Length(min=1, max=100, message='от 1-ого до 100-а символов')])
    body = TextAreaField(
        'Описание:',
        validators=[DataRequired(message='текст отсутствует')])
    public = BooleanField('Публичный')
    hidden = BooleanField('Скрытый')
    submit = SubmitField('Сохранить')

    def validate_body(self, field):
        field.data = field.data.replace('\r\n', '\n')
        if len(field.data) > 65000:
            raise ValidationError('поле содержит больше 65000 символов')


class BondItem(FlaskForm):
    suffix = StringField(
        'Суффикс:',
        validators=[DataRequired(message='без суффикса никак')])
    list_ = SelectField('Список', coerce=int)
    submit = SubmitField('Связать')

    def __init__(self, choices, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.list_.choices = choices

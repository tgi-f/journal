from datetime import datetime

from flask import url_for

from .. import db
from ..models.blogs import Item
from ..models.lists import List, Section


def parse_data(request):
    """
    Args:
        request: the request

    Returns:
         target,
         options (the options template),
         details (the details template)

    """
    entity = request.form.get('entity', 'item', type=str)
    d = {'item': (Item, 'blogs/options.html', 'blogs/details.html'),
         'list_': (List, 'lists/options.html', 'lists/details.html')}
    return (d[entity][0].query.filter_by(
        suffix=request.form.get('suffix', 'rand-S', type=str)).first(),
            d[entity][1], d[entity][2])


def parse_follow(request):
    """
    Args:
        request: the request

    Returns: target, endpoint (the view function for redirection)

    """
    entity = request.form.get('entity', 'item', type=str)
    d = {'item': (Item, 'blogs.show_item'),
         'list_': (List, 'lists.show_list')}
    return (d[entity][0].query.filter_by(
        suffix=request.form.get('suffix', 'rand-S', type=str)).first(),
            d[entity][1])


def change_target_attr(target, attr):
    if getattr(target, attr):
        setattr(target, attr, False)
    else:
        if attr == 'public' and target.published == target.created:
            target.published = datetime.utcnow()
        setattr(target, attr, True)
    db.session.add(target)
    db.session.commit()
    return target


def censor(target):
    destination, message = None, None
    if target.censored:
        target.censored = False
        if isinstance(target, Item):
            message = 'Топик разблокирован.'
            destination = url_for('blogs.show_censored')
        elif isinstance(target, List):
            message = 'Список разблокирован.'
            destination = url_for('lists.show_censored')
    else:
        target.censored = True
        target.public = False
        if isinstance(target, Item):
            for section in target.sections:
                section.list_.censored = True
                section.list_.public = False
                db.session.add(section)
            message = 'Топик заблокирован.'
            destination = url_for('blogs.show_item', slug=target.slug)
        elif isinstance(target, List):
            message = 'Список заблокирован.'
            destination = url_for('lists.show_list', slug=target.slug)
    db.session.add(target)
    db.session.commit()
    return destination, message


def create_section(list_, item):
    section = Section(number=list_.current_)
    section.item = item
    section.list_ = list_
    list_.edited = datetime.utcnow()
    db.session.add(section)
    db.session.commit()

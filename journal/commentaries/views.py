from flask import flash, jsonify, render_template, request
from flask_login import current_user

from . import commentaries
from .tools import check_suffix, check_commentary, get_target, prove_permissions
from ..models.auth import User
from ..models.blogs import Item
from ..models.commentaries import Commentary
from ..models.lists import List
from ..tasks.commentaries import create_commentary


@commentaries.route('/generate-form', methods=['POST'])
def generate_form():
    result = {'data': None}
    entity = request.form.get('entity', 'item', type=str)
    d = {'item': Item, 'list_': List}
    target = d[entity].query.filter_by(
        suffix=request.form.get('suffix', 'rand-S', type=str)).first()
    if target:
        result = {'data': render_template(
            'commentaries/prompt.html',
            target=target,
            parent=request.form.get('parent', None, type=int),
            page=request.form.get('page', None, type=int))}
    return jsonify(result)


@commentaries.route('/create-commentary', methods=['POST'])
def send_commentary():
    result = {'task_id': None}
    suffix = request.form.get('suffix', 'rand-S', type=str)
    body = request.form.get('body', None, type=str)
    if check_suffix(suffix) and body \
            and len(body.replace('\r\n', '\n')) <= 65000:
        entity, redirect = get_target(
            suffix, request.form.get('page', None, type=int))
        task = create_commentary.delay(
            entity, suffix, current_user.username,
            request.form.get('parent', 0, type=int), body)
        result = {'task_id': task.id,
                  'redirect': redirect}
    return jsonify(result)


@commentaries.route('/remove-commentary', methods=['POST'])
def remove_commentary():
    result = {'empty': True}
    commentary = Commentary.query.get(
        request.form.get('commentary', 0, type=int))
    if commentary and prove_permissions(commentary, current_user):
        result = {'empty': False,
                  'redirect': check_commentary(
                      commentary,
                      request.form.get('last', 0, type=int),
                      request.form.get('page', None, type=int))}
        flash('Комментарий удалён.')
    return jsonify(result)


@commentaries.route('/ban-commentator', methods=['POST'])
def ban_commentator():
    result = {'empty': True}
    target = User.query.filter_by(
        username=request.form.get('user', None, type=str)).first()
    suffix = request.form.get('suffix', 'rand-S', type=str)
    if check_suffix(suffix) and target and not target.is_root():
        if current_user.is_blocking(target):
            current_user.deblock(target)
            flash('{} разблокирован в Ваших пространствах.'
                  .format(target.username))
        else:
            current_user.block(target)
            flash('{} заблокирован в Ваших пространствах.'
                  .format(target.username))
        _, redirect = get_target(
            suffix, request.form.get('page', None, type=int))
        result = {'empty': False,
                  'redirect': redirect}
    return jsonify(result)

from flask import url_for

from .. import db
from ..models.auth_units import permissions
from ..models.blogs import Item
from ..models.commentaries import Commentary
from ..models.lists import List


def check_suffix(suffix):
    if 7 <= len(suffix) <= 8:
        return True
    return False


def get_target(suffix, page):
    if page == 1:
        page = None
    if len(suffix) == 7:
        target = List.query.filter_by(suffix=suffix).first()
        return (target.__class__.__name__,
                url_for('lists.show_list', slug=target.slug, page=page))
    elif len(suffix) == 8:
        target = Item.query.filter_by(suffix=suffix).first()
        return (target.__class__.__name__,
                url_for('blogs.show_item', slug=target.slug, page=page))


def prove_permissions(commentary, current_user):
    if commentary.item:
        entity_owner = commentary.item.owner
    else:
        entity_owner = commentary.list_.owner
    if current_user == commentary.author\
            or (current_user == entity_owner and
                not commentary.author.is_root())\
            or (current_user.can(permissions.REMOVE_COMMENTARY) and
                not commentary.author.is_root()):
        return True
    return False


def define_endpoint(commentary):
    if commentary.item:
        return commentary.item, 'blogs.show_item'
    else:
        return commentary.list_, 'lists.show_list'


def define_page(page, last, commentary, parent):
    if commentary:
        commentary = Commentary.query.get(commentary.id)
    if parent:
        parent = Commentary.query.get(parent.id)
    if commentary or parent:
        page = page
    else:
        if last:
            page = page - 1 or 1
    if page == 1:
        return None
    return page


def check_parent(commentary, parent, last, page):
    entity, endpoint = define_endpoint(commentary)
    if parent:
        elder = parent.parent
        if parent.deleted and not parent.children.first():
            db.session.delete(parent)
            db.session.commit()
            if elder:
                return check_parent(parent, elder, last, page)
    return url_for(
        endpoint, slug=entity.slug,
        page=define_page(page, last, commentary, parent))


def check_commentary(commentary, last, page):
    parent = commentary.parent
    if commentary.children.first():
        commentary.deleted = True
        db.session.add(commentary)
    else:
        db.session.delete(commentary)
    db.session.commit()
    return check_parent(commentary, parent, last, page)

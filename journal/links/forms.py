from urllib.parse import urlparse

from flask_wtf import FlaskForm
from wtforms.fields import StringField, SubmitField
from wtforms.validators import DataRequired, Length, URL, ValidationError


class CreateLink(FlaskForm):
    url = StringField(
        'URL:',
        validators=[DataRequired(message='обязательное для заполнения поле'),
                    Length(max=512, message='длина url не более 512 знаков'),
                    URL(message='здесь может быть только url-адрес')])
    submit = SubmitField('Добавить')

    def validate_url(self, field):
        parsed = urlparse(field.data)
        if parsed.scheme not in ('http', 'https'):
            raise ValidationError('поддерживаются только http и https ссылки')

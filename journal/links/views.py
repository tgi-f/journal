from datetime import datetime

from flask import (
    current_app, flash, jsonify, render_template, request, url_for)
from flask_login import current_user, login_required

from . import links
from .forms import CreateLink
from .. import db
from ..accessories import define_page
from ..decoration import permission_required
from ..models.auth_units import permissions
from ..models.links import Link
from ..tasks.links import create_alias


@links.route('/', methods=['GET', 'POST'])
@login_required
@permission_required(permissions.CREATE_LINK_ALIAS)
def show_links():
    page = request.args.get('page', 1, type=int)
    pagination = Link.query.filter_by(owner=current_user)\
        .order_by(Link.created.desc()).paginate(
        page, current_app.config.get('LINKS_PER_PAGE', 3), error_out=True)
    form = CreateLink()
    if form.validate_on_submit():
        repeated = Link.query.filter_by(
            owner=current_user, url=form.url.data).first()
        if repeated:
            flash('Алиас был создан ранее.')
            form.url.data = ''
            return render_template(
                'links/show_links.html', now=datetime.utcnow(), form=form,
                pagination=pagination, repeated=repeated)
        task = create_alias.delay(form.url.data, current_user.username)
        message = 'Создаю новый алиас.'
        return render_template(
            'redirect.html', task_id=task.id, message=message, timeout=800,
            dest=url_for('links.show_links'))
    return render_template(
        'links/show_links.html', now=datetime.utcnow(), form=form,
        pagination=pagination, repeated=None)


@links.route('/actions/remove-alias', methods=['POST'])
def remove_alias():
    page = define_page(
        request.form.get('page', 1, type=int),
        request.form.get('last', 0, type=int))
    result = {'empty': True}
    target = Link.query.filter_by(
        spring=request.form.get('suffix', 'random-S', type=str)).first()
    if target and target.owner == current_user:
        db.session.delete(target)
        db.session.commit()
        result = {'empty': False,
                  'redirect': url_for('links.show_links', page=page)}
        flash('Алиас успешно удалён.')
    return jsonify(result)

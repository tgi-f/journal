from flask import session

from . import db


def define_page(page, last):
    if last:
        page = page - 1 or 1
    if page == 1:
        return None
    return page


def increment_views(target, argument):
    clicked = session.get(argument, list())
    if target.suffix not in clicked:
        clicked.append(target.suffix)
        session[argument] = clicked
        target.clicked += 1
        db.session.add(target)
        db.session.commit()

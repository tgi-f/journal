from datetime import datetime

from flask import url_for

from .. import db
from ..models.pm import Message


def get_messages(sender, recipient, per_page):
    messages = Message.query.filter(
        Message.sender == sender,
        Message.recipient == recipient,
        Message.postponed.is_(False),
        Message.removed_by_sender.is_(False))\
        .union(Message.query.filter(
            Message.sender == recipient,
            Message.recipient == sender,
            Message.postponed.is_(False),
            Message.removed_by_recipient.is_(False)))\
        .order_by(Message.sent.asc())
    if messages.count() % per_page:
        last_page = messages.count() // per_page + 1
    else:
        last_page = messages.count() / per_page
    return messages, int(last_page) or 1


def receive_incoming(sender, recipient):
    incoming = Message.query.filter(
        Message.sender == sender,
        Message.recipient == recipient,
        Message.postponed.is_(False),
        Message.received.is_(None)).first()
    if incoming:
        incoming.received = datetime.utcnow()
        db.session.add(incoming)
        db.session.commit()
        return True
    return False


def restore_postponed(sender, recipient):
    postponed = Message.query.filter(
        Message.sender == sender,
        Message.recipient == recipient,
        Message.postponed.is_(True)).first()
    if postponed:
        postponed.postponed = False
        db.session.add(postponed)
        db.session.commit()


def remove_this_message(actor, message, page, last):
    redirect = None
    if last:
        page = None
    if actor == message.sender:
        if message.received is None or message.removed_by_recipient:
            db.session.delete(message)
        else:
            message.removed_by_sender = True
            db.session.add(message)
        redirect = url_for(
            'pm.show_conversation',
            username=message.recipient.username, page=page)
    elif actor == message.recipient:
        if message.removed_by_sender:
            db.session.delete(message)
        else:
            message.removed_by_recipient = True
            db.session.add(message)
        redirect = url_for(
            'pm.show_conversation',
            username=message.sender.username, page=page)
    db.session.commit()
    return redirect

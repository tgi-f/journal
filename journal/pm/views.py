from datetime import datetime
from sqlalchemy import and_, or_

from flask import (
    abort, current_app, jsonify, flash, render_template, request, url_for)
from flask_login import current_user, login_required

from . import pm
from .tools import (
    get_messages, receive_incoming, remove_this_message, restore_postponed)
from .. import db
from ..decoration import permission_required
from ..models.auth import User
from ..models.auth_units import permissions
from ..models.pm import Message
from ..tasks.pm import send_edited, send_pm


@pm.route('/conversation/<username>')
@login_required
@permission_required(permissions.SEND_PM)
def show_conversation(username):
    target = User.query.filter_by(username=username).first_or_404()
    if target == current_user:
        abort(404)
    if not target.can(permissions.SEND_PM):
        flash('{0} не может получать приватные сообщения.'
              .format(target.username))
    restore_postponed(current_user, target)
    if receive_incoming(target, current_user):
        flash('Получено новое сообщение.')
    per_page = current_app.config.get('PM_PER_PAGE', 3)
    messages, last_page = get_messages(current_user, target, per_page)
    page = request.args.get('page', last_page, type=int)
    pagination = messages.paginate(page, per_page=per_page, error_out=True)
    return render_template(
        'pm/conversation.html', pagination=pagination, target=target,
        last_page=last_page, now=datetime.utcnow())


@pm.route('/conversations')
@login_required
@permission_required(permissions.SEND_PM)
def show_conversations():
    pagination = Message.query.filter(
        Message.id.in_([message.id for message in [Message.query.filter(or_(
            and_(Message.sender == each,
                 Message.recipient == current_user,
                 Message.removed_by_recipient.is_(False)),
            and_(Message.recipient == each,
                 Message.sender == current_user,
                 Message.removed_by_sender.is_(False)))).order_by(
            Message.sent.desc()).first() for each in User.query.join(
            Message, User.received_messages).filter(
            Message.sender == current_user,
            Message.removed_by_sender.is_(False)).union(
            User.query.join(Message, User.sent_messages).filter(
                Message.recipient == current_user,
                Message.removed_by_recipient.is_(False)))] if message])
    ).order_by(Message.received.desc()).paginate(
        request.args.get('page', 1, type=int),
        per_page=current_app.config.get('FRIENDS_PER_PAGE', 3),
        error_out=True)
    return render_template('pm/conversations.html', pagination=pagination)


@pm.route('/action/send-message', methods=['POST'])
def send_message():
    result = {'task_id': None}
    body = request.form.get('body', None, type=str)
    recipient = User.query.filter_by(
        username=request.form.get('recipient', '1', type=str)).first()
    if body and len(body.replace('\r\n', '\n')) <= 65000 \
            and recipient and not recipient.is_blocking(current_user):
        task = send_pm.delay(current_user.username, recipient.username, body)
        result = {'task_id': task.id,
                  'redirect': url_for(
                      'pm.show_conversation', username=recipient.username)}
    return jsonify(result)


@pm.route('/action/request-editing', methods=['POST'])
def request_editing():
    result = {'html': None}
    message = Message.query.get(request.form.get('message', 0, type=int))
    if message and current_user == message.sender:
        if not message.received:
            message.postponed = True
            db.session.add(message)
            db.session.commit()
        result = {'html': render_template('pm/editor.html', message=message)}
    return jsonify(result)


@pm.route('/action/edit-message', methods=['POST'])
def edit_message():
    result = {'task_id': None}
    message = Message.query.get(request.form.get('message', 0, type=int))
    body = request.form.get('body', None, type=str)
    if message and body and len(body.replace('\r\n', '\n')) <= 65000 \
            and current_user == message.sender and message.postponed:
        task = send_edited.delay(message.id, body)
        result = {'task_id': task.id,
                  'redirect': url_for(
                      'pm.show_conversation',
                      username=message.recipient.username)}
    return jsonify(result)


@pm.route('/action/remove-message', methods=['POST'])
def remove_message():
    result = {'empty': True}
    message = Message.query.get(request.form.get('message', 0, type=int))
    if message and \
            (current_user == message.sender or
             current_user == message.recipient):
        redirect = remove_this_message(
            current_user, message,
            request.form.get('page', None, type=int),
            request.form.get('last', 0, type=int))
        result = {'empty': False,
                  'redirect': redirect}
    return jsonify(result)

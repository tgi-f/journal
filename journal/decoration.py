from functools import wraps

from flask import abort
from flask_login import current_user

from .models.auth_units import permissions


def permission_required(permission):
    def decorator(f):
        @wraps(f)
        def wrap_f(*args, **kwargs):
            if not current_user.can(permission):
                abort(403)
            return f(*args, **kwargs)
        return wrap_f
    return decorator


def root_required(f):
    return permission_required(permissions.ADMINISTER_SERVICE)(f)

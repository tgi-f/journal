from io import BytesIO
from random import choice
from os.path import join

from celery.result import AsyncResult
from flask import (
    abort, current_app, flash, jsonify, redirect, render_template,
    request, send_file, send_from_directory, session, url_for)
from flask_login import current_user, login_required

from .. import db, celery
from ..announce.tools import sort_heap_announces
from ..models.auth import User
from ..models.blogs import Item
from ..models.cache import connect
from ..models.captcha import Captcha
from ..models.links import Link
from ..models.lists import List
from ..models.pictures import Picture
from ..tasks.preliminary import format_preliminary
from . import main
from .tools import get_entity


@main.route('/')
def index():
    a_s = sort_heap_announces()
    entity = choice(('list_', 'item'))
    item = get_entity(entity)
    return render_template('index.html', a_s=a_s, entity=entity, item=item)


@main.route('/robots.txt')
def show_robots():
    return send_from_directory(
        current_app.static_folder, request.path[1:], mimetype='text/plain')


@main.route('/society/<username>')
@login_required
def show_profile(username):
    user = User.query.filter_by(username=username).first_or_404()
    kwargs = {'user': user,
              'p_lists': user.lists.filter(List.public.is_(True)).count(),
              'h_lists': user.lists.filter(
                  List.public.is_(True), List.hidden.is_(True)).count(),
              'p_items': user.items.filter(Item.public.is_(True)).count(),
              'h_items': user.items.filter(
                  Item.public.is_(True), Item.hidden.is_(True)).count()}
    return render_template('profile.html', **kwargs)


@main.route('/<spring>')
def jump(spring):
    if len(spring) == 6:
        link = Link.query.filter_by(spring=spring).first_or_404()
        jumps = session.get('jumps', list())
        if link.spring not in jumps and current_user != link.owner:
            link.clicked += 1
            db.session.add(link)
            db.session.commit()
            jumps.append(link.spring)
            session['jumps'] = jumps
        return redirect(link.url)
    elif len(spring) == 7:
        list_ = List.query.filter_by(suffix=spring).first_or_404()
        return redirect(url_for('lists.show_list', slug=list_.slug))
    elif len(spring) == 8:
        item = Item.query.filter_by(suffix=spring).first_or_404()
        return redirect(url_for('blogs.show_item', slug=item.slug))
    return abort(404)


@main.route('/favicon.ico')
def favicon():
    return send_from_directory(
        join(current_app.static_folder, 'images'), request.path[1:],
        mimetype='image/vnd.microsoft.icon')


@main.route('/actions/block', methods=['POST'])
def block():
    result = {'emtpy': True}
    target = User.query.filter_by(
        username=request.form.get('username', None, type=str)).first()
    if target and current_user.is_authenticated:
        if current_user.is_blocking(target):
            current_user.deblock(target)
        else:
            current_user.block(target)
        result = {'empty': False,
                  'is_blocking': current_user.is_blocking(target)}
    return jsonify(result)


@main.route('/actions/check-task', methods=['POST'])
def check_task():
    task = AsyncResult(request.form.get('task_id'), app=celery)
    if task.state == 'SUCCESS':
        flash(task.result)
    elif task.state == 'FAILURE' or task.state == 'RETRY':
        flash('Нештатная ситуация, сообщите администрации сервиса.')
    return task.state


@main.route('/actions/check-item', methods=['POST'])
def check_item():
    task = AsyncResult(request.form.get('task_id'), app=celery)
    entity = request.form.get('entity', 'item', type=str)
    d = {'list_': (List, 'lists.show_lists', 'lists.show_list',
                   'Список успешно создан.'),
         'item': (Item, 'blogs.show_journal', 'blogs.show_item',
                  'Топик успешно создан.'),
         'announcement': (None, 'announce.show_announcements', None,
                          'Объявление успешно создано.')}
    result = {'state': task.state, 'redirect': url_for(d[entity][1])}
    if task.state == 'SUCCESS':
        if entity == 'item' or entity == 'list_':
            target = d[entity][0].query.filter_by(suffix=task.result).first()
            if target:
                result['redirect'] = url_for(d[entity][2], slug=target.slug)
        elif entity == 'announcement':
            result['redirect'] = url_for(d[entity][1])
        flash(d[entity][3])
    elif task.state == 'FAILURE' or task.state == 'RETRY':
        flash('Нештатная ситуация, сообщите администрации сервиса.')
    return jsonify(result)


@main.route('/actions/show-preliminary', methods=['POST'])
def show_preliminary():
    task = format_preliminary.delay(request.form.get('body'))
    result = {'html': render_template('preliminary.html'),
              'task_id': task.id}
    return jsonify(result)


@main.route('/actions/check-preliminary', methods=['POST'])
def check_preliminary():
    task = AsyncResult(request.form.get('task_id'), app=celery)
    result = {'state': task.state}
    if task.state == 'SUCCESS':
        rc = connect(current_app.config.get('CACHE_HOST', 'localhost'))
        cache = rc.get(task.result)
        if cache:
            result = {'state': task.state,
                      'body_html': cache.decode('utf-8')}
            rc.expire(task.result, 15)
    return jsonify(result)


@main.route('/pic/<suffix>')
def show_picture(suffix):
    pic = Picture.query.filter_by(suffix=suffix).first_or_404()
    m_type = 'image/{}'.format(pic.format.lower())
    response = send_file(BytesIO(pic.picture), mimetype=m_type)
    response.content_length = pic.volume
    return response


@main.route('/captcha/<suffix>')
def show_captcha(suffix):
    captcha = Captcha.query.filter_by(suffix=suffix).first_or_404()
    response = send_file(
        BytesIO(captcha.picture), mimetype='image/jpeg', cache_timeout=0)
    response.content_length = len(captcha.picture)
    response.cache_control.no_store = True
    response.cache_control.no_cache = True
    response.cache_control.must_revalidate = True
    response.cache_control.public = False
    return response

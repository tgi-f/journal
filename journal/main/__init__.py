from flask import Blueprint

main = Blueprint('main', __name__)

from . import views, errors
from .tools import format_date
from ..models.auth_units import permissions


@main.app_context_processor
def inject_permissions():
    return {'permissions': permissions, 'format_date': format_date}

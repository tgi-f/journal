from sqlalchemy.sql import func

from ..models.blogs import Item
from ..models.lists import List


def format_date(timestamp):
    return timestamp.isoformat() + 'Z'


def get_entity(entity):
    if entity == 'list_':
        return List.query.filter(
            List.hidden.is_(False),
            List.public.is_(True),
            List.censored.is_(False)).order_by(func.random()).first()
    if entity == 'item':
        return Item.query.filter(
            Item.hidden.is_(False),
            Item.public.is_(True),
            Item.owner_id.isnot(None),
            Item.censored.is_(False)).order_by(func.random()).first()

from flask import render_template
from flask_wtf.csrf import CSRFError

from . import main


@main.app_errorhandler(400)
def notify_bad_request(e):
    return render_template(
        'error.html', error=400, reason='Неверный запрос.'), 400


@main.app_errorhandler(403)
def refuse_request(e):
    return render_template(
        'error.html', error=403, reason='Доступ запрещён.'), 403


@main.app_errorhandler(404)
def notify_not_found_page(e):
    return render_template(
        'error.html', error=404, reason='Страница не найдена.'), 404


@main.app_errorhandler(405)
def refuse_method(e):
    return render_template(
        'error.html', error=405, reason='Метод не позволен.'), 405


@main.app_errorhandler(500)
def notify_server_error(e):
    return render_template(
        'error.html', error=500, reason='Внутренняя ошибка сервера.'), 500


@main.app_errorhandler(CSRFError)
def handle_csrf_error(e):
    return render_template(
        'error.html', error='CSRF Error', reason=e.description), 400


@main.app_errorhandler(413)
def refuse_large_request(e):
    return render_template(
        'error.html', error=413, reason='Запрос чрезмерен.'), 413

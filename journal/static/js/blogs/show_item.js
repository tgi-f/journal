$(document).ready(function() {
  moment = customizeMoment(moment);
  formatFooter(moment);
  var $panel = $('#left-panel');
  var uploading = $panel.data().upload;
  var checker = $panel.data().check;
  var token = $panel.data().tee;
  var suffix = $panel.data().suffix;
  var length = $panel.data().length;
  var page = $panel.data().page;

  countClicks($panel.data().count, suffix);

  $('.commentary-body').each(function() {
    $(this).addClass('entity-body-block');
  });
  $('.date-field').each(function() {formatDateTime($(this), moment);});
  $('.c-date-field').each(function() {formatDateTime($(this), moment);});
  $('.slidable .block-header').on('click', showHideBlock);

  $('.entity-body-block img').each(adjustBlogimage);
  $('.entity-body-block iframe').each(adjustFrame);
  var $twits = $('.entity-body-block').find('.twitter-tweet');
  if ($twits.length) {
    adjustTwits($twits);
  }
  $('.commentary-body').each(function() {
    $(this).removeClass('entity-body-block');
  });

  $('.options').on('click', '.update-button', updateEntity);

  $('.options').on('click', '.labels-button', function() {
    $(this).blur();
    window.location.assign($(this).data().url);
  });

  $('.options').on('click', '.edit-title', function() {
    var $body = $('.text-editor-block');
    var $bond = $('.bond-block');
    if (!$body.is(':hidden')) { $('.edit-text').trigger('click');}
    if (!$bond.is(':hidden')) { $bond.slideUp('slow');}
    var $target = $('.title-editor-block');
    if ($target.is(':hidden')) {
      $target.slideDown('slow', function() {scrollPanel($target);});
    } else {
      $target.slideUp('slow', function() {
        scrollPanel($('#left-panel'));
      });
    }
    $(this).blur();
  });

  $('.options').on('click', '.edit-text', function() {
    var $target = $('.text-editor-block');
    var $body = $('.entity-body-block');
    var $title = $('.title-editor-block');
    var $bond = $('.bond-block');
    if (!$title.is(':hidden')) {$title.slideUp('slow');}
    if (!$bond.is(':hidden')) {$bond.slideUp('slow');}
    if ($target.is(':hidden')) {
      $body.slideUp('slow', function() {
        $target.slideDown('slow', function() {scrollPanel($target);});
      });
    } else {
      $target.slideUp('slow', function() {
        $body.slideDown('slow', function() {
          scrollPanel($('.entity-body-block'));
        });
      });
    }
    $(this).blur();
  });

  $('.options').on(
  'click', '.bond-button', {suffix: suffix, token: token},
  function(event) {
    var $target = $('.bond-block');
    var $body = $('.text-editor-block');
    var $title = $('.title-editor-block');
    if (!$body.is(':hidden')) { $('.edit-text').trigger('click');}
    if (!$title.is(':hidden')) { $title.slideUp('slow');}
    if ($target.is(':hidden')) {
      if (!$target.find('.bond-block-content').length) {
        $.post(
          $(this).data().url,
          {
            suffix: event.data.suffix,
            csrf_token: event.data.token,
          },
          function(data) {
            if (!data.empty) {
              $target.append(data.content);
              $target.slideDown('slow', function() {scrollPanel($target);});
            }
          },
          'json');
      } else {
        $target.slideDown('slow', function() {scrollPanel($target);});
      }
    } else {
      $target.slideUp('slow', function() {
        scrollPanel($('.entity-body-block'));
      });
    }
    $(this).blur();
  });

  $('#text-submit').on(
  'click',
  {
    token: token, suffix: suffix,
    uploading: uploading, checker: checker
  },
  function(event) {
    event.preventDefault();
    var $text = $('#text-editor').val();
    $('#text-editor').trigger('blur');
    if (!$('#text-editor').parents('.form-group').hasClass('has-error')) {
      $.post(
        $(this).data().url,
        {
          text: $text,
          suffix: event.data.suffix,
          csrf_token: event.data.token,
        },
        function(data) {
          if (!data.empty) {
            if (!data.task_id) {
              window.location.replace(data.redirect)
            } else {
              showProgress('.text-editor-block', event.data.uploading);
              checkTask(
                800, event.data.checker, event.data.token,
                data.task_id, data.redirect);
            }
          }
        },
        'json');
    }
    $(this).blur();
  });

  $('#title-submit').on(
  'click', {token: token, suffix: suffix},
  function(event) {
    event.preventDefault();
    var $abstract = $('#abstract').val();
    $('#abstract').trigger('blur');
    if (!$('.title-editor-block').hasClass('has-error')) {
      $.post(
        $(this).data().url,
        {
          abstract: $abstract,
          suffix: event.data.suffix,
          csrf_token: event.data.token,
        },
        function(data) {
          if (!data.empty) {
            window.location.replace(data.redirect);
          }
        },
        'json');
    }
    $(this).blur();
  });

  $('#text-editor').on('keyup blur', markError);

  $('#abstract').on(
  'keyup', {max: 100, block: '.title-editor-block'}, markInputError);

  $('#abstract').on(
  'blur', {max: 100, block: '.title-editor-block'}, markInputError);

  $('.options').on('click', '.trash-button', clickTrash);

  $('.options').on(
  'click', '.remove-button',
  {
    token: token, suffix: suffix,
    uploading: uploading, checker: checker
  }, function(event) {
    $(this).blur();
    $.post(
      $(this).data().url,
      {
        suffix: event.data.suffix,
        csrf_token: event.data.token,
      },
      function(data) {
        if (data.task_id) {
          showProgress('.entity-body-block', event.data.uploading);
          $('.options').hide();
          checkTask(
            800, event.data.checker, event.data.token,
            data.task_id, data.redirect);
        }
      },
      'json');
  });

  $('.options').on(
  'click', '.tape-out',
  {token: token, suffix: suffix, entity: 'item'},
  changeReplace);

  $('.options').on(
  'click', '.tape-in',
  {token: token, suffix: suffix, entity: 'item'}, changeReplace);

  $('.options').on(
  'click', '.heap-button',
  {token: token, suffix: suffix, entity: 'item'}, changeTarget);

  $('.options').on(
    'click', '.public-button',
    {moment: moment, token: token, suffix: suffix, entity: 'item'},
    changeTarget);

  $('.options').on(
  'click', '.censor-this',
   {token: token, suffix: suffix, entity: 'item'}, changeReplace);

  $('.options').on(
  'click', '.commentary-state-button',
  {token: token, suffix: suffix,
   entity: 'item', length: length}, changeTarget);

  $('.options').on(
  'click', '.like-button',
  {token: token, suffix: suffix, entity: 'item'}, changeTarget);

  $('.options').on(
  'click', '.dislike-button',
  {token: token, suffix: suffix, entity: 'item'}, changeTarget);

  $('#sub-content').on(
  'click', '.new-comment-button',
  {token: token, suffix: suffix, entity: 'item'}, clickComment);

  $('#sub-content').on('keyup blur', '#comment-editor', markError);

  $('#sub-content').on(
  'click', '#comment-submit',
  {token: token,
   uploading: uploading, checker: checker}, sendComment);

  $('.show-this-comment').on('click', showCommentary);

  $('#sub-content').on('click', '.slide-all', slideAllCommentaries);

  $('.answer-button').on(
  'click',
  {token: token, suffix: suffix, entity: 'item', page: page},
  clickAnswer);

  $('.remove-comment').on(
  'click', {token: token, page: page}, removeCommentary);

  $('.blocking-button').on(
  'click',
  {token: token, suffix: suffix, page: page}, banCommentator);

  $('.pushprelim')
  .on( 'click', {token: token, target: '#text-editor'}, showPrelim);

  $('.new-comment-block')
  .on('click', '.pushprelim',
      {token: token, target: '#comment-editor'}, showPrelim);

  $('.answer-comment-block')
  .on('click', '.pushprelim',
      {token: token, target: '#comment-editor'}, showPrelim);

  $('#preliminary').on('click', '.closeprelim', function() {
    $('#preliminary').empty();
  });
});

$(document).ready(function() {
  moment = customizeMoment(moment);
  formatFooter(moment);
  var $today = $('.today');
  if ($today.length) {
    formatDateTime($today, moment);
    setInterval(
      function() {
        $today
        .text(moment().format('LL') + ', ' + moment().format('HH:mm:ss'));
      }, 1000);
  }
  if ($('.pushprelim').length) {
    var token = $('.pushprelim').data().tee;
  }

  $('.date-field').each(function() {
    formatDateTime($(this), moment);
  });

  $('.announcement-block .block-header').on('click', showHideAnnounce);
  $('.announcement-body iframe').each(adjustFrame);
  var $twits = $('.announcement-body').find('.twitter-tweet');
  if ($twits.length) { adjustTwits($twits); }

  var $form = $('.to-be-hidden');
  if ($form.find('.error').length) {
    $form.slideDown('slow');
  }

  $('#public').on('change', function() {
    if (!$(this).is(':checked')) {
      if ($('#hidden').is(':checked')) {
        $('#hidden').trigger('click');
      }
    }
  });

  $('#hidden').on('change', function() {
    if ($(this).is(':checked')) {
      if (!$('#public').is(':checked')) {
        $('#public').trigger('click');
      }
    }
  });

  $('.entity-link').on('click', linkPropagation);

  $('#abstract').on('keyup', {max: 100, block: '.form-group'}, markInputError);

  $('#body').on('keyup', markError);

  $('.slidable .block-header').on('click', showHideBlock);

  $('.pushprelim').on(
    'click', {token: token, target: '#body'}, showPrelim);

  $('#preliminary').on('click', '.closeprelim', function() {
    $('#preliminary').empty();
  });
});

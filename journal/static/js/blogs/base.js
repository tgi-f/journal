/*
    lists/show_list.html; js/lists/show_list.js
    blogs/show_item.html; js/blogs/show_item.js
*/


function updateInfoline(infoline, moment) {
  $('.info-line').empty().append(infoline);
  $('.date-field').each(function() {formatDateTime($(this), moment);});
}

function changeReplace(event) {
  $(this).blur();
  $.post(
    $(this).data().url,
    {
      suffix: event.data.suffix,
      entity: event.data.entity,
      csrf_token: event.data.token,
    },
    function(data) {
      if (!data.empty) {
        window.location.replace(data.redirect);
      }
    },
    'json');
}

function changeTarget(event) {
  $(this).blur();
  $.post(
    $(this).data().url,
    {
      suffix: event.data.suffix,
      entity: event.data.entity,
      length: event.data.length,
      csrf_token: event.data.token,
    },
    function(data) {
      if (!data.empty) {
        if (data.details) {
          $('.common-details').empty().append(data.details);
        }
        if (data.options) {
          $('.options').empty().append(data.options);
        }
        if (data.infoline) {
          updateInfoline(data.infoline, event.data.moment);
        }
        if (data.c_options) {
          $('.comments-options').remove();
          $('.options').after(data.c_options);
        }
      }
    },
    'json');
}

function updateEntity() {
  $(this).blur();
  window.location.assign($(this).attr('data-update'));
}

function clickTrash() {
  var $target = $(this).siblings('.remove-button');
  if ($target.is(':hidden')) {
    $target.fadeIn('slow');
  } else {
    $target.fadeOut('slow');
  }
  $(this).blur();
}

function hideForm(block) {
  if (block.is(':hidden')) {
    block.empty();
  } else {
    block.slideUp('slow', function() {block.empty();});
  }
}

function clickComment(event) {
  var $form = $('.new-comment-block');
  $('.answer-comment-block').each(function() {hideForm($(this));});
  if ($form.is(':hidden')) {
    $.post(
      $(this).data().url,
      {
        suffix: event.data.suffix,
        entity: event.data.entity,
        csrf_token: event.data.token,
      },
      function(data) {
        $form.append(data.data);
        $form.slideDown('slow', function() {scrollPanel($form);});
        $('#comment-editor').focus();
      },
      'json');
  } else {
    $form.slideUp('slow', function() {$form.empty();});
  }
  $(this).blur();
}

function clickAnswer(event) {
  hideForm($('.new-comment-block'));
  $('.answer-comment-block').each(function() {hideForm($(this));});
  var $answer = $(this).parents('.commentary-head')
                       .siblings('.answer-comment-block');
  var $parent = $(this).attr('data-parent');
  if ($answer.is(':hidden')) {
    $.post(
      $(this).data().url,
      {
        suffix: event.data.suffix,
        entity: event.data.entity,
        parent: $parent,
        page: event.data.page,
        csrf_token: event.data.token,
      },
      function(data) {
        $answer.append(data.data);
        $answer.slideDown('slow', function() {
          scrollPanel($answer);
        });
        $('#comment-editor').focus();
      }, 'json');
  } else {
    $answer.slideUp('slow', function() {$answer.empty();});
  }
  $(this).blur();
}

function sendComment(event) {
  event.preventDefault();
  var $body = $('#comment-editor').val();
  $('#comment-editor').trigger('blur');
  var $block = $(this).parents('form').parent();
  if ($block.hasClass('new-comment-block')) {
    var $cls = '.new-comment-block';
  } else if ($block.hasClass('answer-comment-block')) {
    var $cls = '.answer-comment-block';
  }
  if (!$('#comment-editor').parents('.form-group').hasClass('has-error')) {
    $.post(
      $(this).data().url,
      {
        body: $body,
        suffix: $(this).data().suffix,
        parent: $(this).data().p || null,
        page: $(this).data().page,
        csrf_token: event.data.token,
      },
      function(data) {
        if (data.task_id) {
          showProgress($cls, event.data.uploading);
          checkTask(
            800, event.data.checker, event.data.token,
            data.task_id, data.redirect);
        }
      },
      'json');
  }
  $(this).blur();
}

function showCommentary() {
  var $icon = $(this).find('.glyphicon');
  var $opts = $(this).siblings('.optional-buttons');
  var $body = $(this).parents('.commentary-head')
                     .siblings('.commentary-body');
  if ($body.is(':hidden')) {
    $body.slideDown('slow', function() {
      $icon.removeClass('glyphicon-chevron-down')
           .addClass('glyphicon-chevron-up');
      $opts.fadeIn('slow');
      $body.find('img').each(adjustBlogimage);
    });
  } else {
    $body.slideUp('slow', function() {
      $icon.removeClass('glyphicon-chevron-up')
           .addClass('glyphicon-chevron-down');
      $opts.fadeOut('slow');
    });
  }
  $(this).blur();
}

function slideAllCommentaries() {
  var $icon = $(this).find('.glyphicon');
  if ($icon.hasClass('glyphicon-chevron-down')) {
    $icon.removeClass('glyphicon-chevron-down')
         .addClass('glyphicon-chevron-up');
    $(this).attr("title", "свернуть все");
    $('.show-this-comment').each(function() {
      var $body = $(this).parents('.commentary-head')
                         .siblings('.commentary-body');
      if ($body.is(':hidden')) {$(this).trigger('click');}
    });
  } else if ($icon.hasClass('glyphicon-chevron-up')) {
    $icon.removeClass('glyphicon-chevron-up')
         .addClass('glyphicon-chevron-down');
    $(this).attr("title", "раскрыть все");
    $('.show-this-comment').each(function() {
      var $body = $(this).parents('.commentary-head')
                         .siblings('.commentary-body');
      if (!$body.is(':hidden')) {$(this).trigger('click');}
    });
  }
  $(this).blur();
}

function removeCommentary(event) {
  $.post(
    $(this).data().url,
    {
      last: $(this).data().last,
      commentary: $(this).data().c,
      page: event.data.page,
      csrf_token: event.data.token,
    },
    function(data) {
      if (!data.empty) {
        window.location.replace(data.redirect);
      }
    },
    'json');
  $(this).blur();
}

function banCommentator(event) {
  $.post(
    $(this).data().url,
    {
      user: $(this).data().author,
      suffix: event.data.suffix,
      page: event.data.page,
      csrf_token: event.data.token,
    },
    function(data) {
      if (!data.empty) {window.location.replace(data.redirect);}
    }, 'json');
}

function formatFooter(moment) {
  var $footer = $.trim($('#footer-link').text());
  $('#footer-link').text($footer + ', ' + moment().format('YYYY') + ' г.');
}

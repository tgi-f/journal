/*
    lists/show_list.html; js/lists/show_list.js
    lists/edit_item.html; js/lists/edit_item.js
    lists/current_lists.html; js/lists/current_lists.js
    blogs/show_item.html; js/blogs/show_item.js
    blogs/current_blogs.html; js/blogs/current_blogs.js
    announce/editor.html; js/announce/editor.js
    announce/announcement.html; js/announce/announcement.js
*/


function markInputError(event) {
  var $value = $(this).val();
  var $block = $(this).parents(event.data.block);
  if ($value.length == 0 || $value.length > event.data.max) {
    $block.addClass('has-error');
  } else {
    if ($block.hasClass('has-error')) {
      $block.removeClass('has-error');
    }
  }
}

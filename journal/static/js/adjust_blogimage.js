/*
    index.html; js/main/index.js
    pm/conversation.html; js/pm/conversation.js
    lists/show_list.html; js/lists/show_list.js
    lists/edit_item.html; js/lists/edit_item.js
    lists/current_lists.html; js/lists/current_lists.js
    labels/edit.html; js/labels/edit.js
    blogs/show_item.html; js/blogs/show_item.js
    blogs/current_blogs.html; js/blogs/current_blogs.js
    announce/announcement.html; js/announce/announcement.js
*/

function adjust(image) {
  var width = image.width();
  var $parent = image.parents('p');
  $parent.css({"text-align": "center"});
  var $width = parseInt($parent.outerWidth());
  if (width > $width) {
    image.attr('width', $width);
  }
}

function adjustBlogimage() {
  adjust($(this));
  $(this).on('load', function() {
    adjust($(this));
  });
}

$(document).ready(function() {
  moment = customizeMoment(moment);
  formatFooter(moment);
  formatDateTime($('.today'), moment);
  setInterval(
    function() {
      $('.today').text(
        moment().format('LL') + ', ' +
        moment().format('HH:mm:ss'));
    },
    1000);
  $('.date-field').each(function() {
    var $date = $.trim($(this).text());
    $(this).text(moment($date).format('DD.MM.YYYY') + ' г.');
  });
  var token = $('#aliases').data().tee;
  $('.remove-alias')
  .on('click', {token: token}, function(event) {
    var $this = $(this);
    $this.blur();
    $.post(
      $this.data().url,
      {
        last: $this.data().last,
        page: $this.data().page,
        suffix: $this.data().suffix,
        csrf_token: event.data.token,
      },
      function(data) {
        if (!data.empty) {
          window.location.replace(data.redirect);
        }
      }, 'json');
  });

  $('.trash-button').on('click', function() {
    var $target = $(this).siblings('.remove-alias');
    if ($target.is(':hidden')) {
      $('.remove-alias').each(function() {$(this).fadeOut('slow');});
      $target.fadeIn('slow');
    } else {
      $target.fadeOut('slow');
    }
    $(this).blur();
  });
});

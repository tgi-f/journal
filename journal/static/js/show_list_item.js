/*
    index.html; js/main/index.js
    lists/show_list.html; js/lists/show_list.js
    lists/edit_item.html; js/lists/edit_item.js
    labels/edit.html; js/labels/edit.js
*/

function showListItem() {
  var $body = $(this).siblings('.list-item-body');
  var $del = $(this).siblings('.list-item-body').find('.delete-item');
  if ($body.is(':hidden')) {
    $body.slideDown('slow', function() {
      $body.find('img').each(adjustBlogimage);
    });
    scrollPanel($(this));
  } else {
    $body.slideUp('slow', function() {
      if ($del.length) $del.hide();
    });
  }
}

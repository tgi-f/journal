function showMessageForm() {
  var $block = $('.new-private-message');
  if ($block.length) {
    if ($block.is(':hidden')) {
      $block.slideDown('slow', function() {scrollPanel($block);});
    } else {
      $block.slideUp('slow');
    }
  }
  $(this).blur();
}

$(document).ready(function() {
  moment = customizeMoment(moment);
  formatFooter(moment);
  formatDateTime($('.today'), moment);
  setInterval(
    function() {
      $('.today')
      .text(moment().format('LL') + ', ' + moment().format('HH:mm:ss'));
    }, 1000);
  var $panel = $('#c-panel');
  var token = $panel.data().tee;
  var uploading = $panel.data().upload;
  var checker = $panel.data().check;
  $('.date-field').each(function() { formatDateTime($(this), moment); });
  $('.item-body iframe').each(adjustFrame);
  var $twits = $('.item-body').find('.twitter-tweet');
  if ($twits.length) { adjustTwits($twits); }
  if ($('.last-message').length) { scrollPanel($('.last-message')); }

  $('.show-this').on('click', function() {
    var $icon = $(this).find('.glyphicon');
    var $opts = $(this).siblings('.optional-buttons');
    var $body = $(this).parents('.private-message-options')
                       .siblings('.item-body');
    if ($body.is(':hidden')) {
      $body.slideDown('slow', function() {
        $icon.removeClass('glyphicon-chevron-down')
             .addClass('glyphicon-chevron-up');
        $opts.fadeIn('slow');
        $body.find('img').each(adjustBlogimage);
      });
    } else {
      $body.slideUp('slow', function() {
        $icon.removeClass('glyphicon-chevron-up')
             .addClass('glyphicon-chevron-down');
        $opts.fadeOut('slow');
      });
    }
    $(this).blur();
  });

  $('.slide-down-all').on('click', function() {
    var $icon = $(this).find('.glyphicon');
    if ($icon.hasClass('glyphicon-chevron-down')) {
      $icon.removeClass('glyphicon-chevron-down')
           .addClass('glyphicon-chevron-up');
      $(this).attr("title", "свернуть все");
      $('.show-this').each(function() {
        var $body = $(this).parents('.private-message-options')
                           .siblings('.item-body');
        if ($body.is(':hidden')) {$(this).trigger('click');}
      });
    } else if ($icon.hasClass('glyphicon-chevron-up')) {
      $icon.removeClass('glyphicon-chevron-up')
           .addClass('glyphicon-chevron-down');
      $(this).attr("title", "раскрыть все");
      $('.show-this').each(function() {
        var $body = $(this).parents('.private-message-options')
                           .siblings('.item-body');
        if (!$body.is(':hidden')) {$(this).trigger('click');}
      });
    }
    $(this).blur();
  });

  $('.trash-button').on('click', function() {
    var $remove = $(this).siblings('.remove-button');
    if ($remove.is(':hidden')) {
      $remove.fadeIn('slow');
    } else {
      $remove.fadeOut('slow');
    }
    $(this).blur();
  });

  $('.answer-button').on('click',showMessageForm);

  $('.new-message-button').on('click', showMessageForm);

  $('#private-message-editor').on('keyup blur', markError);
  $('.private-message').on('keyup blur', '#being-edited', markError);

  $('#private-message-submit').on(
  'click',
  {token: token, uploading: uploading, checker: checker,},
  function(event) {
    event.preventDefault();
    var $body = $('#private-message-editor').val();
    $('#private-message-editor').trigger('blur');
    var $block = $('#private-message-editor').parents('.form-group');
    if (!$block.hasClass('has-error')) {
      $.post(
        $(this).data().url,
        {
          body: $body,
          recipient: $(this).data().recipient,
          csrf_token: event.data.token,
        },
        function(data) {
          if (data.task_id) {
            showProgress('.form-form', event.data.uploading);
            checkTask(
              800, event.data.checker, event.data.token,
              data.task_id, data.redirect);
          }
        },
        'json');
    }
    $(this).blur();
  });

  $('.private-message').on('click', '.cancel-editing', function() {
    $(this).blur();
    var $target = $(this).attr('data-u');
    window.location.replace($target);
  });

  $('.edit-button').on('click', {token: token}, function(event) {
    $(this).blur();
    var $body = $(this).parents('.private-message-options')
                       .siblings('.item-body');
    var $head = $(this).parents('.private-message-options')
                       .siblings('.private-message-head');
    var $this = $(this).parents('.private-message');
    var $opts = $(this).parents('.private-message-options');
    var $message = $(this).attr('data-m');
    $.post(
      $(this).data().url,
      {
        message: $message,
        csrf_token: event.data.token,
      },
      function(data) {
        if (data.html) {
          $body.slideUp('slow', function() {
            $opts.fadeOut();
            $body.remove();
            $head.after(data.html);
            $('.editor-body').slideDown('slow', function() {
              scrollPanel($this);
            });
          });
        }
      },
      'json');
  });

  $('.private-message').on(
  'click', '#edit-pm',
  {token: token, uploading: uploading, checker: checker},
  function(event) {
    event.preventDefault();
    $(this).blur();
    var $message = $(this).attr('data-m');
    var $body = $('#being-edited').val();
    $('#being-edited').trigger('blur');
    var $block = $('#being-edited').parents('.form-group');
    if (!$block.hasClass('has-error')) {
      $.post(
        $(this).data().url,
        {
          body: $body,
          message: $message,
          csrf_token: event.data.token,
        },
        function(data) {
          if (data.task_id) {
            showProgress('.form-form', event.data.uploading);
            checkTask(
              800, event.data.checker, event.data.token,
              data.task_id, data.redirect);
          }
        }, 'json');
    }
    $(this).blur();
  });

  $('.remove-button').on(
  'click', {token: token}, function(event) {
    var $last = $(this).attr('data-l');
    var $message = $(this).attr('data-m');
    $.post(
      $(this).data().url,
      {
        message: $message,
        last: $last,
        page: $(this).data().page,
        csrf_token: event.data.token,
      },
      function(data) {
        if(!data.empty) { window.location.replace(data.redirect);}
      },
      'json');
    $(this).blur();
  });

  $('.pushprelim').on(
    'click', {token: token, target: '#private-message-editor'}, showPrelim);

  $('.private-message').on(
    'click', '.pushprelim',
    {token: token, target: '#being-edited'}, showPrelim);

  $('#preliminary').on('click', '.closeprelim', function() {
    $('#preliminary').empty();
  });
});

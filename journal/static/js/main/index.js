$(document).ready(function() {
  moment = customizeMoment(moment);
  formatFooter(moment);
  $('.date-field').each(function() {formatDateTime($(this), moment);});
  $('.slidable .block-header').on('click', showHideBlock);
  $('.entity-link').on('click', linkPropagation);

  $('.announcement-block .block-header').on('click', showHideAnnounce);
  $('.announcement-body iframe').each(adjustFrame);

  var $panel = $('#e-panel');
  if ($panel.length) {
    var entity = $panel.data().entity;
    if (entity == 'item') {
      var $twits = $('.entity-body-block').find('.twitter-tweet');
      if ($twits.length) { adjustTwits($twits);}
      $('.announcement-body').each(function() {
        $(this).removeClass('entity-body-block');
      });
      $('.entity-body-block img').each(adjustBlogimage);
      $('.entity-body-block iframe').each(adjustFrame);
    } else if (entity == 'list_') {
      var $twits = $('.list-item-body').find('.twitter-tweet');
      if ($twits.length) {adjustTwits($twits);}
      $('.announcement-body').each(function() {
        $(this).removeClass('list-item-body');
      });
      $('.list-item-body iframe').each(adjustFrame);
      $('#items-block').on(
      'click', '.list-item-block .block-header', showListItem);
      $('#items-block').on('click', '.slide-button', slideListItem);
    }
  }
});

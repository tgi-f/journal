$(document).ready(function() {
  moment = customizeMoment(moment);
  formatFooter(moment);
  formatDateTime($('#registered'), moment);
  var $last_seen = $.trim($('#last-seen').text());
  $('#last-seen').text(moment($last_seen).fromNow());

  $('.slidable .block-header').on('click', function() {
    var $body = $(this).siblings('.block-body');
    if ($body.is(':hidden')) {
      $body.slideDown('slow');
    } else {
      $body.slideUp('slow');
    }
  });

  $('.group-block .block-header')
  .on('click', function() {
    var $body = $(this).siblings('.block-body');
    if ($body.length) {
      if ($body.is(':hidden')) {
        $body.slideDown('slow');
      } else {
        $body.slideUp('slow');
      }
    } else {
      var $username = $(this).attr('data-u');
      var $this = $(this);
      $.post(
        $(this).data().change,
        {
          username: $username,
          csrf_token: $(this).data().tee,
        },
        function(data) {
          $(data).insertAfter($this);
          $this.siblings('.block-body').slideDown('slow');
        },
        'html');
    }
  });

  $('.blocking-button')
  .on('click', function() {
    var $this = $(this);
    var $username = $this.attr('data-u');
    $.post(
      $(this).data().url,
      {
        username: $username,
        csrf_token: $(this).data().tee,
      },
      function(data) {
        if (!data.empty) {
          if (data.is_blocking) {
            $this.removeClass('btn-success').addClass('btn-warning');
            $this.text('простить');
          } else {
            $this.removeClass('btn-warning').addClass('btn-success');
            $this.text('заблокировать');
          }
        }
      },
      'json');
    $this.blur();
  });

  $('.content-block')
  .on('click', '#submit',  function(event) {
    event.preventDefault();
    var $group = $(this).parents('form').serializeArray()[0].value;
    var $name = $('#target').val();
    $.post(
      $(this).data().url,
      {
        group: $group,
        name: $name,
        csrf_token: $(this).data().tee,
      },
      function(data) {
        window.location.replace(data.redirect);
      },
      'json');
  });
});

function checkPrelim(timeout, url, csrf, task_id) {
  setTimeout(
    function(url, csrf, task_id) {
      var timer = setInterval(
        function(u, cs, t) {
          $.post(
            u,
            {csrf_token: cs, task_id: t},
            function(data) {
              if (data.state == 'SUCCESS' && data.body_html) {
                clearInterval(timer);
                $('.modal-body').empty().append(data.body_html);
                $('.modal-body img').each(adjustBlogimage);
                $('.modal-body iframe').each(adjustFrame);
                var $twits = $('.modal-body').find('.twitter-tweet');
                if ($twits.length) adjustTwits($twits);
              }
            },
            'json');
        },
        timeout, url, csrf, task_id);
    },
    timeout, url, csrf, task_id);
}

function showPrelim(event) {
  $(this).blur();
  $body = $(event.data.target).val();
  $block = $(event.data.target).parents('.form-group');
  var check = $(this).data().check;
  if ($body.trim() && !$block.hasClass('has-error') && $body.length <= 65000) {
    $.post(
      $(this).data().url,
      {
        csrf_token: event.data.token,
        body: $body,
      },
      function(data) {
        $('#preliminary').empty().append(data.html);
        $('#toggleprelim').trigger('click');
        checkPrelim(800, check, event.data.token, data.task_id);
      },
      'json');
  }
}

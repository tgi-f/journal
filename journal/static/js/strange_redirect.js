$(document).ready(function() {
  moment = customizeMoment(moment);
  formatFooter(moment);
  var $panel = $('#data-panel');
  var timeout = $panel.data().timeout;
  setTimeout(
    function(url, csrf, id, ent) {
      var timer = setInterval(
        function(u, c, i, e) {
          $.post(
            u,
            {csrf_token:c, task_id:i, entity:e},
            function(data) {
              if (data.state=='SUCCESS' ||
                  data.state=='RETRY' || data.state=='FAILURE') {
                clearInterval(timer);
                var $des = data.redirect;
                window.location.replace($des);
              }
            }, 'json');
        },
        timeout, url, csrf, id, ent);
    },
    timeout, $panel.data().url,
    $panel.data().tee, $panel.data().task, $panel.data().entity);
});

/*
    index.html; js/main/index.js
    pm/conversation.html; js/pm/conversation.js
    lists/show_list.html; js/lists/show_list.js
    lists/edit_item.html; js/lists/edit_item.js
    lists/current_lists.html; js/lists/current_lists.js
    labels/edit.html; js/labels/edit.js
    blogs/show_item.html; js/blogs/show_item.js
    blogs/current_blogs.html; js/blogs/current_blogs.js
    announce/announcement.html; js/announce/announcement.js
*/

function adjustFrame() {
  $(this).wrap('<div class="embedded-container"></div>');
  $(this).addClass('video');
}

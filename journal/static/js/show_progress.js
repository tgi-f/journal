/*
    pm/conversation.html; js/pm/conversation.js
    lists/show_list.html; js/lists/show_list.js
    blogs/show_item.html; js/blogs/show_item.js
    pictures/album.html; js/pictures/album.js
    pictures/albums.html; js/pictures/albums.js
    admin/pictures.html; js/admin/pictures.js
    announce/announcement.html; js/announce/announcement.js
*/

function showProgress(cls, uploading) {
  var html = '<div class="upload-progress-block text-center">' +
             '<img alt="uploading" width="32" height="32" ' +
             'src="' + uploading + '"></div>';
  $(cls).empty().append(html);
  var block = '<div class="flashed-message another">' +
         '<div class="alert alert-warning">' +
      '<button class="close" type="button" data-dismiss="alert">' +
      '&times;</button>' +
      'Действие выполняется, ожидайте...</div></div>';
  $('#left-panel').before(block);
}

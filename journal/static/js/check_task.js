/*
    pm/conversation.html; js/pm/conversation.js
    lists/show_list.html; js/lists/show_list.js
    blogs/show_item.html; js/blogs/show_item.js
    announce/announcements.html; js/announce/announcements.js
    admin/pictures.html; js/admin/pictures.js
    pictures/albums.html; js/pictures/albums.js
    pictures/album.html; js/pictures/album.js
*/

function checkTask(timeout, url, csrf, id, des) {
  setTimeout(
    function(url, csrf, id, des) {
      var timer = setInterval(
        function(u, c, i, d) {
          $.post(
            u,
            {csrf_token: c, task_id:i},
            function(data) {
              if (data=='SUCCESS' || data=='FAILURE' || data=='RETRY') {
                clearInterval(timer);
                window.location.replace(d);
              }
            },
            'text');
        },
        timeout, url, csrf, id, des);
    },
    timeout, url, csrf, id, des);
}

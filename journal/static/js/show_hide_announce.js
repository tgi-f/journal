/*
    index.html; js/main/index.js;
    lists/current_lists.html; js/lists/current_lists.js
    blogs/current_blogs.html; js/blogs/current_blogs.js
*/

function showHideAnnounce() {
  var $body = $(this).siblings('.block-body');
  if ($body.is(':hidden')) {
    $body.slideDown('slow', function() {
      $('.announcement-body img').each(adjustBlogimage);
    });
    scrollPanel($(this));
  } else {
    $body.slideUp('slow');
  }
}

/*
    announce/announcement.html; js/announce/announcement.js
    admin/pictures.html; js/admin/pictures.js
    pictures/albums.html; js/pictures/albums.js
    pictures/album.html; js/pictures/album.js
*/

function removeTarget(event) {
  $(this).blur();
  var $this = $(this);
  var $last = $this.data().last;
  var $page = $this.data().page;
  var $suffix = $this.data().suffix;
  var $admin = $this.data().admin;
  var $check = $this.data().check;
  var $upload = $this.data().upload;
  $.post(
      $this.data().url,
      {
        csrf_token: event.data.token,
        suffix: $suffix,
        page: $page,
        admin: $admin,
        last: $last,
      },
      function(data) {
        if (!data.empty) {
          if (event.data.view == 'album'){
            showProgress('.items-table-block', $upload);
          } else if (event.data.view == 'admin-pics') {
            $this.parents('.picture-info')
                 .siblings('.picture-itself')
                 .addClass('indicate-progress');
            showProgress('.indicate-progress', $upload);
          } else if (event.data.view == 'announce') {
            $this.parents('.block-body').addClass('indicate-progress');
            showProgress('.indicate-progress', $upload);
          }
          if ($admin && $page == 1) {
            $('.another').addClass('next-block');
          }
          checkTask(
            800, $check, event.data.token,
            data.task_id, data.redirect);
        }
      }, 'json');
}

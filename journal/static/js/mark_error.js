/*
    pm/conversation.html; js/pm/conversation.js
    lists/show_list.html; js/lists/show_list.js
    lists/edit_item.html; js/lists/edit_item.js
    blogs/show_item.html; js/blogs/show_item.js
    blogs/current_blogs.html; js/blogs/current_blogs.js
*/

function markError() {
  var $value = $(this).val();
  var $group = $(this).parents('.form-group');
  if ($value == 0 || $value.length > 65000) {
    $group.addClass('has-error');
  } else {
    if ($group.hasClass('has-error')) {
      $group.removeClass('has-error');
    }
  }
}

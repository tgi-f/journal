/*
    index.html; js/main/index.js
    lists/show_list.html; js/lists/show_list.js
    lists/edit_item.html; js/lists/edit_item.js
*/

function slideListItem() {
  $(this).blur();
  var $body = $(this).parents('.list-item-body');
  var $scrolled = $body.siblings('.block-header');
  var $del = $(this).parents('.list-item-body').find('.delete-item');
  $body.slideUp('slow', function() {
    scrollPanel($scrolled);
    if ($del.length) $del.hide();
  });
}

$(document).ready(function() {
  moment = customizeMoment(moment);
  formatFooter(moment);
  formatDateTime($('.today'), moment);
  setInterval(
    function() {
      $('.today').text(
        moment().format('LL') + ', ' +
        moment().format('HH:mm:ss'));
    },
    1000);
  $('.reload-button').on('click', function() {
    $(this).blur();
    window.location.reload();
  });

  var $deni = $('.denial-button');
  if ($deni.length) {
    $deni.on('click', function() {
      $(this).blur();
      window.location.replace($(this).data().url);
    });
  }
});

/*
    index.html; js/main/index.js
    lists/show_list.html; js/lists/show_list.js
    lists/edit_item.html; js/lists/edit_item.js
    lists/current_lists.html; js/lists/current_lists.js
    labels/edit.html; js/labels/edit.js
    blogs/show_item.html; js/blogs/show_item.js
    blogs/current_blogs.html; js/blogs/current_blogs.js
    announce/announcement.html; js/announce/announcement.js
    admin/society.html; js/admin/society.js
    admin/pictures.html; js/admin/pictures.js
*/

function showHideBlock() {
  var $body = $(this).siblings('.block-body');
  if ($body.is(':hidden')) {
    $body.slideDown('slow');
    scrollPanel($(this));
  } else {
    $body.slideUp('slow');
  }
}

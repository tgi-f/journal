$(document).ready(function() {
  moment = customizeMoment(moment);
  formatFooter(moment);
  formatDateTime($('.today'), moment);
  setInterval(
    function() {
      $('.today').text(
        moment().format('LL') + ', ' + moment().format('HH:mm:ss'));
    }, 1000);
  $('.slidable .block-header').on('click', function() {
    window.location.replace($(this).data().url);
  });
  $('.slidable').find('.block-body').removeClass('to-be-hidden');
  $('#public').on('change', function() {
    if (!$(this).is(':checked')) {
      if ($('#main').is(':checked')) {
        $('#main').trigger('click');
      }
    }
  });

  $('#main').on('change', function() {
    if ($(this).is(':checked')) {
      if (!$('#public').is(':checked')) {
        $('#public').trigger('click');
      }
    }
  });

  $('#abstract').on('keyup blur', {max: 50, block: '.form-group'}, markInputError);

  $('#body').on('keyup blur', function() {
    var $value = $(this).val();
    var $marker = $('#length-marker');
    var $mblock = $('.length-marker');
    var $block = $(this).parents('.form-group');
    $marker.text(512 - $value.length);
    if ($value.length > 512 || $value.length == 0) {
      $block.addClass('has-error');
      if ($value.length > 512) {$mblock.addClass('error');}
    } else {
      if ($block.hasClass('has-error')) {$block.removeClass('has-error');}
      if ($mblock.hasClass('error')) {$mblock.removeClass('error');}
    }
  });

  var $push = $('.pushprelim');
  var token = $push.data().tee;
  $push.on('click', {token: token, target: '#body'}, showPrelim);

  $('#preliminary').on('click', '.closeprelim', function() {
    $('#preliminary').empty();
  });
});

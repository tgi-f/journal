$(document).ready(function() {
  moment = customizeMoment(moment);
  formatFooter(moment);
  formatDateTime($('.today'), moment);
  setInterval(
    function() {
      $('.today').text(
        moment().format('LL') + ', ' + moment().format('HH:mm:ss'));
    }, 1000);
  $('.date-field').each(function() {formatDateTime($(this), moment);});
  var token = $('#left-panel').data().tee;
  var $form = $('.slidable .to-be-hidden');
  if ($form.find('.error').length) {$form.slideDown('slow');}
  $('.slidable .block-header').on('click', showHideBlock);
  $('.entity-body iframe').each(adjustFrame);
  var $twits = $('.entity-body').find('.twitter-tweet');
  if ($twits.length) { adjustTwits($twits); }

  $('#public').on('change', function() {
    if (!$(this).is(':checked')) {
      if ($('#main').is(':checked')) {
        $('#main').trigger('click');
      }
    }
  });

  $('#main').on('change', function() {
    if ($(this).is(':checked')) {
      if (!$('#public').is(':checked')) {
        $('#public').trigger('click');
      }
    }
  });

  $('#abstract').on('keyup blur', {max: 50, block: '.form-group'}, markInputError);

  $('#body').on('keyup blur', function() {
    var $value = $(this).val();
    var $marker = $('#length-marker');
    var $mblock = $('.length-marker');
    var $block = $(this).parents('.form-group');
    $marker.text(512 - $value.length);
    if ($value.length > 512 || $value.length == 0) {
      $block.addClass('has-error');
      if ($value.length > 512) {$mblock.addClass('error');}
    } else {
      if ($block.hasClass('has-error')) {$block.removeClass('has-error');}
      if ($mblock.hasClass('error')) {$mblock.removeClass('error');}
    }
  });

  $('.show-this').on('click', function() {
    var $icon = $(this).find('.glyphicon');
    var $body = $(this).parents('.announce-options').siblings('.entity-body');
    if ($body.is(':hidden')) {
      $body.slideDown('slow', function() {
        $icon.removeClass('glyphicon-chevron-down')
             .addClass('glyphicon-chevron-up');
        $body.find('img').each(adjustBlogimage);
      });
    } else {
      $body.slideUp('slow', function() {
        $icon.removeClass('glyphicon-chevron-up')
             .addClass('glyphicon-chevron-down');
      });
    }
    $(this).blur();
  });

  $('.trash-button').on('click', function() {
    var $remove = $(this).siblings('.remove-button');
    if ($remove.is(':hidden')) {
      $remove.fadeIn('slow');
    } else {
      $remove.fadeOut('slow');
    }
    $(this).blur();
  });

  $('.content-block').on('mouseleave', function() {
    var $remove = $(this).find('.remove-button');
    if (!$remove.is(':hidden')) {$remove.fadeOut('slow');}
  });

  $('.remove-button')
  .on('click', {token: token, view: 'announce'}, removeTarget);

  $('.pushprelim').on(
    'click', {token: token, target: '#body'}, showPrelim);

  $('#preliminary').on('click', '.closeprelim', function() {
    $('#preliminary').empty();
  });
});

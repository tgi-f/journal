/*
    blogs/show_item.html; js/blogs/show_item.js;
    lists/show_list.html; js/lists/show_list.js;
*/


function countClicks(url, suffix) {
  $.post(
    url,
    {
      suffix: suffix,
    },
    function(data) {
      if (!data.empty) {
        var html = '<span class="glyphicon glyphicon-eye-open ' +
                   'white-space"></span>' + data.clicks;
        $('.clicks-show').empty().append(html);
      }
    },
    'json');
}

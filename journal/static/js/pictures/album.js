$(document).ready(function() {
  moment = customizeMoment(moment);
  formatFooter(moment);
  $('.today').each(function() {formatDateTime($(this), moment)});
  setInterval(
    function() {
      $('.today').text(
        moment().format('LL') + ', ' +
        moment().format('HH:mm:ss'));
    },
    1000);
  var token = $('#left-panel').data().tee;
  prepareSpace();

  if ($('.flashed-message').length) {
    var pat = /suffix:\w{6}/;
    var $flashed = $('.flashed-message');
    var $html = $flashed.find('.alert').html();
    if ($html.search(pat) > 0) {
      var suffix = $html.match(pat)[0].split(':')[1];
      var result = 'Изображение загружено ранее в <a href="/pictures/' +
                   suffix + '">альбом</a>.';
      $html = $html.replace(pat, result);
      $flashed.find('.alert').html($html);
    }
  }

  formatDateTime($('#album-created'), moment);

  $('.upload-new').on('click', showForm);

  $('.upload-from').on('click', function() {
    var $form = $('.block-body-form');
    var $link = $('.block-link-form');
    if ($link.is(':hidden')) {
      if(!$form.is(':hidden')) { $form.slideUp('slow');}
      $link.slideDown('slow');
    } else {
      $link.slideUp('slow');
    }
    $(this).blur();
  });

  $('.upload-url')
  .on('click', {token: token}, function(event) {
    var $data = $('#picture-address').val();
    if ($data) {
      $.post(
        $(this).data().url,
        {
          suffix: $(this).data().suffix,
          url: $data,
          csrf_token: event.data.token,
        },
        function(data) {
          if (data.empty) {
            hideForm('.block-link-form');
            $('#picture-address').val('');
          } else {
            window.location.replace(data.redirect);
          }
        },
        'json');
    } else {
      hideForm('.block-link-form');
    }
    $(this).blur();
  });

  $('.album-reload').on('click', redirect);

  $('.go-home').on('click', redirect);

  $('input[name=image]').on('change', function() {
    $('input[name=submit]').trigger('click');
    $('.upload-progress-block').removeClass('hidden');
  });

  $('.show-statistic')
  .on('click', {token: token}, function(event) {
    if ($('.clicked-item').length) {
      hideForm('.block-body-form');
      hideForm('.block-link-form');
      $('.clicked-item').removeClass('clicked-item');
      $('.remove-button').each(function() {$(this).fadeOut('slow');});
      event.data.url = $(this).data().url;
      switchStatistic(event, $(this).data().suffix, moment);
    }
    $(this).blur();
  });

  $('.album-header-panel')
  .on('click', {token: token}, function(event) {
    if (!$(this).hasClass('clicked-item')) {
      hideForm('.block-body-form');
      hideForm('.block-link-form');
      $('.remove-button').each(function() { $(this).fadeOut('slow') });
      $('.clicked-item').removeClass('clicked-item');
      $(this).addClass('clicked-item');
      $.post(
        $(this).data().url,
        {
          suffix: $(this).data().suffix,
          csrf_token: event.data.token,
        },
        function(data){
          if (!data.empty) {
            $('#right-panel').empty().append(data.html);
            formatDateTime($('#picture-uploaded'), moment);
            var $block_width = parseInt($('.album-statistic').width());
            var $pic_width = parseInt($('.picture-body img').attr('width'));
            if ($pic_width >= $block_width) {
              var $pic_height = parseInt($('.picture-body img').attr('height'));
              var width = $block_width - 4;
              var height = Math.round($pic_height / ($pic_width / width));
              $('.picture-body img').attr({"width": width, "height": height});
            }
          }
        },
        "json");
    }
  });

  $('.trash-button').on('click', function() {
    var $album = $(this).parents('.album-tools-panel')
                        .siblings('.album-header-panel');
    if ($album.hasClass('clicked-item')) {
      var $target = $(this).siblings('.remove-button');
      if ($target.is(':hidden')) {
        $target.fadeIn('slow');
      } else {
        $target.fadeOut('slow');
      }
    }
    $(this).blur();
  });

  $('.remove-button')
  .on('click', {token: token, view: 'album'}, removeTarget);

  $('#right-panel').on('click', '.copyLink', showOptionForm);

  $('#right-panel').on('click', '.showForm', showOptionForm);

  $('#right-panel').on('click', '#copy-button', function() {
    var clipboard = new Clipboard('#copy-button');
    clipboard.on('success', function(e) {
      $('.album-form').slideUp('slow');
    });
  });

  $('#right-panel').on('keyup', '#titleField', showFormError);

  $('#right-panel')
  .on('click', '.public-check', {token: token}, changeAlbumStatus);

  $('#right-panel')
  .on('click', '.rename-album', {token: token}, renameAlbum);
});

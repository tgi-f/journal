/*
    pictures/albums.html; js/pictures/albums.js
    pictures/album.html; js/pictures/album.js
*/

function redirect () {
  $(this).blur();
  var $destination = $(this).attr('data-dest');
  window.location.assign($destination);
}

function prepareSpace() {
  var $body = $('.block-body-form');
  $body.hide().removeClass('hidden');
  if ($body.find('.error').length) {
    $body.slideDown('slow');
  }
}

function showForm() {
  var $form = $('.block-body-form');
  var $link = $('.block-link-form');
  if ($form.is(':hidden')) {
    if (!$link.is(':hidden')) { $link.slideUp('slow');}
    $form.slideDown('slow');
  } else {
    $form.slideUp('slow');
  }
  $(this).blur();
}


function hideForm(elem_class) {
  if (!$(elem_class).is(':hidden')) {
    $(elem_class).slideUp('slow');
  }
}

function showOptionForm() {
  var $target = $(this).parents('.album-options').siblings('.album-form');
  if ($target.is(':hidden')) {
    if ($target.hasClass('hidden')) $target.hide().removeClass('hidden');
    $target.slideDown('slow');
  } else {
    $target.slideUp('slow');
  }
  $(this).blur();
}

function showFormError() {
  var $input = $(this).val();
  if ($input.length > 0 && $input.length < 3 || $input.length > 100) {
    $(this).parents('.form-group').addClass('has-error');
  } else if (
         $input.length == 0 || $input.length >= 3 || $input.length <= 100) {
    $(this).parents('.form-group').removeClass('has-error');
  }
}

function changeAlbumStatus(event) {
  $.post(
    $(this).data().url,
    {
      csrf_token: event.data.token,
      suffix: $(this).data().suffix,
    },
    function(data) {
      if (!data.empty) {
        if (!data.public) {
          $('.public-check span')
            .removeClass('glyphicon-ok-circle')
            .addClass('glyphicon-ban-circle');
        } else {
          $('.public-check span')
            .removeClass('glyphicon-ban-circle')
            .addClass('glyphicon-ok-circle');
        }
      }
    },
    "json");
  $(this).blur();
}

function renameAlbum(event) {
  var $title = $.trim($('#titleField').val());
  if ($title.length >= 3 && $title.length <= 100) {
    $('.album-form').slideUp();
    $.post(
      $(this).data().url,
      {
        suffix: $(this).data().suffix,
        csrf_token: event.data.token,
        title: $title,
      },
      function(data) {
        if (!data.empty) {
          window.location.replace(data.redirect);
        }
      },
      "json");
  }
  $(this).blur();
}

function switchStatistic(event, suffix, moment) {
  $.post(
    event.data.url,
    {
      suffix: suffix,
      csrf_token: event.data.token,
    },
    function(data) {
      if (!data.empty) {
        $('#right-panel').empty().append(data.html);
        if ($('#album-created').length) {
          formatDateTime($('#album-created'), moment);
        }
      }
    },
    "json");
}

function adjustPicture() {
  var $slides = $('#slides');
  var $block_width = parseInt($slides.outerWidth());
  var $placeholder = $('.placeholder-pic');
  if ($placeholder.length) {
    var $data = {
      pic_width: parseInt($placeholder.attr('data-width') - 2),
      pic_height: parseInt($placeholder.attr('data-height') - 2),
      pic_src: $placeholder.attr('data-url'),
    };
    var $block = $('.content-block');
    if ($data.pic_width >= $block_width) {
      var width = $block_width;
      var height = Math.round($data.pic_height / ($data.pic_width / width));
      $data.pic_width = width;
      $data.pic_height = height;
    }

    $block.css({
      "margin": "auto",
      "width": $data.pic_width + "px",
      "height": $data.pic_height + "px",
      "background": "url(" + $data.pic_src + ")",
      "background-size": "cover",
    });
  }
}

$(document).ready(function() {
  moment = customizeMoment(moment);
  formatFooter(moment);
  adjustPicture();
  $('#slides').on('click', '.content-block', function() {
    var $suffix = $('.placeholder-pic').attr('data-suffix');
    $.post(
      $(this).data().url,
      {
        csrf_token: $(this).data().tee,
        suffix: $suffix,
      },
      function(data) {
        if (!data.empty) {
          $('#slides').empty().append(data.content);
          adjustPicture();
        }
      },
      'json');
  });
});

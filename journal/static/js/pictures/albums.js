$(document).ready(function() {
  moment = customizeMoment(moment);
  formatFooter(moment);
  formatDateTime($('.today'), moment);
  setInterval(
    function() {
      $('.today').text(
        moment().format('LL') + ', ' +
        moment().format('HH:mm:ss'));
    },
    1000);
  var token = $('#left-panel').data().tee;
  prepareSpace();

  formatDateTime($('#user-registered'), moment);

  $('.create-new').on('click', showForm);

  $('.block-body-form').on('keyup', '#title', showFormError);

  $('.album-header-panel')
  .on('click', {token: token}, function(event) {
    if (!$(this).hasClass('clicked-item')) {
      hideForm('.block-body-form');
      $('.remove-button').each(function() {
        var $folder = $(this).siblings('.show-album');
        $(this).fadeOut('slow', function() {$folder.fadeIn('slow');});
      });
      $('.clicked-item').removeClass('clicked-item');
      $(this).addClass('clicked-item');
      event.data.url = $(this).data().url;
      switchStatistic(event, $(this).data().suffix, moment);
    }
  });

  $('.trash-button').on('click', function() {
    var $album = $(this).parents('.album-tools-panel')
                        .siblings('.album-header-panel');
    if ($album.hasClass('clicked-item')) {
      var $folder = $(this).siblings('.show-album');
      var $remove = $(this).siblings('.remove-button');
      if ($remove.is(':hidden')) {
        $folder.fadeOut('slow', function() {$remove.fadeIn('slow');});
      } else {
        $remove.fadeOut('slow', function() {$folder.fadeIn('slow');});
      }
    }
    $(this).blur();
  });

  $('.remove-button')
  .on('click',
      {token: token, view: 'album'}, removeTarget);

  $('.user-home').on('click', {token: token}, function(event) {
    if ($('.clicked-item').length) {
      hideForm('.block-body-form');
      $('.remove-button').each(function() {
        var $folder = $(this).siblings('.show-album');
        $(this).fadeOut('slow', function() {$folder.fadeIn('slow');});
      });
      $('.clicked-item').removeClass('clicked-item');
      $.post(
        $(this).data().url,
        {
          username: $(this).data().username,
          csrf_token: event.data.token,
        },
        function(data) {
          if (!data.empty) {
            $('#right-panel').empty().append(data.html);
            if ($('#user-registered').length) {
              formatDateTime($('#user-registered'), moment);
            }
          }
        },
        "json");
    }
    $(this).blur();
  });

  $('.album-reload').on('click', redirect);

  $('.show-album').on('click', redirect);

  $('#right-panel').on('click', '.showForm', showOptionForm);

  $('#right-panel').on('keyup', '#titleField', showFormError);

  $('#right-panel')
  .on('click', '.public-check',
      {token: token}, changeAlbumStatus);

  $('#right-panel')
  .on('click', '.rename-album', {token: token}, renameAlbum);
});

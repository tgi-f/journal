$(document).ready(function() {
  moment = customizeMoment(moment);
  formatFooter(moment);
  var token = $('#left-panel').data().tee;
  $('.date-field').each(function() {formatDateTime($(this), moment);});

  $('.slidable .block-header').on('click', showHideBlock);

  var $editor = $('.item-editor-block');
  $editor.slideDown('slow', function() {scrollPanel($editor);});

  $('.list-item-body iframe').each(adjustFrame);

  var $twits = $('.list-item-body').find('.twitter-tweet');
  if ($twits.length) { adjustTwits($twits);}

  $('#items-block').on('click', '.list-item-block .block-header', showListItem);

  $('#items-block').on('click', '.entity-link', function(event) {
    event.preventDefault();
  });

  $('#items-block').on('click', '.slide-button', slideListItem);

  $('.cancel-button').on('click', function() {
    $(this).blur();
    var $address = $(this).attr('data-url');
    window.location.replace($address);
  });

  $('#abstract').on(
  'keyup blur', {max: 100, block: '.form-group'}, markInputError);

  $('#abstract').on('keypress', function(event) {
    if (event.keyCode == 13){
      event.preventDefault();
    }
  });

  $('#body').on('keyup blur', markError);

  $('.save-button').on('click', function() {
    $(this).blur();
    var $blockA = $('#abstract').parents('.form-group');
    var $blockB = $('#body').parents('.form-group');
    if (!$blockA.hasClass('has-error') && !$blockB.hasClass('has-error')) {
      $('#submit').trigger('click');
    }
  });

  $('.pushprelim').on(
    'click', {token: token, target: '#body'}, showPrelim);

  $('#preliminary').on('click', '.closeprelim', function() {
    $('#preliminary').empty();
  });
});

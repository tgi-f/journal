/*
    lists/show_list.html; js/lists/show_list.js
*/

function insertData(
    content, firstRemoved, secondRemoved, top, bottom, parent) {
  $('#twit-platform').remove();
  var $new = $(content);
  $new.find('iframe').each(adjustFrame);
  var $twits = $new.find('.twitter-tweet');
  if ($twits.length) {adjustTwits($twits);}
  firstRemoved.remove();
  secondRemoved.remove();
  if (top.length) {
    $new.insertAfter(top);
  } else if (bottom.length) {
    $new.insertBefore(bottom);
  } else {
    parent.append($new);
  }
  var $clicked = $('.clicked-item');
  $clicked.find('.block-header').trigger('click');
  $clicked.removeClass('clicked-item');
}

$(document).ready(function() {
  moment = customizeMoment(moment);
  formatFooter(moment);
  var $panel = $('#left-panel');
  var token = $panel.data().tee;
  var suffix = $panel.data().suffix;
  var uploading = $panel.data().upload;
  var checker = $panel.data().check;
  var length = $panel.data().length;
  var page = $panel.data().page;

  countClicks($panel.data().count, suffix);

  $('.commentary-body').each(function() {
    $(this).addClass('list-item-body');
  });
  $('.date-field').each(function() { formatDateTime($(this), moment); });
  $('.c-date-field').each(function() { formatDateTime($(this), moment); });

  $('.list-item-body iframe').each(adjustFrame);
  var $twits = $('.list-item-body').find('.twitter-tweet');
  if ($twits.length) {
    adjustTwits($twits);
  }

  $('.commentary-body').each(function() {
    $(this).removeClass('list-item-body');
  });

  $('#items-block').on(
  'click', '.list-item-block .block-header', showListItem);

  $('#items-block').on('click', '.entity-link', linkPropagation);

  $('#items-block').on('click', '.slide-button', slideListItem);

  $('#items-block').on('click', '.edit-item', function() {
    $(this).blur();
    var $address = $(this).attr('data-url');
    window.location.assign($address);
  });

  $('#items-block').on(
  'click', '.up-item', {moment: moment, token: token}, function(event) {
    $(this).blur();
    var $section = $(this).attr('data-s');
    var $sectionBlock = $(this).parents('.list-item-block');
    var $previous = $sectionBlock.prev('.list-item-block');
    var $parent = $sectionBlock.parent('#items-block');
    var $top = $previous.prev('.list-item-block');
    var $bottom = $sectionBlock.next('.list-item-block');
    $.post(
      $(this).data().url,
      {
        section: $section,
        csrf_token: event.data.token,
      },
      function(data) {
        if (!data.empty) {
          insertData(
            data.content, $sectionBlock, $previous, $top, $bottom, $parent);
          if (data.infoline) {
            updateInfoline(data.infoline, event.data.moment);
          }
        }
      },
      'json');
  });

  $('#items-block').on(
  'click', '.down-item', {moment: moment, token: token}, function(event) {
    $(this).blur();
    var $section = $(this).attr('data-s');
    var $sectionBlock = $(this).parents('.list-item-block');
    var $next = $sectionBlock.next('.list-item-block');
    var $parent = $sectionBlock.parent('#items-block');
    var $top = $sectionBlock.prev('.list-item-block');
    var $bottom = $next.next('.list-item-block');
    $.post(
      $(this).data().url,
      {
        section: $section,
        csrf_token: event.data.token,
      },
      function(data) {
        if (!data.empty) {
          insertData(
            data.content, $sectionBlock, $next, $top, $bottom, $parent);
          if (data.infoline) {
            updateInfoline(data.infoline, event.data.moment);
          }
        }
      },
      'json');
  });

  $('#items-block').on('click', '.item-trash-button', function() {
    var $delete = $(this).siblings('.delete-item');
    $(this).blur();
    if ($delete.is(':hidden')) {
      $delete.fadeIn('slow');
    } else {
      $delete.fadeOut('slow');
    }
  });

  $('.slidable .block-header').on('click', showHideBlock);

  $('.options').on('click', '.update-button', updateEntity);

  $('.options').on(
  'click', '.labels-button', function(event) {
    $(this).blur();
    window.location.assign($(this).data().url);
  });

  $('.options').on('click', '.edit-title', function() {
    var $target = $('.title-editor-block');
    var $preamble = $('.text-editor-block');
    var $item = $('.item-editor-block');
    if (!$preamble.is(':hidden')) {$preamble.slideUp('slow');}
    if (!$item.is(':hidden')) {$item.slideUp('slow');}
    if ($target.is(':hidden')) {
      $target.slideDown('slow', function() { scrollPanel($target); });
    } else {
      $target.slideUp('slow', function() { scrollPanel($('#left-panel')); });
    }
    $(this).blur();
  });

  $('.options').on('click', '.edit-preamble', function() {
    var $target = $('.text-editor-block');
    var $title = $('.title-editor-block');
    var $item = $('.item-editor-block');
    if (!$title.is(':hidden')) {$title.slideUp('slow'); }
    if (!$item.is(':hidden')) {$item.slideUp('slow');}
    if ($target.is(':hidden')) {
      $target.slideDown('slow', function() {scrollPanel($target);});
    } else {
      $target.slideUp('slow', function() {scrollPanel($('.common-details'));});
    }
    $(this).blur();
  });

  $('.options').on('click', '.add-item', function() {
    var $title = $('.title-editor-block');
    var $preamble = $('.text-editor-block');
    var $target = $('.item-editor-block');
    if (!$preamble.is(':hidden')) {$preamble.slideUp('slow');}
    if (!$title.is(':hidden')) {$title.slideUp('slow');}
    if ($target.is(':hidden')) {
      $target.slideDown('slow', function() {scrollPanel($target);});
    } else {
      $target.slideUp('slow', function() {scrollPanel($('.common-details'));})
    }
    $(this).blur();
  });

  $('#list-title').on(
  'keyup blur', {max: 64, block: '.title-editor-block'}, markInputError);

  $('#item-abstract').on(
  'keyup blur', {max: 100, block: '.form-group'}, markInputError);

  $('#item-body-editor').on('keyup blur', markError);

  $('#sub-content').on('keyup blur', '#comment-editor', markError);

  $('#sub-content').on(
  'click', '#comment-submit',
  {token: token, uploading: uploading, checker: checker}, sendComment);


  $('#preamble-editor').on('keyup blur', function() {
    var $value = $(this).val();
    var $marker = $('#length-marker');
    var $mblock = $('.length-marker');
    var $block = $('.text-editor-block');
    $marker.text(384 - $value.length);
    if ($value.length > 384) {
      $block.addClass('has-error');
      $mblock.addClass('error');
    } else {
      if ($block.hasClass('has-error')) $block.removeClass('has-error');
      if ($mblock.hasClass('error')) $mblock.removeClass('error');
    }
  });

  $('#title-submit').on(
  'click', {token: token, suffix: suffix}, function(event) {
    event.preventDefault();
    $(this).blur();
    var $title = $('#list-title').val();
    if ($title.length > 0 && $title.length <=64) {
      $.post(
        $(this).data().url,
        {
          title: $title,
          suffix: event.data.suffix,
          csrf_token: event.data.token,
        },
        function(data) {
          if (!data.empty) {
            window.location.replace(data.redirect);
          }
        },
        'json');
    }
  });

  $('#preamble-submit').on(
  'click', {token: token, suffix: suffix}, function(event) {
    event.preventDefault();
    $(this).blur();
    var $preamble = $('#preamble-editor').val();
    if ($preamble.length < 384) {
      $.post(
        $(this).data().url,
        {
          preamble: $preamble,
          suffix: event.data.suffix,
          csrf_token: event.data.token,
        },
        function(data) {
          if (!data.empty) {
            window.location.replace(data.redirect);
          }
        },
        'json');
    }
  });

  $('#item-submit').on(
  'click',
  {token: token, suffix: suffix, uploading: uploading, checker: checker},
  function(event) {
    event.preventDefault();
    $(this).blur();
    var $abstract = $('#item-abstract').val();
    var $body = $('#item-body-editor').val();
    $('#item-abstract').trigger('blur');
    $('#item-body-editor').trigger('blur');
    var $blockA = $('#item-abstract').parents('.form-group');
    var $blockB = $('#item-body-editor').parents('.form-group');
    if (!$blockA.hasClass('has-error') && !$blockB.hasClass('has-error')) {
      $.post(
        $(this).data().url,
        {
          abstract: $abstract,
          body: $body,
          suffix: event.data.suffix,
          csrf_token: event.data.token,
        },
        function(data) {
          if (data.task_id) {
            showProgress('.item-editor-block', event.data.uploading);
            checkTask(
              800, event.data.checker, event.data.token,
              data.task_id, data.redirect);
          }
        },
        'json');
    }
  });

  $('#items-block').on(
  'click', '.delete-item',
  {token: token, uploading: uploading, checker: checker},
  function(event) {
    $(this).blur();
    var $section = $(this).attr('data-s');
    $(this).parents('.item-options-block')
           .siblings('.item-body-block').addClass('being-deleted');
    $.post(
      $(this).data().url,
      {
        section: $section,
        csrf_token: event.data.token,
      },
      function(data) {
        if (data.task_id) {
          showProgress('.being-deleted', event.data.uploading);
          checkTask(
            800, event.data.checker, event.data.token,
            data.task_id, data.redirect);
        }
      },
      'json');
  });

  $('.options').on(
  'click', '.tape-in',
  {token: token, suffix: suffix, entity: 'list_'}, changeReplace);

  $('.options').on(
  'click', '.tape-out',
  {token: token, suffix: suffix, entity: 'list_'}, changeReplace);

  $('.options').on(
  'click', '.like-button',
  {token: token, suffix: suffix, entity: 'list_'}, changeTarget);

  $('.options').on(
  'click', '.dislike-button',
  {token: token, suffix: suffix, entity: 'list_'}, changeTarget);

  $('.options').on(
  'click', '.public-button',
  {moment: moment, token: token, suffix: suffix, entity: 'list_'},
  changeTarget);

  $('.options').on(
  'click', '.heap-button',
  {token: token, suffix: suffix, entity: 'list_'}, changeTarget);

  $('.options').on(
  'click', '.censor-this',
  {token: token, suffix: suffix, entity: 'list_'}, changeReplace);

  $('.options')
  .on('click', '.commentary-state-button',
      {token: token, suffix: suffix, entity: 'list_', length: length},
      changeTarget);

  $('.options').on(
  'click', '.list-status-button',
  {token: token, suffix: suffix, entity: 'list_'}, changeTarget);

  $('.options').on('click', '.trash-button', clickTrash);

  $('.options').on(
  'click', '.remove-button',
  {token: token, suffix: suffix, uploading: uploading, checker: checker},
  function(event) {
    $(this).blur();
    $.post(
      $(this).data().url,
      {
        suffix: event.data.suffix,
        csrf_token: event.data.token,
      },
      function(data) {
        if (data.task_id) {
          if ($('.list-alert').length) {
            $('.list-alert').removeClass('alert alert-warning')
                            .addClass('content-block');
            showProgress('.list-alert', event.data.uploading);
          } else {
            showProgress('#items-block', event.data.uploading);
            $('#items-block').addClass('content-block')
                             .css({"margin-top": "6px",});
          }
          $('.options').hide();
          checkTask(
            800, event.data.checker, event.data.token,
            data.task_id, data.redirect);
        }
      },
      'json');
  });

  $('#sub-content').on(
  'click', '.new-comment-button',
  {token: token, suffix: suffix, entity: 'list_'}, clickComment);

  $('.show-this-comment').on('click', showCommentary);

  $('#sub-content').on('click', '.slide-all', slideAllCommentaries);

  $('.answer-button').on(
  'click',
  {token: token, suffix: suffix, entity: 'list_', page: page},
  clickAnswer);

  $('.remove-comment').on(
  'click', {token: token, page: page}, removeCommentary);

  $('.blocking-button').on(
  'click',
  {token: token, suffix: suffix, page: page}, banCommentator);

  $('.pushprelim').on(
    'click', {token: token, target: '#item-body-editor'}, showPrelim);

  $('.new-comment-block').on(
    'click', '.pushprelim',
    {token: token, target: '#comment-editor'}, showPrelim);

  $('.answer-comment-block').on(
    'click', '.pushprelim',
    {token: token, target: '#comment-editor'}, showPrelim);

  $('#preliminary').on('click', '.closeprelim', function() {
    $('#preliminary').empty();
  });
});

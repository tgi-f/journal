$(document).ready(function() {
  moment = customizeMoment(moment);
  formatFooter(moment);
  var $today = $('.today');
  if ($today.length) {
    formatDateTime($today, moment);
    setInterval(
      function() {
        $today
        .text(moment().format('LL') + ', ' + moment().format('HH:mm:ss'));
      }, 1000);
  }
  $('.date-field').each(function() {
    formatDateTime($(this), moment);
  });

  $('.announcement-block .block-header').on('click', showHideAnnounce);
  $('.announcement-body iframe').each(adjustFrame);
  var $twits = $('.announcement-body').find('.twitter-tweet');
  if ($twits.length) { adjustTwits($twits); }

  var $form = $('.to-be-hidden');
  if ($form.find('.error').length) {
    $form.slideDown('slow');
  }

  $('.entity-link').on('click', linkPropagation);

  $('#title').on('keyup', {max: 64, block: '.form-group'}, markInputError)

  $('.slidable .block-header').on('click', showHideBlock);
});

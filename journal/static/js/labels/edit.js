$(document).ready(function() {
  moment = customizeMoment(moment);
  formatFooter(moment);
  scrollPanel($('.labels-editor'));
  $('#names').focus();
  var entity = $('#left-panel').data().entity;
  $('.date-field').each(function() {formatDateTime($(this), moment);});
  $('.slidable .block-header').on('click', showHideBlock);
  $('.cancel-button').on('click', function() {
    $(this).blur();
    window.location.replace($(this).attr('data-u'));
  });
  if (entity == 8) {
    $('.entity-body-block img').each(adjustBlogimage);
    $('.entity-body-block iframe').each(adjustFrame);
    var $twits = $('.entity-body-block').find('.twitter-tweet');
    if ($twits.length) {adjustTwits($twits);}
  } else if (entity == 7) {
    $('.list-item-body iframe').each(adjustFrame);
    var $twits = $('.list-item-body').find('.twitter-tweet');
    if ($twits.length) {adjustTwits($twits);}
    $('.entity-link').on('click', function(event) {
      event.preventDefault();
    });
    $('.list-item-block .block-header').on('click', showListItem);
    $('.slide-button').on('click', function() {
      $(this).parents('.list-item-body').slideUp('slow', function() {
        scrollPanel($('.labels-editor'));
      });
    });
  }
});

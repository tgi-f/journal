$(document).ready(function() {
  moment = customizeMoment(moment);
  formatFooter(moment);
  var $panel = $('#data-panel');
  var timeout = $panel.data().timeout;
  setTimeout(
    function(url, csrf, id, des) {
      var timer = setInterval(
        function(u, c, i) {
          $.post(
            u,
            {csrf_token:c, task_id:i},
            function(data) {
              if (data=='SUCCESS' || data=='RETRY' || data=='FAILURE') {
                clearInterval(timer);
                var $des = $('.destiny').attr('href');
                window.location.replace($des);

              }
            }, 'text');
        },
        timeout, url, csrf, id);
    },
    timeout, $panel.data().url,
    $panel.data().tee, $panel.data().task);
});

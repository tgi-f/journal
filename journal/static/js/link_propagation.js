/*
    index.html; js/main/index.js
    lists/show_list.html; js/lists/show_list.js
    lists/current_lists.html; js/lists/current_lists.js
    blogs/current_blogs.html; js/blogs/current_blogs.js
*/

function linkPropagation(event) {
  event.stopPropagation();
}

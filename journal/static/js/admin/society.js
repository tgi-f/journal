$(document).ready(function() {
  moment = customizeMoment(moment);
  formatFooter(moment);
  formatDateTime($('.today'), moment);
  setInterval(
    function() {
      $('.today')
      .text(moment().format('LL') + ', ' + moment().format('HH:mm:ss'));
    }, 1000);
  $('.slidable .block-header').on('click', showHideBlock);
  var $form = $('.to-be-hidden');
  if ($form.find('.error').length) $form.slideDown('slow');

  $('.date-field').each(function() {formatDateTime($(this), moment);});

  $('#username-input').on(
  'keyup', function(event) {
    var list = [0, 8, 9, 13, 17, 18, 20, 27, 32, 33, 34, 35, 36, 37, 38, 39,
                40, 45, 46, 91, 93, 144];
    var $value = $(this).val();
    var home = $(this).data().home;
    if ($.inArray(event.which, list) == -1 ||
        ((event.which == 8 || event.which == 46) && $value != '')) {
      $.post(
        $(this).data().url,
        {
          value: $value,
          csrf_token: $(this).data().tee,
        },
        function(data) {
          if (!data.empty) {
            $('#center-panel').empty().append(data.html);
            $('.date-field')
            .each(function() {formatDateTime($(this), moment);});
          }
        },
        'json');
    }
    if ((event.which == 46 || event.which == 8) && $value == '') {
      window.location.replace(home);
    }
  });
});

function adjustPicture() {
  var $block_width = parseInt($(this).parents('.picture-itself').width());
  var $pic_width = parseInt($(this).attr('width'));
  if ($pic_width >= $block_width) {
    var $pic_height = parseInt($(this).attr('height'));
    var width = $block_width - 4;
    var height = Math.round($pic_height / ($pic_width / width));
    $(this).attr({"width": width, "height": height});
  }
}

$(document).ready(function() {
  moment = customizeMoment(moment);
  formatFooter(moment);
  var token = $('#left-panel').data().tee;
  $('.date-field').each(function() {formatDateTime($(this), moment);});
  $('.picture-itself img').each(adjustPicture);
  $('.slidable .block-header').on('click', showHideBlock);

  $('#left-panel').on('click', '.details-button', function() {
    var $info = $(this).parents('.picture-block').siblings('.picture-info');
    var $icon = $(this).find('.glyphicon');
    if ($info.is(':hidden')) {
      $info.hide();
      $info.removeClass('hidden');
      $info.slideDown('slow');
      $(this).attr({"title": "скрыть детали"});
      $icon.removeClass('glyphicon-chevron-down')
           .addClass('glyphicon-chevron-up');
    } else {
      $info.slideUp('slow');
      $(this).attr({"title": "показать детали"});
      $icon.removeClass('glyphicon-chevron-up')
           .addClass('glyphicon-chevron-down');
    }
    $(this).blur();
  });

  $('#left-panel')
  .on('click', '.slides-off', {token: token}, function(event) {
    var $page = $(this).data().page;
    if ($page == 1) { $page = null;}
    $.post(
      $(this).data().url,
      {
        suffix: $(this).data().s,
        page: $page,
        csrf_token: event.data.token,
      },
      function(data) {
        if (!data.empty) { window.location.replace(data.redirect); }
      },
      'json');
  });

  $('#left-panel')
  .on('click', '.remove-button',
      {token: token, view: 'admin-pics'}, removeTarget);

  $('#left-panel').on('click', '.trash-button', function() {
    $button = $(this).siblings('.remove-button');
    if ($button.is(':hidden')) {
      $button.hide();
      $button.removeClass('hidden');
      $button.fadeIn('slow');
    } else {
      $button.fadeOut('slow');
    }
    $(this).blur();
  });

  $('#suffix-input').on('keyup', {token: token}, function(event) {
    var $value = $(this).val();
    if (event.which == 13 && $value) {
      $.post(
        $(this).data().url,
        {
          suffix: $value,
          csrf_token: event.data.token,
        },
        function(data) {
          if (!data.empty) {
            $('#left-panel').empty().prepend(data.html);
            $('.date-field')
            .each(function() {formatDateTime($(this), moment);});
            $('.picture-itself img').each(adjustPicture);
          }
        },
        'json');
      $(this).val("");
      $(this).blur();
    } else if (event.which == 13 && !$value) {
      window.location.replace($(this).data().home);
    }
  });
});

from random import shuffle

from ..models.announce import Announcement


def sort_announces(owner):
    stock = owner.announcements.filter(Announcement.public.is_(True))
    if stock.first():
        published = [a.suffix for a in stock]
        if len(published) > 4:
            shuffle(published)
            published = published[:4]
        return Announcement.query.filter(Announcement.suffix.in_(published))\
            .order_by(Announcement.published.desc())
    return None


def sort_heap_announces():
    stock = Announcement.query.filter(
        Announcement.public.is_(True), Announcement.main.is_(True))
    if stock.first():
        published = [a.suffix for a in stock]
        if len(published) > 8:
            shuffle(published)
            published = published[:8]
        return Announcement.query.filter(Announcement.suffix.in_(published))\
            .order_by(Announcement.published.desc())
    return None

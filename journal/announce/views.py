from datetime import datetime

from flask import abort, current_app, jsonify, render_template, request, url_for
from flask_login import current_user, login_required

from . import announce
from .forms import CreateAnnouncement
from ..accessories import define_page
from ..decoration import permission_required
from ..models.announce import Announcement
from ..models.auth_units import permissions
from ..tasks.announce import create_new, edit_this, remove_announcement


@announce.route('/announcements', methods=['GET', 'POST'])
@login_required
@permission_required(permissions.MAKE_ANNOUNCEMENT)
def show_announcements():
    form = CreateAnnouncement()
    if form.validate_on_submit():
        task = create_new.delay(
            form.abstract.data, form.body.data,
            form.public.data, form.main.data,
            current_user.username)
        message = 'Создаю новое объявление, дождитесь перенаправления.'
        return render_template(
            'item_redirect.html', entity='announcement',
            task_id=task.id, message=message, timeout=800)
    pagination = Announcement.query.filter(Announcement.owner == current_user)\
        .order_by(Announcement.published.desc())\
        .paginate(
        page=request.args.get('page', 1, type=int),
        per_page=current_app.config.get('ANNOUNCEMENTS_PER_PAGE', 3),
        error_out=True)
    return render_template(
        'announce/announcements.html', form=form, pagination=pagination,
        now=datetime.utcnow())


@announce.route('/edit/<suffix>', methods=['GET', 'POST'])
@permission_required(permissions.MAKE_ANNOUNCEMENT)
def edit_announcement(suffix):
    target = Announcement.query.filter_by(suffix=suffix).first_or_404()
    if target.owner != current_user:
        abort(404)
    form = CreateAnnouncement()
    if form.validate_on_submit():
        task = edit_this.delay(
            target.suffix, form.abstract.data, form.body.data,
            form.public.data, form.main.data)
        message = 'Редактирую объявление.'
        return render_template(
            'redirect.html', task_id=task.id, message=message, timeout=800,
            dest=url_for('announce.show_announcements'))
    form.abstract.data = target.abstract
    form.body.data = target.body
    form.public.data = target.public
    form.main.data = target.main
    return render_template(
        'announce/editor.html', form=form, now=datetime.utcnow())


@announce.route('/action/remove', methods=['POST'])
def remove():
    result = {'empty': True}
    page = define_page(
        request.form.get('page', 1, type=int),
        request.form.get('last', 0, type=int))
    target = Announcement.query.filter_by(
        suffix=request.form.get('suffix', 'random-S', type=str)).first()
    if target and current_user == target.owner:
        task = remove_announcement.delay(target.suffix)
        result = {'empty': False,
                  'task_id': task.id,
                  'redirect': url_for('announce.show_announcements', page=page)}
    return jsonify(result)

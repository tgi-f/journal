from flask_wtf import FlaskForm
from wtforms import BooleanField, StringField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, Length, ValidationError


class CreateAnnouncement(FlaskForm):
    abstract = StringField(
        'Заголовок:',
        validators=[
            DataRequired(message='без заголовка нельзя'),
            Length(min=1, max=50, message='от 1-ого до 50-ти символов')])
    body = TextAreaField(
        'Описание:',
        validators=[
            DataRequired(message='текст отсутствует')])
    public = BooleanField('Публичное')
    main = BooleanField('На главной')
    submit = SubmitField('Сохранить')

    def validate_body(self, field):
        field.data = field.data.replace('\r\n', '\n')
        if len(field.data) > 512:
            raise ValidationError('не более 512 знаков')

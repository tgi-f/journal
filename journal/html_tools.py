from bleach import clean, linkify
from bleach.callbacks import nofollow, target_blank
from bs4 import BeautifulSoup
from markdown import markdown
from mdx_del_ins import DelInsExtension
from pyembed.core.error import PyEmbedError
from pyembed.markdown import PyEmbedMarkdown


def markdown_this(value):
    try:
        html = markdown(
            value,
            extensions=['markdown.extensions.fenced_code',
                        'markdown.extensions.codehilite',
                        'markdown.extensions.nl2br',
                        DelInsExtension(),
                        PyEmbedMarkdown()],
            output_format='html5')
    except PyEmbedError:
        html = markdown(
            value,
            extensions=['markdown.extensions.fenced_code',
                        'markdown.extensions.codehilite',
                        'markdown.extensions.nl2br',
                        DelInsExtension()],
            output_format='html5')
    return html


def clean_this(value):
    tags = ['a', 'blockquote', 'br', 'code', 'del', 'em',
            'iframe', 'img', 'ins', 'hr', 'li', 'ol',
            'pre', 'span', 'strong', 'ul', 'p']
    attrs = {'*': ['class'],
             'a': ['href', 'title'],
             'abbr': ['title'],
             'iframe': ['src', 'width', 'height', 'frameborder'],
             'img': ['src', 'alt']}
    return clean(markdown_this(value), tags=tags, attributes=attrs)


def get_soup(string):
    soup = BeautifulSoup(string, 'html5lib')
    soup.body.unwrap()
    soup.head.unwrap()
    soup.html.unwrap()
    return soup


def prevent_py(attrs, new=False):
    if not new:
        return attrs
    text = attrs['_text']
    if text.endswith('.py') and not text.startswith(('http:', 'https:')):
        return None
    return attrs


def prevent_md(attrs, new=False):
    if not new:
        return attrs
    text = attrs['_text']
    if text.endswith('.md') and not text.startswith(('http:', 'https:')):
        return None
    return attrs


CALLBACKS = [nofollow, target_blank, prevent_py, prevent_md]


def linkify_list(string):
    soup = get_soup(string)
    r = list()
    for item in soup.find_all('li'):
        if item.iframe:
            r.append(str(item))
        else:
            r.append(linkify(str(item), callbacks=CALLBACKS))
    return r


def linkify_ul(string):
    r = linkify_list(string)
    r.insert(0, '<ul>')
    r.append('</ul>')
    return '\n'.join(r)


def linkify_ol(string):
    r = linkify_list(string)
    r.insert(0, '<ol>')
    r.append('</ol>')
    return '\n'.join(r)


def linkify_string(string):
    soup = get_soup(string)
    if soup.p:
        return linkify(string, callbacks=CALLBACKS)
    elif soup.ul:
        return linkify_ul(string)
    elif soup.ol:
        return linkify_ol(string)
    else:
        return string


def linkify_block(html):
    soup = get_soup(html)
    o = map(
        linkify_string, [str(child) for child in soup.children if child.name])
    return '\n'.join(list(o))


def purify(string_):
    r = ''
    for w in get_soup(string_).get_text().replace('\n', ' ').split(' '):
        if len(r) + len(w) + 3 > 256:
            break
        if w:
            r = r + ' ' + w
    if not r:
        r = '.'
    from string import punctuation
    if r[-1] in punctuation:
        r = r[:-1]
    return r.strip() + '...'

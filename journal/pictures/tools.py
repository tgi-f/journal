import requests

from io import BytesIO

from flask import url_for
from PIL import Image


def read_image(binary):
    try:
        image = Image.open(BytesIO(binary))
    except OSError:
        image = None
    if image and image.format not in ('JPEG', 'PNG', 'GIF'):
        image = None
    return image


def arrange_picture(picture):
    image = read_image(picture.picture)
    if image:
        picture.width = image.width
        picture.height = image.height
        picture.format = image.format
        picture.volume = len(picture.picture)
        picture.suffix = picture.gen_suffix(image.format)
    return picture, image


def define_redirect(request, page, suffix):
    if request.form.get('admin', None, type=str):
        return url_for('admin.show_pictures', page=page)
    return url_for('pictures.show_album', suffix=suffix, page=page)


def check_url(url):
    valid, result = ('image/png', 'image/jpeg', 'image/gif'), None
    head = requests.head(url, headers={'Connection': 'close'})
    if head.status_code == 200:
        length = int(head.headers.get('Content-Length', '0'))
        c_type = head.headers.get('Content-Type', 'random-bad')\
                             .split(';')[0].strip().lower()
        if c_type in valid and length and length < 5 * pow(1024, 2):
            get = requests.get(url, headers={'Connection': 'close'})
            if get.status_code == 200:
                image = read_image(get.content)
                if image:
                    result = {'content': get.content,
                              'width': image.width,
                              'height': image.height,
                              'format': image.format,
                              'volume': len(get.content)}
                image.close()
    return result

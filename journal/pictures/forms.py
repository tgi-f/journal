from urllib.parse import urlparse

from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed, FileRequired
from wtforms.fields import BooleanField, StringField, SubmitField
from wtforms.validators import DataRequired, Length, URL, ValidationError


class CreateAlbum(FlaskForm):
    title = StringField(
        'Имя:',
        validators=[DataRequired(message='следует ввести наименование'),
                    Length(min=3, max=100, message='от 3-х до 100-а знаков')])
    public = BooleanField('Публичный')
    submit = SubmitField('Создать')


class UploadFile(FlaskForm):
    image = FileField(
        'picture',
        validators=[FileRequired('файл не выбран'),
                    FileAllowed(['jpeg', 'jpg', 'png', 'gif'],
                                'формат не поддерживается')])
    submit = SubmitField('Выгрузить')

    def validate_image(self, field):
        if len(field.data.filename) > 128:
            raise ValidationError('слишком длинное имя файла')


class GetUrl(FlaskForm):
    url = StringField(
        'URL:',
        validators=[Length(max=512), DataRequired(), URL()])

    def __init__(self, url, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.url.data = url

    def validate_url(self, field):
        parsed = urlparse(field.data)
        if parsed.scheme not in ('http', 'https'):
            raise ValidationError('поддерживаются только http и https ссылки')

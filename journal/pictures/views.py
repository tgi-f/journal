from datetime import datetime

from flask import (
    abort, current_app, jsonify, render_template, request, url_for)
from flask_login import current_user, login_required
from sqlalchemy.sql import func
from werkzeug.utils import secure_filename

from .. import db
from ..accessories import define_page
from ..decoration import permission_required
from ..models.auth import User
from ..models.auth_units import permissions
from ..models.pictures import Album, Picture
from ..tasks.pictures import (
    create_album, remove_one_album, remove_one_picture,
    verify_picture, verify_url)
from . import pictures
from .forms import CreateAlbum, GetUrl, UploadFile
from .tools import define_redirect


@pictures.route('/', methods=['GET', 'POST'])
@login_required
@permission_required(permissions.UPLOAD_PICTURES)
def show_albums():
    form = CreateAlbum()
    if form.validate_on_submit():
        task = create_album.delay(
            form.title.data, form.public.data, current_user.username)
        message = 'Создаю новый альбом.'
        return render_template(
            'redirect.html', task_id=task.id, message=message, timeout=800,
            dest=url_for('pictures.show_albums'))
    pagination = Album.query.filter_by(owner=current_user)\
        .order_by(Album.changed.desc()).paginate(
        request.args.get('page', 1, type=int),
        per_page=current_app.config.get('ALBUMS_PER_PAGE', 3),
        error_out=True)
    return render_template(
        'pictures/albums.html', pagination=pagination, form=form,
        now=datetime.utcnow())


@pictures.route('/<suffix>', methods=['GET', 'POST'])
@login_required
@permission_required(permissions.UPLOAD_PICTURES)
def show_album(suffix):
    target = Album.query.filter_by(suffix=suffix).first_or_404()
    if current_user != target.owner:
        abort(404)
    form = UploadFile()
    if form.validate_on_submit():
        picture = Picture(
            picture=form.image.data.stream.read(),
            filename=secure_filename(form.image.data.filename))
        target.pictures.append(picture)
        db.session.add(picture)
        db.session.commit()
        form.image.data.stream.close()
        task = verify_picture.delay(picture.id)
        message = 'Верификация изображения.'
        return render_template(
            'redirect.html', task_id=task.id, message=message, timeout=800,
            dest=url_for('pictures.show_album', suffix=target.suffix))
    pagination = target.pictures.filter(Picture.suffix.isnot(None))\
        .order_by(Picture.uploaded.desc()).paginate(
        request.args.get('page', 1, type=int),
        per_page=current_app.config.get('ALBUMS_PER_PAGE', 3),
        error_out=True)
    return render_template(
        'pictures/album.html', target=target, form=form, pagination=pagination,
        now=datetime.utcnow())


@pictures.route('/action/check-url')
def check_task():
    task_id = request.args.get('taskid', None, type=str)
    suffix = request.args.get('suffix', None, type=str)
    if task_id and suffix:
        message = 'Верификация изображения.'
        return render_template(
            'redirect.html', task_id=task_id, message=message, timeout=800,
            dest=url_for('pictures.show_album', suffix=suffix))
    return abort(404)


@pictures.route('/action/get-url', methods=['POST'])
def upload_from():
    result = {'empty': True}
    album = Album.query.filter_by(
        suffix=request.form.get('suffix', 'rand-S', type=str)).first()
    form = GetUrl(request.form.get('url', None, type=str), csrf_enabled=False)
    if current_user == album.owner and form.validate():
        task = verify_url.delay(form.url.data, album.suffix)
        result = {'empty': False,
                  'redirect': url_for(
                      'pictures.check_task', taskid=task.id,
                      suffix=album.suffix, _external=True)}
    return jsonify(result)


@pictures.route('/show-slides')
def show_slides():
    pic = Picture.query.join(Picture, Album.pictures)\
        .filter(Album.public.is_(True))\
        .filter(Picture.width > 200, Picture.height > 200)\
        .order_by(func.random()).first()
    return render_template('pictures/slides.html', pic=pic)


@pictures.route('/show-next', methods=['POST'])
def show_next():
    pic = Picture.query.join(Picture, Album.pictures)\
        .filter(Album.public.is_(True))\
        .filter(
            Picture.width > 200,
            Picture.height > 200,
            Picture.suffix != request.form.get('suffix', 'random_S', str))\
        .order_by(func.random()).first()
    result = {'empty': True}
    if pic:
        result = {'empty': False,
                  'content': render_template(
                      'pictures/next_slide.html', pic=pic)}
    return jsonify(result)


@pictures.route('/action/show/picture-statistic', methods=['POST'])
def show_picture():
    result = {'empty': True}
    picture = Picture.query.filter_by(
        suffix=request.form.get('suffix', 'random_S', type=str)).first()
    if picture and picture.album.owner == current_user:
        result = {'empty': False,
                  'html': render_template(
                      'pictures/picture_statistic.html', picture=picture)}
    return jsonify(result)


@pictures.route('/action/change/status', methods=['POST'])
def change_status():
    result = {'empty': True}
    target = Album.query.filter_by(
        suffix=request.form.get('suffix', 'random_s', type=str)).first()
    if target and current_user == target.owner:
        if target.public:
            target.public = False
        else:
            target.public = True
        db.session.add(target)
        db.session.commit()
        result = {'empty': False,
                  'public': target.public}
    return jsonify(result)


@pictures.route('/action/rename/album', methods=['POST'])
def rename_album():
    result = {'empty': True}
    target = Album.query.filter_by(
        suffix=request.form.get('suffix', 'random_s', type=str)).first()
    title = request.form.get('title', None, type=str)
    if target and title and current_user == target.owner:
        target.title = title
        db.session.add(target)
        db.session.commit()
        result = {'empty': False,
                  'redirect': url_for(
                      'pictures.show_album', suffix=target.suffix)}
    return jsonify(result)


@pictures.route('/action/show/album-statistic', methods=['POST'])
def show_album_statistic():
    result = {'empty': True}
    target = Album.query.filter_by(
        suffix=request.form.get('suffix', 'random_s', type=str)).first()
    if target and current_user == target.owner:
        result = {'empty': False,
                  'html': render_template(
                      'pictures/album_statistic.html', target=target)}
    return jsonify(result)


@pictures.route('/action/show/user-statistic', methods=['POST'])
def show_user_statistic():
    result = {'empty': True}
    target = User.query.filter_by(
        username=request.form.get('username', '_random_s', type=str)).first()
    if target and current_user == target:
        result = {'empty': False,
                  'html': render_template(
                      'pictures/user_statistic.html', target=target)}
    return jsonify(result)


@pictures.route('/action/remove/album', methods=['POST'])
def remove_album():
    page = define_page(
        request.form.get('page', 1, type=int),
        request.form.get('last', 0, type=int))
    result = {'empty': True}
    target = Album.query.filter_by(
        suffix=request.form.get('suffix', 'random-S', type=str)).first()
    if target and target.owner == current_user:
        task = remove_one_album.delay(target.suffix)
        result = {'empty': False,
                  'task_id': task.id,
                  'redirect': url_for('pictures.show_albums', page=page)}
    return jsonify(result)


@pictures.route('/action/remove/picture', methods=['POST'])
def remove_picture():
    result = {'empty': True}
    page = define_page(
        request.form.get('page', 1, type=int),
        request.form.get('last', 0, type=int))
    target = Picture.query.filter_by(
        suffix=request.form.get('suffix', 'random-S', type=str)).first()
    if target and (target.album.owner == current_user or
                   current_user.is_root()):
        task = remove_one_picture.delay(target.suffix)
        result = {'empty': False,
                  'task_id': task.id,
                  'redirect': define_redirect(
                      request, page, target.album.suffix)}
    return jsonify(result)

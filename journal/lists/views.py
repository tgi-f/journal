from datetime import datetime

from flask import (
    abort, current_app, flash, jsonify,
    redirect, render_template, request, url_for)
from flask_login import current_user, login_required

from . import lists
from .forms import CreateList, EditItem
from .tools import change_list_status
from .. import csrf, db
from ..announce.tools import sort_announces, sort_heap_announces
from ..accessories import increment_views
from ..blogs.tools import parse_data
from ..decoration import permission_required
from ..models.auth import Follow, User
from ..models.auth_units import permissions
from ..models.commentaries import Commentary
from ..models.labels import Label
from ..models.lists import List, Section
from ..tasks.lists import (
    add_new_item, create_list, delete_list_item,
    edit_list_item, remove_one_list)


@lists.route('/', methods=['GET', 'POST'])
@login_required
def show_lists():
    if not current_user.can(permissions.CREATE_ENTITY)\
            and not current_user.lists.first():
        return redirect(url_for('lists.show_heap'))
    form = None
    if current_user.can(permissions.CREATE_ENTITY):
        form = CreateList()
        if form.validate_on_submit():
            task = create_list.delay(form.title.data, current_user.username)
            message = 'Создаю новый список, дождитесь перенаправления.'
            return render_template(
                'item_redirect.html', entity='list_',
                task_id=task.id, message=message, timeout=800)
    pagination = List.query.filter(List.owner == current_user)\
        .order_by(List.edited.desc()).paginate(
        page=request.args.get('page', 1, type=int),
        per_page=current_app.config.get('POSTS_PER_PAGE', 3),
        error_out=True)
    return render_template(
        'lists/current_lists.html', form=form, pagination=pagination,
        now=datetime.utcnow(), view=1)


@lists.route('/tag/<labelname>')
@login_required
def sort_labels(labelname):
    label = Label.query.filter_by(name=labelname).first_or_404()
    pagination = label.lists.filter(List.owner == current_user)\
        .order_by(List.edited.desc()).paginate(
        page=request.args.get('page', 1, type=int),
        per_page=current_app.config.get('POSTS_PER_PAGE', 3),
        error_out=True)
    if not pagination.items:
        abort(404)
    return render_template(
        'lists/current_lists.html', form=None, pagination=pagination,
        view=5, labelname=label.name)


@lists.route('/<slug>')
def show_list(slug):
    target = List.query.filter_by(slug=slug).first_or_404()
    if current_user != target.owner and not target.public:
        if current_user.can(permissions.BLOCK_ENTITY) and target.censored:
            pass
        else:
            abort(403)
    pagination = Commentary.query.filter_by(list_id=target.id)\
        .filter(Commentary.parent_id.is_(None))\
        .order_by(Commentary.timestamp.asc()).paginate(
        page=request.args.get('page', 1, type=int),
        per_page=current_app.config.get('COMMENTS_PER_PAGE', 3),
        error_out=True)
    return render_template(
        'lists/show_list.html', list_=target, pagination=pagination)


@lists.route('/action/count-clicks', methods=['POST'])
@csrf.exempt
def count_clicks():
    result = {'empty': True}
    target = List.query.filter_by(
        suffix=request.form.get('suffix', 'rand-S', type=str)).first()
    if target and current_user != target.owner:
        increment_views(target, 'l_clicked')
        result = {'empty': False,
                  'clicks': target.clicked}
    return jsonify(result)


@lists.route('/society/show/heap')
def show_heap():
    if current_user.can(permissions.BLOCK_ENTITY):
        pagination = List.query.filter(
            List.public.is_(True),
            List.censored.is_(False)).order_by(List.published.desc()).paginate(
            page=request.args.get('page', 1, type=int),
            per_page=current_app.config.get('POSTS_PER_PAGE', 3),
            error_out=True)
    else:
        pagination = List.query.filter(
            List.hidden.is_(False),
            List.public.is_(True),
            List.censored.is_(False)).order_by(List.published.desc()).paginate(
            page=request.args.get('page', 1, type=int),
            per_page=current_app.config.get('POSTS_PER_PAGE', 3),
            error_out=True)
    a_s = sort_heap_announces()
    return render_template(
        'lists/current_lists.html', pagination=pagination, form=None,
        view=2, a_s=a_s)


@lists.route('/society/show/heap/tag/<labelname>')
def sort_heap_labels(labelname):
    label = Label.query.filter_by(name=labelname).first_or_404()
    if current_user.can(permissions.BLOCK_ENTITY):
        pagination = label.lists.filter(
            List.public.is_(True),
            List.censored.is_(False)).order_by(List.published.desc()).paginate(
            page=request.args.get('page', 1, type=int),
            per_page=current_app.config.get('POSTS_PER_PAGE', 3),
            error_out=True)
    else:
        pagination = label.lists.filter(
            List.hidden.is_(False),
            List.public.is_(True),
            List.censored.is_(False)).order_by(List.published.desc()).paginate(
            page=request.args.get('page', 1, type=int),
            per_page=current_app.config.get('POSTS_PER_PAGE', 3),
            error_out=True)
    if not pagination.items:
        abort(404)
    return render_template(
        'lists/current_lists.html', form=None, pagination=pagination,
        view=6, labelname=label.name)


@lists.route('/society/show/censored')
@permission_required(permissions.BLOCK_ENTITY)
def show_censored():
    pagination = List.query.filter(List.censored.is_(True))\
        .order_by(List.published.desc()).paginate(
        page=request.args.get('page', 1, type=int),
        per_page=current_app.config.get('POSTS_PER_PAGE', 3),
        error_out=True)
    return render_template(
        'lists/current_lists.html', form=None, pagination=pagination,
        view=9)


@lists.route('/society/show/tape')
@login_required
def show_tape():
    pagination = current_user.followed_lists.paginate(
        page=request.args.get('page', 1, type=int),
        per_page=current_app.config.get('POSTS_PER_PAGE', 3),
        error_out=True)
    if not pagination.items:
        abort(404)
    a_s = sort_heap_announces()
    return render_template(
        'lists/current_lists.html', pagination=pagination, form=None,
        view=3, a_s=a_s)


@lists.route('/society/show/tape/tag/<labelname>')
@login_required
def sort_tape_labels(labelname):
    label = Label.query.filter_by(name=labelname).first_or_404()
    pagination = label.lists.join(Follow, Follow.followed_id == List.owner_id)\
        .filter(Follow.follower_id == current_user.id,
                List.public.is_(True), List.censored.is_(False))\
        .order_by(List.published.desc()).paginate(
        page=request.args.get('page', 1, type=int),
        per_page=current_app.config.get('POSTS_PER_PAGE', 3),
        error_out=True)
    if not pagination.items:
        abort(404)
    return render_template(
        'lists/current_lists.html', form=None, pagination=pagination,
        view=7, labelname=label.name)


@lists.route('/society/<username>')
def show_owner(username):
    target = User.query.filter_by(username=username).first_or_404()
    if current_user == target:
        return redirect(url_for('lists.show_lists'))
    pagination = target.lists.filter(
        List.hidden.is_(False),
        List.public.is_(True),
        List.censored.is_(False)).order_by(
        List.published.desc()).paginate(
        page=request.args.get('page', 1, type=int),
        per_page=current_app.config.get('POSTS_PER_PAGE', 3),
        error_out=True)
    if not pagination.items:
        abort(404)
    a_s = sort_announces(target)
    return render_template(
        'lists/current_lists.html', pagination=pagination, form=None, view=4,
        username=target.username, a_s=a_s)


@lists.route('/society/<username>/tag/<labelname>')
def sort_owner_labels(username, labelname):
    target = User.query.filter_by(username=username).first_or_404()
    label = Label.query.filter_by(name=labelname).first_or_404()
    if current_user == target:
        return redirect(url_for('lists.sort_labels', labelname=labelname))
    pagination = label.lists.filter(
        List.owner == target,
        List.hidden.is_(False),
        List.public.is_(True),
        List.censored.is_(False)).order_by(List.published.desc()).paginate(
        page=request.args.get('page', 1, type=int),
        per_page=current_app.config.get('POSTS_PER_PAGE', 3),
        error_out=True)
    if not pagination.items:
        abort(404)
    return render_template(
        'lists/current_lists.html', form=None, pagination=pagination,
        view=8, username=target.username, labelname=label.name)


@lists.route('/action/edit-title', methods=['POST'])
def edit_title():
    result = {'empty': True}
    target = List.query.filter_by(
        suffix=request.form.get('suffix', 'rand-S', type=str)).first()
    title = request.form.get('title', None, type=str)
    if target and current_user == target.owner and title:
        if target.title != title:
            target.title = title
            target.slug = target.gen_slug(title)
            target.edited = datetime.utcnow()
            db.session.add(target)
            db.session.commit()
            flash('Заголовок списка отредактирован.')
        else:
            flash('Заголовок топика не изменился.')
        result = {'empty': False,
                  'redirect': url_for('lists.show_list', slug=target.slug)}
    return jsonify(result)


@lists.route('/action/edit-preamble', methods=['POST'])
def edit_preamble():
    result = {'empty': True}
    target = List.query.filter_by(
        suffix=request.form.get('suffix', 'rand-S', type=str)).first()
    preamble = request.form.get('preamble', None, type=str)
    if target and current_user == target.owner and preamble:
        if target.preamble != preamble:
            target.preamble = preamble
            target.edited = datetime.utcnow()
            db.session.add(target)
            db.session.commit()
            flash('Описание списка отредактировано.')
        else:
            flash('Описание списка не изменилось.')
        result = {'empty': False,
                  'redirect': url_for('lists.show_list', slug=target.slug)}
    return jsonify(result)


@lists.route('/action/add-item', methods=['POST'])
def add_item():
    result = {'task_id': None}
    target = List.query.filter_by(
        suffix=request.form.get('suffix', 'rand-S', type=str)).first()
    abstract = request.form.get('abstract', None, type=str)
    body = request.form.get('body', None, type=str)
    if target and current_user == target.owner and abstract and body \
            and len(body.replace('\r\n', '\n')) <= 65000:
        task = add_new_item.delay(target.suffix, abstract, body)
        result = {'task_id': task.id,
                  'redirect': url_for('lists.show_list', slug=target.slug)}
    return jsonify(result)


@lists.route('/action/delete-item', methods=['POST'])
def delete_item():
    result = {'task_id': None}
    section = Section.query.get(request.form.get('section', None, type=int))
    if section and section.list_.owner == current_user:
        task = delete_list_item.delay(section.id)
        result = {'task_id': task.id,
                  'redirect': url_for(
                      'lists.show_list', slug=section.list_.slug)}
    return jsonify(result)


@lists.route('/edit/<suffix>', methods=['GET', 'POST'])
def edit_item(suffix):
    list_ = List.query.filter_by(suffix=suffix).first_or_404()
    if current_user != list_.owner:
        abort(403)
    section = list_.sections.filter_by(
        number=request.args.get('item', None, type=int)).first_or_404()
    if section.item.slug:
        abort(404)
    form = EditItem()
    if form.validate_on_submit():
        if form.abstract.data == section.item.abstract \
                and form.body.data == section.item.body:
            flash('Топик №{} не изменился.'.format(section.number))
            return redirect(url_for('lists.show_list', slug=list_.slug))
        else:
            task = edit_list_item.delay(
                form.abstract.data, form.body.data, section.id)
            message = 'Редактирую топик списка.'
            return render_template(
                'redirect.html', task_id=task.id, message=message, timeout=800,
                dest=url_for('lists.show_list', slug=list_.slug))
    form.abstract.data = section.item.abstract
    form.body.data = section.item.body
    return render_template(
        'lists/edit_item.html', list_=list_, section=section, form=form)


@lists.route('/action/change-status', methods=['POST'])
def change_status():
    result = {'empty': True}
    target, options, details = parse_data(request)
    if target and current_user == target.owner:
        target = change_list_status(target)
        result = {'empty': False,
                  'options': render_template(options, item=target),
                  'details': render_template(details, item=target),
                  'infoline': None,
                  'c_options': None}
    return jsonify(result)


@lists.route('/action/up-item', methods=['POST'])
def up_item():
    result = {'empty': True}
    section = Section.query.get(request.form.get('section', None, type=int))
    if section and section.list_.owner == current_user and section.number != 1:
        prev = section.list_.sections\
            .filter_by(number=section.number-1).first()
        prev.number = section.number
        section.number -= 1
        section.list_.edited = datetime.utcnow()
        db.session.add_all((prev, section))
        db.session.commit()
        result = {'empty': False,
                  'content': render_template(
                      'lists/up_item.html', section=section, previous=prev),
                  'infoline': render_template(
                      'blogs/infoline.html', item=section.list_)}
    return jsonify(result)


@lists.route('/action/down-item', methods=['POST'])
def down_item():
    result = {'emtpy': True}
    section = Section.query.get(request.form.get('section', None, type=int))
    if section and section.list_.owner == current_user \
            and section.number < section.list_.sections.count():
        next_ = section.list_.sections\
            .filter_by(number=section.number+1).first()
        next_.number = section.number
        section.number += 1
        section.list_.edited = datetime.utcnow()
        db.session.add_all((next_, section))
        db.session.commit()
        result = {'empty': False,
                  'content': render_template(
                      'lists/down_item.html', section=section, next_=next_),
                  'infoline': render_template(
                      'blogs/infoline.html', item=section.list_)}
    return jsonify(result)


@lists.route('/action/remove-list', methods=['POST'])
def remove_list():
    result = {'task_id': None}
    target = List.query.filter_by(
        suffix=request.form.get('suffix', 'rand-S', type=str)).first()
    if target and current_user == target.owner:
        task = remove_one_list.delay(target.suffix)
        result = {'task_id': task.id,
                  'redirect': url_for('lists.show_lists')}
    return jsonify(result)

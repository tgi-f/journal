from .. import db


def change_list_status(list_):
    if list_.open:
        list_.open = False
    else:
        if list_.owner.lists.filter_by(open=True).count() >= 4:
            closed = list_.owner.lists.filter_by(open=True).first()
            closed.open = False
            db.session.add(closed)
        list_.open = True
    db.session.add(list_)
    db.session.commit()
    return list_

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, Length, ValidationError


class CreateList(FlaskForm):
    title = StringField(
        'Заголовок:',
        validators=[
            DataRequired(message='без заголовка нельзя'),
            Length(min=1, max=64, message='от 1-ого до 64-х символов')])
    submit = SubmitField('Создать')


class EditItem(FlaskForm):
    abstract = StringField(
        'Заголовок:',
        validators=[
            DataRequired(message='без заголовка нельзя'),
            Length(min=1, max=100, message='от 1-ого до 100-а символовэ')])
    body = TextAreaField(
        'Описание:',
        validators=[DataRequired(message='текст отсутствует')])
    submit = SubmitField('Сохранить')

    def validate_body(self, field):
        field.data = field.data.replace('\r\n', '\n')
        if len(field.data) > 65000:
            raise ValidationError('поле содержит больше 65000 символов')

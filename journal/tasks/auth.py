from captcha.image import ImageCaptcha
from flask import current_app

from .. import celery, db, mail
from ..models.auth import Account
from ..models.captcha import Captcha
from ..auth.tools import create_message, resolve_account, Resolver


@celery.task
def request_password(address, index, target):
    app = current_app._get_current_object()
    acc = Account.query.filter_by(address=address).first()
    act = Resolver(acc, address)
    length = app.config.get('TOKEN_LENGTH', 1)
    target = target.replace(
        'token', act.account.generate_token(expiration=round(3600*length)))
    msg = create_message(
        app, [act.account.address], act.subj, act.template,
        username=act.username, index=index, target=target, length=length)
    with app.app_context():
        mail.send(msg)
    if app.config.get('MAIL_DEBUG'):
        return msg.body
    return 'Done!'


@celery.task
def request_email_change(swap, address, index, target):
    app = current_app._get_current_object()
    account = resolve_account(swap, address)
    length = app.config.get('TOKEN_LENGTH', 1)
    target = target.replace(
        'token', account.generate_token(expiration=round(3600*length)))
    msg = create_message(
        app, [account.swap],
        'Change Email Request', 'emails/auth/request_email',
        index=index, target=target, length=length,
        username=account.user.username, old=account.address, new=account.swap)
    with app.app_context():
        mail.send(msg)
    if app.config.get('MAIL_DEBUG'):
        return msg.body
    return 'Done!'


@celery.task
def remove_swap(address):
    account = Account.query.filter_by(address=address).first()
    if account and account.swap:
        account.swap = None
        db.session.add(account)
        db.session.commit()
    return 'Done!'


@celery.task
def change_pattern(suffix):
    captcha = Captcha.query.filter_by(suffix=suffix).first()
    if captcha:
        image = ImageCaptcha(
            fonts=current_app.config.get('CAPTCHA_FONTS'),
            font_sizes=current_app.config.get('CAPTCHA_SIZES'),
            width=120, height=60)
        value = Captcha.gen_value()
        data = image.generate(value, format='jpeg')
        captcha.value = value
        captcha.picture = data.read()
        data.close()
        db.session.add(captcha)
        db.session.commit()
    return 'Done!'

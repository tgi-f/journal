from datetime import datetime

from .. import celery, db
from ..models.auth import User
from ..models.blogs import Item


@celery.task
def create_item(abstract, body, hidden, public, username):
    owner = User.query.filter_by(username=username).first()
    if owner:
        item = Item(
            abstract=abstract, body=body, hidden=hidden, public=public)
        item.owner = owner
        db.session.add(item)
        db.session.commit()
        return item.suffix
    return None


@celery.task
def edit_body(suffix, text):
    item = Item.query.filter_by(suffix=suffix).first()
    item.body = text.replace('\r\n', '\n')
    item.edited = datetime.utcnow()
    db.session.add(item)
    db.session.commit()
    return 'Текст топика отредактирован.'


@celery.task
def remove_blog_item(suffix):
    item = Item.query.filter_by(suffix=suffix).first()
    if item:
        for c in item.commentaries:
            db.session.delete(c)
        for label in item.labels.all():
            item.labels.remove(label)
            if label.is_empty:
                db.session.delete(label)
        if item.sections.first():
            for each in ('created', 'edited', 'published',
                         'suffix', 'slug', 'summary', 'owner_id'):
                setattr(item, each, None)
            db.session.add(item)
        else:
            db.session.delete(item)
        db.session.commit()
        return 'Топик удалён из блога.'
    return None

from datetime import datetime

from .. import celery, db
from ..blogs.tools import create_section
from ..models.auth import User
from ..models.blogs import Item
from ..models.lists import List, Section


@celery.task
def create_list(title, username):
    owner = User.query.filter_by(username=username).first()
    if owner:
        list_ = List(title=title)
        owner.lists.append(list_)
        db.session.add(list_)
        db.session.commit()
        return list_.suffix
    return None


@celery.task
def add_new_item(suffix, abstract, body):
    list_ = List.query.filter_by(suffix=suffix).first()
    item = Item(
        abstract=abstract, body=body.replace('\r\n', '\n'), independent=False)
    create_section(list_, item)
    return 'Список дополнен.'


@celery.task
def edit_list_item(abstract, body, section):
    sec = Section.query.get(section)
    if sec:
        if sec.item.abstract != abstract:
            sec.item.abstract = abstract
        if sec.item.body != body:
            sec.item.body = body
        sec.list_.edited = datetime.utcnow()
        db.session.add(sec)
        db.session.commit()
        return 'Топик №{} отредактирован.'.format(sec.number)
    return None


@celery.task
def delete_list_item(section):
    sec = Section.query.get(section)
    if sec:
        list_ = sec.list_
        for each in list_.sections.filter(Section.number > sec.number):
            each.number -= 1
            db.session.add(each)
        if not sec.item.slug \
                and Section.query.filter_by(item=sec.item).count() == 1:
            db.session.delete(sec.item)
        db.session.delete(sec)
        list_.edited = datetime.utcnow()
        if not list_.sections.count():
            list_.public = False
        db.session.add(list_)
        db.session.commit()
        return 'Топик успешно удалён.'
    return None


@celery.task
def remove_one_list(suffix):
    list_ = List.query.filter_by(suffix=suffix).first()
    if list_:
        for c in list_.commentaries:
            db.session.delete(c)
        for label in list_.labels.all():
            list_.labels.remove(label)
            if label.is_empty:
                db.session.delete(label)
        for section in list_.sections:
            if section.item.sections.count() == 1 and not section.item.slug:
                db.session.delete(section.item)
            db.session.delete(section)
        db.session.delete(list_)
        db.session.commit()
        return 'Список удалён безвозвратно.'
    return None

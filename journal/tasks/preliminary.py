from flask import current_app

from .. import celery
from ..html_tools import clean_this, linkify_block
from ..models.cache import connect, get_unique


@celery.task
def format_preliminary(body):
    rc = connect(current_app.config.get('CACHE_HOST', 'localhost'))
    cache = get_unique(rc, 'prelim:')
    rc.set(cache, linkify_block(clean_this(body)))
    return cache

from datetime import datetime

from .. import celery, db
from ..models.announce import Announcement
from ..models.auth import User


def fix_published(owner):
    if owner.announcements\
            .filter(Announcement.public.is_(True)).count() > 5:
        last = owner.announcements.filter(Announcement.public.is_(True))\
            .order_by(Announcement.published.asc()).first()
        last.public = False
        last.main = False
        db.session.add(last)
        db.session.commit()


@celery.task
def create_new(abstract, body, public, main, username):
    owner = User.query.filter_by(username=username).first()
    if owner:
        announce = Announcement(
            abstract=abstract, body=body.replace('\r\n', '\n'),
            public=public, main=main)
        announce.owner = owner
        db.session.add(announce)
        db.session.commit()
        if announce.public:
            fix_published(announce.owner)
        return announce.suffix
    return None


@celery.task
def edit_this(suffix, abstract, body, public, main):
    target = Announcement.query.filter_by(suffix=suffix).first()
    if target:
        target.abstract = abstract
        target.body = body.replace('\r\n', '\n')
        target.public = public
        target.main = main
        if target.public:
            target.published = datetime.utcnow()
        db.session.add(target)
        db.session.commit()
        if target.public:
            fix_published(target.owner)
        return 'Объявление отредактировано.'
    return None


@celery.task
def remove_announcement(suffix):
    target = Announcement.query.filter_by(suffix=suffix).first()
    if target:
        db.session.delete(target)
        db.session.commit()
        return 'Объявление удалено.'
    return None

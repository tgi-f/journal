from .. import celery
from ..models.auth import User
from ..models.auth_units import groups


@celery.task
def clean_user(username):
    user = User.query.filter_by(username=username).first()
    if user and user.group.title in (groups.curator, groups.root):
        for each in user.blockers.all():
            each.blocker.deblock(user)
        for each in user.blocked.all():
            user.deblock(each.blocked)
        return 'Done!'
    return None

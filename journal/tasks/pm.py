from datetime import datetime

from .. import celery, db
from ..models.auth import User
from ..models.pm import Message


@celery.task
def send_pm(sender, recipient, body):
    sender = User.query.filter_by(username=sender).first()
    recipient = User.query.filter_by(username=recipient).first()
    if body and sender and recipient:
        message = Message(body=body.replace('\r\n', '\n'))
        message.sender = sender
        message.recipient = recipient
        db.session.add(message)
        db.session.commit()
        return 'Ваше сообщение отправлено.'
    return None


@celery.task
def send_edited(message, body):
    body = body.replace('\r\n', '\n')
    message = Message.query.get(message)
    if message:
        if body != message.body:
            message.body = body
            message.postponed = False
            message.sent = datetime.utcnow()
            db.session.add(message)
            db.session.commit()
        return 'Ваше сообщение отправлено.'
    return None

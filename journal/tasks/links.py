from .. import celery, db
from ..models.auth import User
from ..models.links import Link


@celery.task
def create_alias(url, username):
    owner = User.query.filter_by(username=username).first()
    if owner:
        link = Link(url=url)
        owner.links.append(link)
        db.session.add(link)
        db.session.commit()
        return 'Алиас успешно создан.'
    return None

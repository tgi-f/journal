from .. import celery, db
from ..models.auth import User
from ..models.commentaries import Commentary
from ..models.blogs import Item
from ..models.lists import List


@celery.task
def create_commentary(entity, suffix, author, parent_id, body):
    d = {'Item': (Item, 'item'),
         'List': (List, 'list_')}
    target = d[entity][0].query.filter_by(suffix=suffix).first()
    author = User.query.filter_by(username=author).first()
    parent = Commentary.query.get(parent_id)
    if target and author and body:
        commentary = Commentary(body=body.replace('\r\n', '\n'))
        commentary.author = author
        setattr(commentary, d[entity][1], target)
        commentary.parent = parent
        db.session.add(commentary)
        db.session.commit()
        return 'Ваш комментарий добавлен.'
    return None

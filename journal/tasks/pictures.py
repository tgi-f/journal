from datetime import datetime

from .. import celery, db
from ..models.auth import User
from ..models.pictures import Album, Picture
from ..pictures.tools import arrange_picture, check_url


@celery.task
def create_album(title, public, username):
    owner = User.query.filter_by(username=username).first()
    album = Album(title=title, public=public)
    owner.albums.append(album)
    db.session.add(album)
    db.session.commit()
    return 'Альбом успешно создан.'


@celery.task
def verify_picture(id_):
    pic, message = Picture.query.get(id_), None
    if pic:
        replica = Picture.query.join(Album, Picture.album)\
            .filter(Album.owner == pic.album.owner)\
            .filter(Picture.id != pic.id, Picture.picture == pic.picture)\
            .first()
        if replica:
            db.session.delete(pic)
            message = 'suffix:{}'.format(replica.album.suffix)
        else:
            pic, image = arrange_picture(pic)
            if image:
                pic.album.volume += pic.volume
                pic.album.changed = pic.uploaded
                db.session.add_all((pic, pic.album))
                message = 'Изображение успешно добавлено.'
                image.close()
            else:
                db.session.delete(pic)
                message = 'Формат не поддерживается. Запрос отклонён.'
        db.session.commit()
    return message


@celery.task
def verify_url(url, suffix):
    image, message = check_url(url), None
    album = Album.query.filter_by(suffix=suffix).first()
    if image and album:
        replica = Picture.query.join(Album, Picture.album)\
            .filter(Album.owner == album.owner)\
            .filter(Picture.picture == image.get('content')).first()
        if replica:
            message = 'suffix:{}'.format(replica.album.suffix)
        else:
            now = datetime.utcnow()
            picture = Picture(picture=image.get('content'))
            picture.height = image.get('height')
            picture.width = image.get('width')
            picture.format = image.get('format')
            picture.volume = image.get('volume')
            picture.suffix = picture.gen_suffix(image.get('format'))
            picture.filename = picture.suffix
            picture.uploaded = now
            picture.album = album
            album.volume += picture.volume
            album.changed = now
            db.session.add_all((picture, album))
            db.session.commit()
            message = 'Изображение успешно добавлено.'
    if image is None:
        message = 'Формат не поддерживается. Запрос отклонён.'
    return message


@celery.task
def remove_one_album(suffix):
    target = Album.query.filter_by(suffix=suffix).first()
    if target:
        for pic in target.pictures:
            db.session.delete(pic)
        db.session.delete(target)
        db.session.commit()
        return 'Альбом удалён безвозвратно.'
    return None


@celery.task
def remove_one_picture(suffix):
    target = Picture.query.filter_by(suffix=suffix).first()
    if target:
        target.album.volume -= target.volume
        db.session.add(target.album)
        db.session.delete(target)
        db.session.commit()
        return 'Файл удалён безвозвратно.'
    return None

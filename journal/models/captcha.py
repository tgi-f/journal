from random import choice, shuffle
from string import ascii_letters, ascii_lowercase, digits

from captcha.image import ImageCaptcha
from flask import current_app

from .. import db


class Captcha(db.Model):
    __tablename__ = 'captchas'

    id = db.Column(db.Integer, primary_key=True)
    picture = db.Column(db.LargeBinary, nullable=False)
    value = db.Column(db.String(5), unique=True)
    suffix = db.Column(db.String(7), unique=True)

    def __init__(self, value, fonts=None, font_sizes=None):
        image = ImageCaptcha(
            fonts=fonts, width=120, height=60, font_sizes=font_sizes)
        data = image.generate(value, format='jpeg')
        self.value = value
        self.picture = data.read()
        self.suffix = self.gen_suffix()
        data.close()

    def __repr__(self):
        return '<Captcha: {0}>'.format(self.value)

    @classmethod
    def gen_suffix(cls):
        while True:
            s = ''.join([choice(ascii_letters + digits) for _ in range(7)])
            if cls.query.filter_by(suffix=s).first():
                continue
            return s

    @classmethod
    def gen_value(cls):
        cache = list(ascii_lowercase + digits)
        while True:
            shuffle(cache)
            v = ''.join(cache[:5])
            if cls.query.filter_by(value=v).first():
                continue
            return v

    @staticmethod
    def insert_data(amount):
        """
        This method automatically generates and inserts data into this table.
        It must be called one time at the start point when project is being
        deployed and the database is created.
        Args:
            amount: the amount of predefined captcha sets

        Returns: None

        """
        for _ in range(amount):
            current = Captcha(
                Captcha.gen_value(),
                fonts=current_app.config.get('CAPTCHA_FONTS'),
                font_sizes=current_app.config.get('CAPTCHA_SIZES'))
            db.session.add(current)
            db.session.commit()

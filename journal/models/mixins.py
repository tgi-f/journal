from random import choice
from string import ascii_letters, digits
from urllib.parse import urlparse

from flask import url_for

from .. import db
from ..html_tools import clean_this, linkify_block
from .blogs_tools import count_n, make_slug
from .exc import BadRequiredArg, NoneRequiredArg


class BodyMixin:
    body = db.Column(db.Text, nullable=False)
    body_html = db.Column(db.Text)

    def __init__(self, **kwargs):
        if 'body' not in kwargs:
            raise NoneRequiredArg('missing required argument: "body"')
        if not kwargs['body'] or not isinstance(kwargs['body'], str):
            raise BadRequiredArg('bad required argument: "body"')

    @staticmethod
    def on_changed_body(target, value, oldvalue, initiator):
        target.body_html = linkify_block(clean_this(value))


class EntityMixin:
    created = db.Column(db.DateTime)
    edited = db.Column(db.DateTime)
    published = db.Column(db.DateTime)
    public = db.Column(db.Boolean, default=False)
    hidden = db.Column(db.Boolean, default=False)
    censored = db.Column(db.Boolean, default=False)
    commentable = db.Column(db.Boolean, default=True)
    clicked = db.Column(db.Integer, default=0)


class SluggedMixin:
    suffix = db.Column(db.String(6), unique=True)
    slug = db.Column(db.String(64), unique=True)

    @classmethod
    def check_slug(cls, value):
        item = cls.query.filter(cls.slug == make_slug(value)).first()
        try:
            count = max(count_n(each.slug) for each in cls.query.filter(
                cls.slug.like(make_slug(value)+'_'+'%')))
        except ValueError:
            count = 0
        return count, item

    def gen_slug(self, value):
        count, item = self.check_slug(value)
        if item is None:
            return make_slug(value)
        else:
            return make_slug(value) + '_' + str(count + 1)

    @classmethod
    def gen_suffix(cls, n):
        while True:
            value = ''.join([choice(ascii_letters + digits) for _ in range(n)])
            if cls.query.filter_by(suffix=value).first():
                continue
            return value

    @property
    def alias_(self):
        return ''.join(urlparse(
            url_for('main.jump', spring=self.suffix, _external=True))[1:])

    @alias_.setter
    def alias_(self, value):
        raise AttributeError('alias_ cannot be set')

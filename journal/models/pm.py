from datetime import datetime

from .mixins import BodyMixin
from .. import db


class Message(BodyMixin, db.Model):
    __tablename__ = 'messages'

    id = db.Column(db.Integer, primary_key=True)
    sent = db.Column(db.DateTime, default=datetime.utcnow)
    received = db.Column(db.DateTime, default=None)
    postponed = db.Column(db.Boolean, default=False)
    removed_by_sender = db.Column(db.Boolean, default=False)
    removed_by_recipient = db.Column(db.Boolean, default=False)
    sender_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    recipient_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    def __init__(self, **kwargs):
        BodyMixin.__init__(self, **kwargs)
        db.Model.__init__(self, **kwargs)

    def __repr__(self):
        return '<Message: #{0} from {1} to {2}>'.format(
            self.id, self.sender.username, self.recipient.username)


db.event.listen(Message.body, 'set', Message.on_changed_body)

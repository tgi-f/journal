from datetime import datetime
from os.path import splitext
from random import choice
from string import ascii_letters, digits

from .. import db
from .exc import BadRequiredArg, NoneRequiredArg
from .pictures_tools import define_units, shorten_line


class Album(db.Model):
    __tablename__ = 'albums'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    created = db.Column(db.DateTime, default=datetime.utcnow)
    changed = db.Column(db.DateTime, default=datetime.utcnow)
    public = db.Column(db.Boolean, default=False, nullable=False)
    suffix = db.Column(db.String(6), nullable=False, unique=True)
    volume = db.Column(db.Integer, default=0)
    owner_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    pictures = db.relationship('Picture', backref='album', lazy='dynamic')

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)
        if 'title' not in kwargs:
            raise NoneRequiredArg('missing required argument: title')
        if not kwargs['title'] or not isinstance(kwargs['title'], str):
            raise BadRequiredArg('bad required argument: title')
        self.suffix = self.gen_suffix()

    def __repr__(self):
        return '<Album: {}>'.format(self.suffix)

    @classmethod
    def gen_suffix(cls):
        while True:
            value = ''.join([choice(ascii_letters + digits) for _ in range(6)])
            if cls.query.filter_by(suffix=value).first():
                continue
            return value

    def title_(self, length):
        if len(self.title) > length:
            return shorten_line(self.title, length)
        return self.title

    def show_size(self):
        return define_units(self.volume)


class Picture(db.Model):
    __tablename__ = 'pictures'

    id = db.Column(db.Integer, primary_key=True)
    uploaded = db.Column(db.DateTime, default=datetime.utcnow)
    picture = db.Column(db.LargeBinary, nullable=False)
    filename = db.Column(db.String(128))
    width = db.Column(db.Integer)
    height = db.Column(db.Integer)
    format = db.Column(db.String(6))
    volume = db.Column(db.Integer)
    suffix = db.Column(db.String(12), unique=True)
    album_id = db.Column(db.Integer, db.ForeignKey('albums.id'))

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)
        if 'picture' not in kwargs:
            raise NoneRequiredArg('missing required argument: "picture"')
        if not kwargs['picture'] or not isinstance(kwargs['picture'], bytes):
            raise BadRequiredArg('bad required argument: "picture"')

    def __repr__(self):
        return '<Picture: {}>'.format(self.shorten_filename(30))

    @classmethod
    def gen_suffix(cls, format_):
        e = {'JPEG': '.jpg', 'PNG': '.png', 'GIF': '.gif'}
        while True:
            s = ''.join([choice(ascii_letters + digits) for _ in range(8)])\
                + e.get(format_)
            if cls.query.filter_by(suffix=s).first():
                continue
            return s

    def shorten_filename(self, length):
        name, ext = splitext(self.filename)
        if ' ' not in name and len(name) >= length:
            if len(name) == length:
                return self.filename
            return name[:length] + '~' + ext
        title = ''
        for i in name.split(' '):
            tempo = title + ' ' + i
            if len(tempo.lstrip() + '~' + ext) > length + 4:
                return title.lstrip() + '~' + ext
            title = tempo
        return self.filename

    def file_size(self):
        return define_units(self.volume)

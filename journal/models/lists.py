from datetime import datetime

from .. import db
from ..models.exc import BadRequiredArg, NoneRequiredArg
from ..models.mixins import EntityMixin, SluggedMixin
from .commentaries import Commentary
from .labels import Label


list_labels = db.Table(
    'list_labels',
    db.Column('list_id', db.Integer, db.ForeignKey('lists.id')),
    db.Column('label_id', db.Integer, db.ForeignKey('labels.id')))


class Section(db.Model):
    __tablename__ = 'sections'

    id = db.Column(db.Integer, primary_key=True)
    number = db.Column(db.Integer)
    list_id = db.Column(db.Integer, db.ForeignKey('lists.id'))
    item_id = db.Column(db.Integer, db.ForeignKey('items.id'))

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)

    def __repr__(self):
        return '<Section: item {0}, list {1}, number {2}>'.format(
            self.item_id, self.list_id, self.number)


class List(EntityMixin, SluggedMixin, db.Model):
    __tablename__ = 'lists'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64), nullable=False)
    preamble = db.Column(db.String(384), default=None)
    open = db.Column(db.Boolean, default=False)
    suffix = db.Column(db.String(7), unique=True)
    owner_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    sections = db.relationship('Section', backref='list_', lazy='dynamic')
    labels = db.relationship(
        Label,
        secondary=list_labels,
        backref=db.backref('lists', lazy='dynamic'),
        lazy='dynamic')
    commentaries = db.relationship(Commentary, backref='list_', lazy='dynamic')

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)
        if 'title' not in kwargs:
            raise NoneRequiredArg('missing required argument: "title"')
        if not kwargs['title'] or not isinstance(kwargs['title'], str):
            raise BadRequiredArg('bad required argument: "title"')
        now = datetime.utcnow()
        self.created = now
        self.edited = now
        self.published = now
        self.suffix = self.gen_suffix(7)
        self.slug = self.gen_slug(kwargs['title'])

    def __repr__(self):
        return '<List: {0}>'.format(self.title)

    @property
    def current_(self):
        try:
            return max(s.number for s in self.sections) + 1
        except ValueError:
            return 1

    @current_.setter
    def current_(self, value):
        raise AttributeError('current_ cannot be set')

    def count_comments(self):
        return self.commentaries.filter_by(deleted=False).count()

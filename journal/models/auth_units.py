from collections import namedtuple, OrderedDict

G = namedtuple(
    'G',
    ['pariah',
     'taciturn',
     'commentator',
     'blogger',
     'curator',
     'keeper',
     'root'])

groups = G(
    #  Now, the maximum length is 14 symbols.
    pariah='Изгои',
    taciturn='Молчащие',
    commentator='Комментаторы',
    blogger='Блогеры',
    curator='Модераторы',
    keeper='Хранители',
    root='Администраторы')

P = namedtuple(
    'P',
    ['CANNOT_LOG_IN',
     'READ_JOURNAL',
     'FOLLOW_USERS',
     'SEND_PM',
     'WRITE_COMMENTARY',
     'CREATE_LINK_ALIAS',
     'CREATE_ENTITY',
     'BLOCK_ENTITY',
     'REMOVE_COMMENTARY',
     'CHANGE_USER_ROLE',
     'UPLOAD_PICTURES',
     'MAKE_ANNOUNCEMENT',
     'ADMINISTER_SERVICE'])

permissions = P(
    #  Now, the maximum length is 24 symbols.
    CANNOT_LOG_IN='заблокирован',
    READ_JOURNAL='журналы и списки:чтение',
    FOLLOW_USERS='лента:новые',
    SEND_PM='приватные сообщения',
    WRITE_COMMENTARY='комментарии:новые',
    CREATE_LINK_ALIAS='ссылки:новые',
    CREATE_ENTITY='журналы и списки:новые',
    BLOCK_ENTITY='журналы и списки:цензура',
    REMOVE_COMMENTARY='комментарии:цензура',
    CHANGE_USER_ROLE='пользователи и группы',
    UPLOAD_PICTURES='картинки:новые',
    MAKE_ANNOUNCEMENT='объявления:новые',
    ADMINISTER_SERVICE='без ограничений')


def set_roles(ps, gs, cls):
    cls.insert_permissions(ps)
    store = OrderedDict()
    store[gs.pariah] = (
            [cls.query.filter_by(kind=ps.CANNOT_LOG_IN).first()], False)
    store[gs.taciturn] = (
            [cls.query.filter_by(kind=ps.READ_JOURNAL).first(),
             cls.query.filter_by(kind=ps.FOLLOW_USERS).first()], False)
    store[gs.commentator] = (
            [cls.query.filter_by(kind=ps.READ_JOURNAL).first(),
             cls.query.filter_by(kind=ps.FOLLOW_USERS).first(),
             cls.query.filter_by(kind=ps.SEND_PM).first(),
             cls.query.filter_by(kind=ps.WRITE_COMMENTARY).first(),
             cls.query.filter_by(kind=ps.CREATE_LINK_ALIAS).first()], False)
    store[gs.blogger] = (
            [cls.query.filter_by(kind=ps.READ_JOURNAL).first(),
             cls.query.filter_by(kind=ps.FOLLOW_USERS).first(),
             cls.query.filter_by(kind=ps.SEND_PM).first(),
             cls.query.filter_by(kind=ps.WRITE_COMMENTARY).first(),
             cls.query.filter_by(kind=ps.CREATE_LINK_ALIAS).first(),
             cls.query.filter_by(kind=ps.CREATE_ENTITY).first()], True)
    store[gs.curator] = (
            [cls.query.filter_by(kind=ps.READ_JOURNAL).first(),
             cls.query.filter_by(kind=ps.FOLLOW_USERS).first(),
             cls.query.filter_by(kind=ps.SEND_PM).first(),
             cls.query.filter_by(kind=ps.CREATE_LINK_ALIAS).first(),
             cls.query.filter_by(kind=ps.BLOCK_ENTITY).first(),
             cls.query.filter_by(kind=ps.REMOVE_COMMENTARY).first(),
             cls.query.filter_by(kind=ps.CHANGE_USER_ROLE).first()], False)
    store[gs.keeper] = (
            [cls.query.filter_by(kind=ps.READ_JOURNAL).first(),
             cls.query.filter_by(kind=ps.FOLLOW_USERS).first(),
             cls.query.filter_by(kind=ps.SEND_PM).first(),
             cls.query.filter_by(kind=ps.WRITE_COMMENTARY).first(),
             cls.query.filter_by(kind=ps.CREATE_LINK_ALIAS).first(),
             cls.query.filter_by(kind=ps.CREATE_ENTITY).first(),
             cls.query.filter_by(kind=ps.CHANGE_USER_ROLE).first(),
             cls.query.filter_by(kind=ps.UPLOAD_PICTURES).first(),
             cls.query.filter_by(kind=ps.MAKE_ANNOUNCEMENT).first()], False)
    store[gs.root] = (
            cls.query.filter(cls.kind != ps.CANNOT_LOG_IN).all(), False)
    return store

from slugify import Slugify

make_slug = Slugify(max_length=50, to_lower=True)


def count_n(string_):
    """
    Args:
        string_: can be any string that ends with an underscore and any,
                 integer, for example 'first-string-ever_1'.
    Returns: an integer with which a given string_ ends or zero.
    """
    try:
        n = int(string_.split('_')[-1])
    except (IndexError, ValueError):
        n = 0
    return n

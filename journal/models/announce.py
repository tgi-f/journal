from datetime import datetime
from random import choice
from string import ascii_letters, digits

from .. import db
from .exc import BadRequiredArg, NoneRequiredArg
from .mixins import BodyMixin


class Announcement(BodyMixin, db.Model):
    __tablename__ = 'announcements'

    id = db.Column(db.Integer, primary_key=True)
    abstract = db.Column(db.String(50), nullable=False)
    suffix = db.Column(db.String(6), unique=True)
    public = db.Column(db.Boolean, default=False)
    main = db.Column(db.Boolean, default=False)
    published = db.Column(db.DateTime, default=datetime.utcnow)
    owner_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    def __init__(self, **kwargs):
        BodyMixin.__init__(self, **kwargs)
        db.Model.__init__(self, **kwargs)
        if 'abstract' not in kwargs:
            raise NoneRequiredArg('missing required argument: "abstract"')
        if not kwargs['abstract'] or not isinstance(kwargs['abstract'], str):
            raise BadRequiredArg('bad required argument: "abstract"')
        self.suffix = self.gen_suffix()

    def __repr__(self):
        return '<Announcement: {} by {}>'.format(
            self.suffix, self.owner.username if self.owner else '...')

    @classmethod
    def gen_suffix(cls):
        while True:
            value = ''.join([choice(ascii_letters + digits) for _ in range(6)])
            if cls.query.filter_by(suffix=value).first():
                continue
            return value


db.event.listen(Announcement.body, 'set', Announcement.on_changed_body)

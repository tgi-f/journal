def define_units(number):
    if number < 1024:
        return number, 'B'
    elif 1024 < number < pow(1024, 2):
        return round(number / 1024, 2), 'KB'
    elif pow(1024, 2) < number < pow(1024, 3):
        return round(number / pow(1024, 2), 2), 'MB'
    elif number > pow(1024, 3):
        return round(number / pow(1024, 3), 2), 'GB'


def shorten_line(line, length):
    if ' ' not in line or len(line.split(' ')[0] + '~') >= length:
        return line[:length-1] + '~'
    result = ''
    for item in line.split(' '):
        tempo = result + ' ' + item
        if len(tempo.lstrip() + '~') > length:
            return result.lstrip() + '~'
        result = tempo

from datetime import datetime
from random import choice
from string import ascii_letters, digits
from urllib.parse import urlparse

from flask import url_for
from sqlalchemy import UniqueConstraint

from .. import db
from .exc import BadRequiredArg, NoneRequiredArg


class Link(db.Model):
    __tablename__ = 'links'

    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(512), nullable=False)
    spring = db.Column(db.String(6), unique=True, nullable=False)
    created = db.Column(db.DateTime, default=datetime.utcnow)
    clicked = db.Column(db.Integer, default=0)
    owner_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    __table_args__ = (
        UniqueConstraint(
            'owner_id', 'url', name='owner_url_uni'),)

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)
        if 'url' not in kwargs:
            raise NoneRequiredArg('missing required argument: "url"')
        if not kwargs['url'] or not isinstance(kwargs['url'], str):
            raise BadRequiredArg('bad required argument: "url"')
        self.spring = self.gen_spring()

    def __repr__(self):
        return '<Link: {}>'.format(self.parse_url())

    @classmethod
    def gen_spring(cls):
        while True:
            value = ''.join([choice(ascii_letters + digits) for _ in range(6)])
            if cls.query.filter_by(spring=value).first():
                continue
            return value

    def parse_url(self):
        line = ''.join(urlparse(self.url)[1:])
        if len(line) > 30:
            line = line[:29] + '~'
        return line

from random import choice
from string import ascii_letters, digits

from redis import Redis


def connect(host):
    return Redis(host=host)


def get_unique(conn, prefix):
    while True:
        result = prefix + ''.join(choice(ascii_letters + digits) for _
                                  in range(6))
        if conn.exists(result):
            continue
        return result


def assign_cache(conn, prefix, value, expiration):
    cache = get_unique(conn, prefix)
    conn.set(cache, value)
    conn.expire(cache, expiration)
    return cache

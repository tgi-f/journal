from datetime import datetime

from .. import db
from ..html_tools import purify
from .commentaries import Commentary
from .lists import Section
from .labels import Label
from .mixins import BodyMixin, EntityMixin, SluggedMixin
from .exc import BadRequiredArg, NoneRequiredArg
from .pictures_tools import shorten_line


item_labels = db.Table(
    'item_labels',
    db.Column('item_id', db.Integer, db.ForeignKey('items.id')),
    db.Column('label_id', db.Integer, db.ForeignKey('labels.id')))


class Item(BodyMixin, EntityMixin, SluggedMixin, db.Model):
    __tablename__ = 'items'

    id = db.Column(db.Integer, primary_key=True)
    abstract = db.Column(db.String(100), nullable=False)
    summary = db.Column(db.String(256), default=None)
    suffix = db.Column(db.String(8), unique=True)
    owner_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    sections = db.relationship(Section, backref='item', lazy='dynamic')
    labels = db.relationship(
        Label,
        secondary=item_labels,
        backref=db.backref('items', lazy='dynamic'),
        lazy='dynamic')
    commentaries = db.relationship(Commentary, backref='item', lazy='dynamic')

    def __init__(self, independent=True, **kwargs):
        BodyMixin.__init__(self, **kwargs)
        db.Model.__init__(self, **kwargs)
        if 'abstract' not in kwargs:
            raise NoneRequiredArg('missing required argument: "abstract"')
        if not kwargs['abstract'] or not isinstance(kwargs['abstract'], str):
            raise BadRequiredArg('bad required argument: "abstract"')
        if independent:
            now = datetime.utcnow()
            self.created = now
            self.edited = now
            self.published = now
            self.suffix = self.gen_suffix(8)
            self.slug = self.gen_slug(kwargs['abstract'])
            self.summary = purify(self.body_html)

    def __repr__(self):
        return '<Item: {}>'.format(self.slug)

    @property
    def abstract_(self):
        if len(self.abstract) > 80:
            return shorten_line(self.abstract, 80)
        return self.abstract

    @abstract_.setter
    def abstract_(self, value):
        raise AttributeError('abstract_ cannot be set')

    @staticmethod
    def on_changed_body_html(target, value, olvalue, initiator):
        if target.owner_id:
            target.summary = purify(value)

    def count_comments(self):
        return self.commentaries.filter_by(deleted=False).count()


db.event.listen(Item.body, 'set', Item.on_changed_body)
db.event.listen(Item.body_html, 'set', Item.on_changed_body_html)

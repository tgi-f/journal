class NoneRequiredArg(Exception):
    pass


class BadRequiredArg(Exception):
    pass

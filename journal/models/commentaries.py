from datetime import datetime

from .mixins import BodyMixin
from .. import db


class Commentary(BodyMixin, db.Model):
    __tablename__ = 'commentaries'

    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, default=datetime.utcnow)
    deleted = db.Column(db.Boolean, default=False)
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    item_id = db.Column(db.Integer, db.ForeignKey('items.id'))
    list_id = db.Column(db.Integer, db.ForeignKey('lists.id'))
    parent_id = db.Column(db.Integer, db.ForeignKey('commentaries.id'))
    children = db.relationship(
        'Commentary',
        backref=db.backref('parent', remote_side=[id]),
        lazy='dynamic', order_by='Commentary.timestamp.asc()')

    def __init__(self, **kwargs):
        BodyMixin.__init__(self, **kwargs)
        db.Model.__init__(self, **kwargs)

    def __repr__(self):
        return '<Commentary: {0}>'.format(self.id)

    def len_branch(self, n=1):
        if self.parent:
            n += 1
            n = self.parent.len_branch(n)
        return n

db.event.listen(Commentary.body, 'set', Commentary.on_changed_body)

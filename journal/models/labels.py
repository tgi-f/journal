from .. import db


class Label(db.Model):
    __tablename__ = 'labels'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), nullable=False, unique=True)

    def __repr__(self):
        return '<Label: {0}>'.format(self.name)

    @property
    def is_empty(self):
        return not bool(self.lists.count() + self.items.count())

    @is_empty.setter
    def is_empty(self, value):
        raise AttributeError('is_empty cannot be set')

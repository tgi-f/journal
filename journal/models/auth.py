from datetime import datetime
from hashlib import md5

from flask import current_app, request
from flask_login import AnonymousUserMixin, UserMixin
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from werkzeug.security import check_password_hash, generate_password_hash

from .announce import Announcement
from .auth_units import groups, set_roles, permissions
from .exc import BadRequiredArg, NoneRequiredArg
from .blogs import Item
from .commentaries import Commentary
from .links import Link
from .lists import List
from .pictures import Album
from .pictures_tools import define_units
from .pm import Message
from .. import db, lm


bonds = db.Table(
    'bonds',
    db.Column('group_id', db.Integer, db.ForeignKey('groups.id')),
    db.Column('permission_id', db.Integer, db.ForeignKey('permissions.id')))


class Permission(db.Model):
    __tablename__ = 'permissions'

    id = db.Column(db.Integer, primary_key=True)
    kind = db.Column(db.String(32), unique=True, nullable=False)

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)
        if 'kind' not in kwargs:
            raise NoneRequiredArg('missing required argument: "kind"')
        if not kwargs['kind'] or not isinstance(kwargs['kind'], str):
            raise BadRequiredArg('bad required argument: "kind"')

    def __repr__(self):
        return '<Permission: {}>'.format(self.kind)

    @staticmethod
    def insert_permissions(perms):
        for perm in Permission.query:
            if perm.kind not in perms:
                db.session.delete(perm)
        for perm in perms:
            p = Permission.query.filter_by(kind=perm).first()
            if p is None:
                p = Permission(kind=perm)
                db.session.add(p)
        db.session.commit()


class Group(db.Model):
    __tablename__ = 'groups'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(32), unique=True, nullable=False)
    default = db.Column(db.Boolean, default=False, nullable=False)
    permissions = db.relationship(
        'Permission',
        secondary=bonds,
        backref=db.backref('groups', lazy='dynamic'),
        lazy='dynamic')
    users = db.relationship('User', backref='group', lazy='dynamic')

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)
        if 'title' not in kwargs:
            raise NoneRequiredArg('missing required argument: "title"')
        if not kwargs['title'] or not isinstance(kwargs['title'], str):
            raise BadRequiredArg('bad required argument: "title"')

    def __repr__(self):
        return '<Group: {}>'.format(self.title)

    @staticmethod
    def insert_groups(perms, grps, pcls):
        roles = set_roles(perms, grps, pcls)
        for role in roles:
            group = Group.query.filter_by(title=role).first()
            if group is None:
                group = Group(title=role)
            for permission in group.permissions:
                if permission not in roles[role][0]:
                    group.permissions.remove(permission)
            for permission in roles[role][0]:
                if permission not in group.permissions:
                    group.permissions.append(permission)
            group.default = roles[role][1]
            db.session.add(group)
        db.session.commit()


class Account(db.Model):
    __tablename__ = 'accounts'

    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(128), unique=True, nullable=False)
    swap = db.Column(db.String(128), default=None)
    ava_hash = db.Column(db.String(32), nullable=False)
    requested = db.Column(db.DateTime, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    user = db.relationship('User', back_populates='account')

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)
        if 'address' not in kwargs:
            raise NoneRequiredArg('missing required argument: "address"')
        if not kwargs['address'] or not isinstance(kwargs['address'], str):
            raise BadRequiredArg('bad required argument: "address"')
        if 'swap' in kwargs:
            self.swap = None

    def __repr__(self):
        return '<Account: {}>'.format(self.address)

    @staticmethod
    def on_changed_address(target, value, oldvalue, initiator):
        if not value or not isinstance(value, str):
            return None
        target.ava_hash = md5(value.encode('utf-8')).hexdigest()

    def generate_token(self, expiration=3600):
        s = Serializer(current_app.config.get('SECRET_KEY'), expiration)
        return s.dumps({'confirm': self.id}).decode('utf-8')

    def get_ava_url(self, size=100, default='mm', rating='g'):
        if request.is_secure:
            url = 'https://secure.gravatar.com/avatar'
        else:
            url = 'http://www.gravatar.com/avatar'
        return '{0}/{1}?s={2}&d={3}&r={4}'.format(
            url, self.ava_hash, size, default, rating)

    def parse_address(self):
        if len(self.address) > 30:
            name, domain = self.address.split('@')
            if len(domain) > 15:
                if len(name) > 14:
                    domain = '~' + domain[-14:]
                else:
                    tail = 30 - len(name) - 2
                    domain = '~' + domain[-tail:]
            head = 30 - len(domain) - 2
            if len(name+'@'+domain) > 30:
                name = name[:head] + '~'
            return name + '@' + domain
        return self.address


db.event.listen(Account.address, 'set', Account.on_changed_address)


item_likes = db.Table(
    'item_likes',
    db.Column('item_id', db.Integer, db.ForeignKey('items.id')),
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')))


item_dislikes = db.Table(
    'item_dislikes',
    db.Column('item_id', db.Integer, db.ForeignKey('items.id')),
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')))

list_likes = db.Table(
    'list_likes',
    db.Column('list_id', db.Integer, db.ForeignKey('lists.id')),
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')))


list_dislikes = db.Table(
    'list_dislikes',
    db.Column('list_id', db.Integer, db.ForeignKey('lists.id')),
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')))


class Follow(db.Model):
    __tablename__ = 'follows'

    follower_id = db.Column(
        db.Integer, db.ForeignKey('users.id'), primary_key=True)
    followed_id = db.Column(
        db.Integer, db.ForeignKey('users.id'), primary_key=True)
    since = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<Follow: {0} follows {1}>'.format(
            self.follower.username, self.followed.username)


class Blocking(db.Model):
    __tablename__ = 'blockings'

    blocker_id = db.Column(
        db.Integer, db.ForeignKey('users.id'), primary_key=True)
    blocked_id = db.Column(
        db.Integer, db.ForeignKey('users.id'), primary_key=True)
    since = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<Blocking: {0} blocks {1}'.format(
            self.blocker.username, self.blocked.username)


class User(UserMixin, db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(16), unique=True, nullable=False)
    registered = db.Column(db.DateTime, default=datetime.utcnow)
    last_visit = db.Column(db.DateTime, default=datetime.utcnow)
    password_hash = db.Column(db.String(128))
    group_id = db.Column(db.Integer, db.ForeignKey('groups.id'))
    account = db.relationship('Account', back_populates='user', uselist=False)
    links = db.relationship(Link, backref='owner', lazy='dynamic')
    albums = db.relationship(Album, backref='owner', lazy='dynamic')
    items = db.relationship(Item, backref='owner', lazy='dynamic')
    lists = db.relationship(List, backref='owner', lazy='dynamic')
    liked_items = db.relationship(
        Item,
        secondary=item_likes,
        backref=db.backref('l_users', lazy='dynamic'),
        lazy='dynamic')
    liked_lists = db.relationship(
        List,
        secondary=list_likes,
        backref=db.backref('l_users', lazy='dynamic'),
        lazy='dynamic')
    disliked_items = db.relationship(
        Item,
        secondary=item_dislikes,
        backref=db.backref('d_users', lazy='dynamic'),
        lazy='dynamic')
    disliked_lists = db.relationship(
        List,
        secondary=list_dislikes,
        backref=db.backref('d_users', lazy='dynamic'),
        lazy='dynamic')
    followed = db.relationship(
        'Follow',
        foreign_keys=[Follow.follower_id],
        backref=db.backref('follower', lazy='joined'),
        lazy='dynamic',
        cascade='all, delete-orphan')
    followers = db.relationship(
        'Follow',
        foreign_keys=[Follow.followed_id],
        backref=db.backref('followed', lazy='joined'),
        lazy='dynamic',
        cascade='all, delete-orphan')
    blocked = db.relationship(
        'Blocking',
        foreign_keys=[Blocking.blocker_id],
        backref=db.backref('blocker', lazy='joined'),
        lazy='dynamic',
        cascade='all, delete-orphan')
    blockers = db.relationship(
        'Blocking',
        foreign_keys=[Blocking.blocked_id],
        backref=db.backref('blocked', lazy='joined'),
        lazy='dynamic',
        cascade='all, delete-orphan')
    commentaries = db.relationship(Commentary, backref='author', lazy='dynamic')
    sent_messages = db.relationship(
        Message,
        foreign_keys=[Message.sender_id],
        backref=db.backref('sender', lazy='joined'),
        lazy='dynamic')
    received_messages = db.relationship(
        Message,
        foreign_keys=[Message.recipient_id],
        backref=db.backref('recipient', lazy='joined'),
        lazy='dynamic')
    announcements = db.relationship(
        Announcement, backref='owner', lazy='dynamic')

    def __init__(self, **kwargs):
        UserMixin.__init__(self)
        db.Model.__init__(self, **kwargs)
        if 'username' not in kwargs:
            raise NoneRequiredArg('missing required argument: "username"')
        if 'password' not in kwargs:
            raise NoneRequiredArg('missing required argument: "password"')
        if not kwargs['username'] or not isinstance(kwargs['username'], str):
            raise BadRequiredArg('bad required argument: "username"')
        if not self.group:
            self.group = Group.query.filter_by(default=True).first()

    def __repr__(self):
        return '<User: {}>'.format(self.username)

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        if not password or not isinstance(password, str):
            raise BadRequiredArg
        self.password_hash = generate_password_hash(
            password, method='pbkdf2:sha256')

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def can(self, permission):
        permission = Permission.query.filter_by(kind=permission).first()
        return self.group is not None and permission is not None and\
            permission in self.group.permissions

    def is_root(self):
        return self.can(permissions.ADMINISTER_SERVICE)

    def is_curator(self):
        return self.group.title == groups.curator

    def is_keeper(self):
        return self.group.title == groups.keeper

    def ping(self):
        self.last_visit = datetime.utcnow()
        db.session.add(self)
        db.session.commit()

    def count_files(self):
        return sum(album.pictures.count() for album in self.albums)

    def show_space(self):
        space = sum(album.volume for album in self.albums)
        return define_units(space)

    @property
    def followed_items(self):
        return Item.query.join(Follow, Follow.followed_id == Item.owner_id)\
            .filter(
            Follow.follower_id == self.id,
            Item.public.is_(True), Item.censored.is_(False))\
            .order_by(Item.published.desc())

    @followed_items.setter
    def followed_items(self, value):
        raise AttributeError('followed_items cannot be set')

    @property
    def followed_lists(self):
        return List.query.join(Follow, Follow.followed_id == List.owner_id)\
            .filter(
            Follow.follower_id == self.id,
            List.public.is_(True), List.censored.is_(False))\
            .order_by(List.published.desc())

    @followed_lists.setter
    def followed_lists(self, value):
        raise AttributeError('followed_lists cannot be set')

    @property
    def has_public_items(self):
        return bool(
            self.items.filter(
                Item.hidden.is_(False),
                Item.public.is_(True),
                Item.censored.is_(False)).first())

    @has_public_items.setter
    def has_public_items(self, value):
        raise AttributeError('has_public_items cannot be set')

    @property
    def has_public_lists(self):
        return bool(self.lists.filter(
            List.hidden.is_(False),
            List.public.is_(True),
            List.censored.is_(False)).first())

    @has_public_lists.setter
    def has_public_lists(self, value):
        raise AttributeError('has_public_lists cannot be set')

    def is_following(self, user):
        return bool(self.followed.filter_by(followed_id=user.id).first())

    def is_blocking(self, user):
        return bool(self.blocked.filter_by(blocked_id=user.id).first())

    def is_followed_by(self, user):
        return bool(self.followers.filter_by(follower_id=user.id).first())

    def is_blocked_by(self, user):
        return bool(self.blockers.filter_by(blocker_id=user.id).first())

    def follow(self, user):
        if not self.is_following(user) and not self.is_blocked_by(user):
            db.session.add(Follow(follower=self, followed=user))
            db.session.commit()

    def block(self, user):
        if not self.is_blocking(user):
            user.unfollow(self)
            db.session.add(Blocking(blocker=self, blocked=user))
            db.session.commit()

    def unfollow(self, user):
        f = self.followed.filter_by(followed_id=user.id).first()
        if f:
            db.session.delete(f)
            db.session.commit()

    def deblock(self, user):
        b = self.blocked.filter_by(blocked_id=user.id).first()
        if b:
            db.session.delete(b)
            db.session.commit()

    def count_new_pm(self):
        return Message.query.filter(
            Message.recipient == self,
            Message.removed_by_recipient.is_(False),
            Message.received.is_(None)).count()

    def count_likes(self):
        return sum(list_.l_users.count() for list_ in self.lists) + \
               sum(item.l_users.count() for item in self.items)

    def count_dislikes(self):
        return sum(list_.d_users.count() for list_ in self.lists) + \
               sum(item.d_users.count() for item in self.items)


class AnonymousUser(AnonymousUserMixin):
    def can(self, permission):
        if permission:
            return False
        return False

    def is_root(self):
        return False


lm.anonymous_user = AnonymousUser


@lm.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))
